#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QKeyEvent>
#include <QMainWindow>
#include <QProcess>
#include "myparentobject.h"
#include "produit.h"
#include "stock.h"
#include "transaction.h"
#include "option.h"
#include "calculator.h"
#include "nonpayee.h"
#include "animationstackedwidget.h"

#include "win10pushbuttonstate.h"
#include "win10pushbuttonstd.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow, public myParentObject
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void initView();
    void initVariables();

    void dashboard();
protected:
    void resizeEvent(QResizeEvent *event){event = event ; len = width() ;}
    int len ;
    void keyReleaseEvent(QKeyEvent *event);
private slots:
    void on_tableWidget_pressed(const QModelIndex &index);
    void backToHomePage();
    void remadeCommande();
    void validedirectment();

private:
    Ui::MainWindow *ui;
    Win10PushButtonState *produit, *stock, *transaction ;
    Win10PushButtonStd *option, *calculatrice, *nonPayee ;

    Produit *myProduit ;
    Stock *myStock ;
    Transaction *myTransaction;
    Option *myOption;
    Calculator *myCalculator ;
    NonPayee *myNonPayee ;

    int gTimeSwitch = 5000 ;
    bool showTransaction = false ;
    QString passwordTransaction = "18121994" ;
    QProcess process;
};

#endif // MAINWINDOW_H

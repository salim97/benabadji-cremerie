#include "nonpayee.h"
#include "ui_nonpayee.h"

#include <QInputDialog>
#include <QWidgetAction>

NonPayee::NonPayee(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::NonPayee)
{
    ui->setupUi(this);


    _myModel->setTable(enumNoPayee::tableName);
    _myModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
    _myModel->select() ;
    ui->tableView->setModel(_myModel);
    //ui->comboBox_type->addItems(_mapColumns[enumNoPayee]);
    //ui->comboBox_type->removeItem(0);

    ui->toolButton_new_product->setVisible(false);
    ui->toolButton_remove->setDisabled(true);
    initAction();
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    //modelCommande.setEditStrategy(QSqlTableModel::OnManualSubmit);
    modelCommande.setTable(enumCommande::tableName);
    ui->tableView_commande->setModel(&modelCommande);
    ui->tableView_commande->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tableView_commande->resizeColumnsToContents();
    ui->tableView_commande->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->toolButton_remade->setDisabled(true);
}

NonPayee::~NonPayee()
{
    delete ui;
}

void NonPayee::initAction()
{

    // init widget action

    QAction  *supprime, *valide, *renovle;
    supprime = new QAction("Supprime", this);
    valide = new QAction("Valide", this);
    renovle = new QAction("Renouvelle", this);

    //Icon
    supprime->setIcon(ui->toolButton_remove->icon());
    valide->setIcon(ui->toolButton_new_product->icon());
    renovle->setIcon(ui->toolButton_remade->icon());

    // on click
    connect(supprime, SIGNAL(triggered(bool)), this, SLOT(on_toolButton_remove_clicked()));
    connect(valide, SIGNAL(triggered(bool)), this, SLOT(on_toolButton_new_product_clicked()));
    connect(renovle, SIGNAL(triggered(bool)), this, SLOT(on_toolButton_remade_clicked()));

    // init table with action
    ui->tableView->setContextMenuPolicy(Qt::ActionsContextMenu);
    ui->tableView->addAction(valide);
    ui->tableView->addAction(supprime);
    ui->tableView->addAction(renovle);


}
void NonPayee::on_toolButton_back_clicked()
{
    if(ui->stackedWidget->currentIndex() != 0 )
    {
        ui->stackedWidget->setCurrentIndex(0);
        ui->toolButton_new_product->setVisible(true);
        ui->toolButton_remade->setEnabled(true);
    }
    else if(_myModel->tableName() == enumCommande::tableName)
        init();
    else
        emit backButton();
}

void NonPayee::init()
{
    ui->stackedWidget->setCurrentIndex(0);
    ui->toolButton_new_product->setVisible(true);
    //_myModel->setTable(enumNoPayee::tableName);
    _myModel->select() ;
    ui->tableView->hideColumn(enumNoPayee::_00id);
    ui->tableView->hideColumn(enumNoPayee::_04Prix_ntkamlek);
    ui->tableView->resizeColumnsToContents();
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->toolButton_new_product->setVisible(true);
    ui->tableView->sortByColumn(enumNoPayee::_01Date, Qt::DescendingOrder);
}

void NonPayee::on_tableView_clicked(const QModelIndex &index)
{
    ui->toolButton_new_product->setEnabled(true);
    ui->toolButton_remove->setEnabled(true);
}

void NonPayee::on_toolButton_remove_clicked()
{if(!getSettings("admin").toBool()) return ;
    if(ui->stackedWidget->currentIndex() != 0 )
        return ;
    if(_myModel->tableName() == enumNoPayee::tableName)
    {
    int currentRow = ui->tableView->currentIndex().row() ;
    QString id = _myModel->record(currentRow).value(enumNoPayee::_00id).toString() ;
    _myModel->removeRow(currentRow);
    if( !_myModel->submitAll() ) msgCritical("insertion erreur", _myModel->lastError().text());

    QSqlTableModel  tempModel;
    tempModel.setTable(enumCommande::tableName);
    tempModel.setEditStrategy(QSqlTableModel::OnManualSubmit);
    tempModel.setFilter("id_facture == '"+id+"'");
    tempModel.select() ;
    for ( int i = 0 ; i < tempModel.rowCount() ; i++ )
        tempModel.removeRow(i);
    if( !tempModel.submitAll() ) msgCritical("insertion erreur", tempModel.lastError().text());
    }
    else
    {
        int currentRow = ui->tableView->currentIndex().row() ;
        _myModel->removeRow(currentRow);
        if( !_myModel->submitAll() ) msgCritical("insertion erreur", _myModel->lastError().text());
    }
}

void NonPayee::on_toolButton_new_product_clicked()
{
    if(ui->stackedWidget->currentIndex() != 0 )
        return ;

if(ui->tableView->currentIndex().row() == -1 )
{
    msgCritical("erreur", "clique sur la commande pour valider");
    return ;
}
if(_myModel->tableName() == enumCommande::tableName)
    return;
qDebug() << _myModel->tableName() ;
    int m_currentRow = ui->tableView->currentIndex().row() ;
    QString null = _myModel->record(m_currentRow).value(enumNoPayee::_05NumeroTable).toString() ;
    if(null == "NULL")
        return ;

    QSqlTableModel temp;

    temp.setTable(enumTransaction::tableName);
    temp.setEditStrategy(QSqlTableModel::OnManualSubmit);
    temp.select();
    // helper
    bool ok ;
    int prixTotalTemp = _myModel->record(ui->tableView->currentIndex().row()).value(enumNoPayee::_03Montant_Total).toInt() ;
    int ch3almadelek = QInputDialog::getInt(this, "dialog", "Montant total est :"+QString::number(prixTotalTemp) + " DA", 1, 1, 999999999,1,&ok);
    if(!ok)
        return ;
    if(ch3almadelek < prixTotalTemp)
    {
        msgInformation("resulta", "erreur "+QString::number(ch3almadelek)+" < "+ QString::number(prixTotalTemp));
        return ;
    }
    msgInformation("resulta", QString::number(ch3almadelek)+" - "+QString::number(prixTotalTemp)+" = " +QString::number(ch3almadelek - prixTotalTemp ));

    int row = temp.rowCount() ;
    temp.insertRow(row) ;

    temp.setData(temp.index(row, enumTransaction::_01Date), getCurrentDateTime());
    temp.setData(temp.index(row, enumTransaction::_02Prix),
                      _myModel->record(ui->tableView->currentIndex().row()).value(enumNoPayee::_03Montant_Total).toString());
    temp.setData(temp.index(row, enumTransaction::_03Prix_ntkamlek),
                      _myModel->record(ui->tableView->currentIndex().row()).value(enumNoPayee::_04Prix_ntkamlek).toString());

    if( !temp.submitAll() ) msgCritical("insertion erreur Transaction", temp.lastError().text());
    //TODO : gla3 hadi
    int currentRow = ui->tableView->currentIndex().row() ;
    QString id = _myModel->record(currentRow).value(enumNoPayee::_00id).toString() ;

    _myModel->removeRow(currentRow);
    if( !_myModel->submitAll() ) msgCritical("insertion erreur", _myModel->lastError().text());
    QSqlTableModel  tempModel;
    tempModel.setTable(enumCommande::tableName);
    tempModel.setEditStrategy(QSqlTableModel::OnManualSubmit);
    tempModel.setFilter("id_facture == '"+id+"C'");
    //msgInformation("id", "id_facture == '"+id+"C'");
    tempModel.select() ;
    //id = temp.record(row).value(enumTransaction::_00id).toString() ;
    id = executeQueryAndGetList("select seq from sqlite_sequence where name='log'")[0] ;
    for ( int i = 0 ; i < tempModel.rowCount() ; i++ )
        tempModel.setData(tempModel.index(i, enumCommande::_01id_facture), id+"F");
    //msgInformation("id", id+"F");
    if( !tempModel.submitAll() ) msgCritical("insertion erreur", tempModel.lastError().text());

}

QString NonPayee::montantTotal()
{
    int total =0 ;
    for (int i = 0 ; i < _myModel->rowCount() ; i++ )
    {
        total += _myModel->record(i).value(enumNoPayee::_03Montant_Total).toInt() ;
    }
    return QString::number(total) ;
}

void NonPayee::validedirectment()
{
   ui->tableView->setCurrentIndex(_myModel->index(0,enumNoPayee::_01Date));
   on_toolButton_new_product_clicked();
}

void NonPayee::on_tableView_doubleClicked(const QModelIndex &index)
{
    if(ui->stackedWidget->currentIndex() != 0)
        return ;
    ui->stackedWidget->setCurrentIndex(1);
    ui->toolButton_remade->setDisabled(true);
    QString id = _myModel->record(index.row()).value(enumNoPayee::_00id).toString() ;
    modelCommande.setFilter("id_facture == '"+id+"C'");
    modelCommande.select() ;
    ui->tableView_commande->hideColumn(enumCommande::_00id);
    ui->tableView_commande->hideColumn(enumCommande::_01id_facture);
    ui->toolButton_new_product->setVisible(false);
}

void NonPayee::on_toolButton_remade_clicked()
{
    if(!getSettings("admin").toBool()) return ;
    nameList.clear();
    quantiteList.clear();
    //QStringList nameList, quantiteList;
    if(ui->stackedWidget->currentIndex() == 0)
    {
        QString table = _myModel->record(ui->tableView->currentIndex().row()).value(enumNoPayee::_05NumeroTable).toString() ;
        if(table == "NULL")
            return ;
        if(ui->tableView->currentIndex().row() == -1 )
        {
            msgCritical("erreur", "clique sur la commande pour valider");
            return ;
        }

        ui->stackedWidget->setCurrentIndex(1);
        QString id = _myModel->record(ui->tableView->currentIndex().row()).value(enumNoPayee::_00id).toString() ;
        modelCommande.setFilter("id_facture == '"+id+"C'");
        modelCommande.select() ;
        ui->tableView_commande->hideColumn(enumCommande::_00id);
        ui->tableView_commande->hideColumn(enumCommande::_01id_facture);
        ui->toolButton_new_product->setVisible(false);
    }

    for (int i = 0 ; i < modelCommande.rowCount() ; i++ )
    {
        nameList << modelCommande.record(i).value(enumCommande::_02Designation).toString();
        quantiteList << modelCommande.record(i).value(enumCommande::_03Quantite).toString();
    }

//    for (int i = 0 ; i < modelCommande.rowCount() ; i++ )
//    {//delete all
//        //nameList << modelCommande.record(i).value(enumCommande::_02Designation).toString();
//        //quantiteList << modelCommande.record(i).value(enumCommande::_03Quantite).toString();
//    }
    ui->stackedWidget->setCurrentIndex(0);
    //on_toolButton_remove_clicked();
    emit remadeCommande();
    //emit backButton();
    //nhothom f
    //supprime men nonpayee w commande

}

void NonPayee::on_toolButton_block_row_clicked()
{

    if(ui->stackedWidget->currentIndex() != 0 )
        return ;

    if(_myModel->tableName() == enumNoPayee::tableName)
    {
    int currentRow = ui->tableView->currentIndex().row() ;
    _myModel->setData(_myModel->index(currentRow, enumNoPayee::_05NumeroTable), "NULL");
    if( !_myModel->submitAll() ) msgCritical("insertion erreur", _myModel->lastError().text());
    _myModel->select() ;
      }
}

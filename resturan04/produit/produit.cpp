#include "produit.h"
#include "ui_produit.h"
#include "mainwindow.h"
#include "QThread"
#include <QAction>
#include <QInputDialog>
#include <QSpinBox>
#include <QWidgetAction>
#include <QApplication>
#include <QDesktopWidget>
#include <QPrinter>
#include <QPrintDialog>
#include <QTextDocument>
Produit::Produit(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Produit)
{
    ui->setupUi(this);
    qDebug() << "01" ;
    //readTable();
    newproduit = new NewProduit(this);
    connect(newproduit, SIGNAL(backButton()), this, SLOT(backToHomePage()));
    ui->stackedWidget->addWidget(newproduit);
    qDebug() << "02" ;
    selectionproduit = new SelectionProduit(this);
    connect(selectionproduit, SIGNAL(backButton()), this, SLOT(backToHomePage()));
    ui->stackedWidget->addWidget(selectionproduit);

    commande = new Commande(this);
    connect(commande, SIGNAL(backButton()), this, SLOT(backToHomePage()));

    ui->stackedWidget->addWidget(commande);
    ui->stackedWidget->setDuration(500);//TODO: GVAR
    columnCount = 3 ; //TODO: GVAR
    //ui->stackedWidget->setLength(len, AnimationStackedWidget::RightToLeft);

    ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectItems);
    ui->tableWidget->setSelectionMode( QAbstractItemView::NoSelection );
    ui->tableWidget->setFocusPolicy(Qt::NoFocus);
    //qDebug() << _myModel->database().tables() ;
    initAction();

    connect(selectionproduit, SIGNAL(selectedList(QStringList)), this, SLOT(selectedList(QStringList)) );
    ui->frame1->setVisible(false);
    ui->tableWidget_commande->setColumnCount(4);
    ui->tableWidget_commande->hideColumn(2);
    ui->tableWidget_commande->hideColumn(3);
    ui->tableWidget_commande->resizeColumnsToContents();
    ui->tableWidget_commande->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableWidget_commande->setSelectionMode(QAbstractItemView::NoSelection);
qDebug() << "03" ;
    // init list widget categorie
    refreshCategorie();
        qDebug() << "04" ;
    ui->toolButton_valide_directement->setVisible(true);
    ui->toolButton_valide_directement_transcation->setVisible(false);
    QAction  *rename;
    rename = new QAction("Raname", this);
    rename->setIcon(QIcon(":/win10/win10/Refresh.png"));

    // on click
    connect(rename, SIGNAL(triggered(bool)), this, SLOT(myRename()));

    // init table with action
    ui->listWidget_categorie->setContextMenuPolicy(Qt::ActionsContextMenu);
    ui->listWidget_categorie->addAction(rename);
    moveDatabaseToRAM();
    ui->checkBox_actualise_apres_limprission->setChecked(true);
    qDebug() << "05" ;
}

void Produit::myRename()
{
    QString oldName = ui->listWidget_categorie->currentItem()->text() ;
    QString newName = QInputDialog::getText(this, "rename", "new name: ",QLineEdit::Normal, oldName);
//    if(oldName.isEmpty())
//        return ;
    if(newName.isEmpty())
        return ;

    QSqlTableModel temp;
    temp.setTable(enumProduit::tableName);
    temp.setEditStrategy(QSqlTableModel::OnManualSubmit);
    QString columnName = _mapColumns[enumProduit::tableName][enumProduit::_02Categorie] ;
    temp.setFilter(columnName +" == '"+oldName+"'");
    temp.select();
    for ( int i = 0 ; i < temp.rowCount(); i++ )
    {
        temp.setData(temp.index(i, enumProduit::_02Categorie), newName);
    }
    if( !temp.submitAll() ) msgCritical("insertion erreur", temp.lastError().text());
    on_toolButton_refresh_clicked();
}

Produit::~Produit()
{
    ui->frame1->setVisible(true);
    QList<int> currentSizes = ui->splitter->sizes() ;
    setSettings("splitterX", currentSizes[0]);
    setSettings("splitterY", currentSizes[1]);
    delete ui;
}

void Produit::backToHomePage()
{
    moveDatabaseToRAM();
    ui->stackedWidget->start(0);
    ui->stackedWidget->setLength(len, AnimationStackedWidget::LeftToRight);
    qDebug() << "ui->listWidget_categorie->currentRow() "  << ui->listWidget_categorie->currentRow() ;
    QString temp ;
    if( ui->listWidget_categorie->currentRow() > -1 )
        temp = ui->listWidget_categorie->currentItem()->text() ;
    else
        temp = ui->listWidget_categorie->item(0)->text() ;


    qDebug() << "temp" << temp ;

    ui->listWidget_categorie->setCurrentRow(-1);
    refreshCategorie();
    if(!temp.isEmpty());
    for ( int i = 0 ; i < ui->listWidget_categorie->count() ; i++ )
    {
        if( ui->listWidget_categorie->item(i)->text() == temp)
        {
            ui->listWidget_categorie->setCurrentRow(i);
            return ;
        }
    }
    ui->listWidget_categorie->setCurrentRow(0);

}

void Produit::on_toolButton_back_clicked()
{
    emit backButton();
}

void Produit::on_toolButton_refresh_clicked()
{

    myFilter.clear();
    ui->lineEdit_search->setText("");
    //readTable();

    ui->label_prix_total_second->setText("0");
    ui->tableWidget_commande->setRowCount(0);
    ui->listWidget_categorie->setCurrentRow(0);
    QStringList listTemp, t ;
    listTemp << "ALL" ;
    QString columnname=_mapColumns[enumProduit::tableName][enumProduit::_02Categorie] ;
    t = getColumnFromTableWithoutDuplicates(enumProduit::tableName,columnname ) ;
    for ( int i = 0 ; i < t.length() ; i++ )
        listTemp << t[i] ;

    refreshCategorie();
    prendre->setEnabled(true);
    replace->setDisabled(true);

}
void Produit::refreshCategorie()
{
    qDebug() << "01" ;
    QString columnname=_mapColumns[enumProduit::tableName][enumProduit::_02Categorie] ;
    ui->listWidget_categorie->setDisabled(true);
    ui->listWidget_categorie->clear();
    ui->listWidget_categorie->addItem("01 ALL");
    qDebug() << "02" ;

    ui->listWidget_categorie->addItems(getColumnFromTableWithoutDuplicates(enumProduit::tableName,columnname ));
    qDebug() << "03" ;
    disabledList = false ;
    ui->listWidget_categorie->setCurrentRow(0);
    disabledList = true ;
    qDebug() << "04" ;

    ui->listWidget_categorie->setEnabled(true);
    ui->listWidget_categorie->sortItems(Qt::AscendingOrder); //DescendingOrder
}

void Produit::on_toolButton_new_product_clicked()
{if(!getSettings("admin").toBool()) return ;
    newproduit->modifie = false ; // on cas ou tkon true machi bel3ani

    int cr = ui->listWidget_categorie->currentRow() ;
    if(cr > 0)
        newproduit->setCurrentCategorie(ui->listWidget_categorie->currentItem()->text()) ;
    else
        newproduit->setCurrentCategorie("") ;

    ui->stackedWidget->start(1);
    ui->stackedWidget->setLength(len, AnimationStackedWidget::RightToLeft);
}

void Produit::on_tableWidget_clicked(const QModelIndex &index)
{
    //if(ui->tableWidget->item(index.row, index.column()))
    itemTableWidget *temp = nullptr;
    temp = (itemTableWidget*)ui->tableWidget->cellWidget(index.row(), index.column());
    //balak apre nagla3ha ...
    if(temp == nullptr)
        return ;


    int quantite = 1 ;

    if(!temp->_02Categorie.toLower().contains("suplement"))
    {
        bool ok ;
        quantite = QInputDialog::getInt(this, "dialog", "Quantite:", 1, 1, 999999999,1,&ok);
        if(!ok)
            return ;
    }
    else
    {
        temp->_01Designation = "  |-->"+ temp->_01Designation ;
    }

    temp->setSelected(true)  ;
    int row = ui->tableWidget_commande->rowCount() ;
    ui->tableWidget_commande->setRowCount(row+1);

    QTableWidgetItem *x=new QTableWidgetItem (temp->_01Designation);
    ui->tableWidget_commande->setItem(row, 0, x) ;

    QSpinBox *quantiteQSpinBox = new QSpinBox(ui->tableWidget_commande);
    quantiteQSpinBox->setMinimum(0);
    quantiteQSpinBox->setMaximum(999999999);
    quantiteQSpinBox->setValue(quantite);
    quantiteQSpinBox->setSingleStep(1);
    ui->tableWidget_commande->setCellWidget(row, 1, quantiteQSpinBox);


    QSpinBox *PUV= new QSpinBox(ui->tableWidget_commande);
    PUV->setMinimum(0);
    PUV->setMaximum(999999999);
    PUV->setValue(temp->_03Prix_Unit_Achate.toInt());
    PUV->setSingleStep(1);
    ui->tableWidget_commande->setCellWidget(row, 2, PUV);

    QSpinBox *PUA= new QSpinBox(ui->tableWidget_commande);
    PUA->setMinimum(0);
    PUA->setMaximum(999999999);
    PUA->setValue(temp->_04Prix_Unit_Vente.toInt());
    PUA->setSingleStep(1);
    ui->tableWidget_commande->setCellWidget(row, 3, PUA);

    if(!ui->frame1->isVisible())
    {
        ui->frame1->setVisible(true);
        QList<int> currentSizes = ui->splitter->sizes() ;
        currentSizes[0] = getSettings("splitterX").toInt() ;
        currentSizes[1] = getSettings("splitterY").toInt() ;
        qDebug() << "currentSizes" <<currentSizes ;
        if(currentSizes[1] == 0)
        {
            currentSizes[0] = 1341 ;
            currentSizes[1] = 400 ;
        }
        ui->splitter->setSizes(currentSizes);
        ui->tableWidget_commande->resizeColumnsToContents();
    }

    calculeLePrixTotal();
    connect(quantiteQSpinBox, SIGNAL(valueChanged(int)), this, SLOT(calculeLePrixTotal()) );


}

void Produit::on_toolButton_imprimie_clicked()
{   
    NumeroDeTable numerodetable ;
    numerodetable.exec() ;

    if(numerodetable.cancel)
    {
        //numerodetable.selectedTable = "" ;
        return ;
    }
    selectedTable = numerodetable.selectedTable ;

    QSqlTableModel myModel;
    myModel.setTable(enumNoPayee::tableName);
    myModel.setEditStrategy(QSqlTableModel::OnManualSubmit);
    myModel.select() ;
    /*-----get last client number --*/
    QStringList list ;
    for (int i = 0 ; i < myModel.rowCount() ; i++ )
    {
        list << myModel.record(i).value(enumNoPayee::_02Nom).toString().replace("client", "") ;
    }
    QString nom ;
    if(list.isEmpty())
        nom = "client1" ;
    else
    {
        int max = 1 ;
        QString item ;
        for (int i = 0 ; i < list.length() ; i++ )
        {
            item = list[i] ;
            if( item.toInt() > max )
                max = item.toInt() ;
        }
        max++ ;
        nom = "client"+QString::number(max) ;
    }
    gnom = nom ; // ptn

    int row = myModel.rowCount() ;
    myModel.insertRow(row);
    /*------get total ch3al ntakmatleh ---*/
    int Prix_ntkamlek = 0 ;
    QSpinBox *quantiteTemp, *prixTemp ;

    for ( int i = 0 ; i < ui->tableWidget_commande->rowCount() ; i++ )
    {
        quantiteTemp = (QSpinBox*)ui->tableWidget_commande->cellWidget(i, 1);
        prixTemp = (QSpinBox*)ui->tableWidget_commande->cellWidget(i, 3);
        Prix_ntkamlek += quantiteTemp->value() * prixTemp->value() ;
    }
    /*------set data ---*/
    lastDATETIME = getCurrentDateTime() ;
    myModel.setData(myModel.index(row, enumNoPayee::_01Date), lastDATETIME);
    myModel.setData(myModel.index(row, enumNoPayee::_02Nom), nom);
    myModel.setData(myModel.index(row, enumNoPayee::_03Montant_Total), ui->label_prix_total_second->text());
    myModel.setData(myModel.index(row, enumNoPayee::_04Prix_ntkamlek), QString::number(Prix_ntkamlek) );
    myModel.setData(myModel.index(row, enumNoPayee::_05NumeroTable), selectedTable);

    if( !myModel.submitAll() ) msgCritical("insertion erreur", myModel.lastError().text());

    /*----inserte wech commande ----*/
    QString lastId = myModel.record(row).value(enumNoPayee::_00id).toString() ;
    myModel.setTable(enumCommande::tableName);
    myModel.select();

    row = myModel.rowCount() ;

    QSpinBox *quantite, *prix;
    //qDebug() << "ui->tableWidget->rowCount() " << ui->tableWidget_commande->rowCount() ;
    for ( int i = 0 ; i < ui->tableWidget_commande->rowCount() ; i++ )
    {
        quantite = (QSpinBox*)ui->tableWidget_commande->cellWidget(i, 1);
        prix = (QSpinBox*)ui->tableWidget_commande->cellWidget(i, 3);// ??

        myModel.insertRow(row);

        myModel.setData(myModel.index(row, enumCommande::_01id_facture),
                        lastId+"C");
        QString temp99 = ui->tableWidget_commande->item(i, 0)->text() ;
        temp99.replace("  |-->", "") ;
        myModel.setData(myModel.index(row, enumCommande::_02Designation),
                        temp99);
        myModel.setData(myModel.index(row, enumCommande::_03Quantite),
                        quantite->text());
        myModel.setData(myModel.index(row, enumCommande::_04Montant_Total),
                        QString::number(prix->value() * quantite->value()));

        row++ ;
    }

    if( !myModel.submitAll() ) msgCritical("insertion erreur", myModel.lastError().text());
    //on_toolButton_back_clicked();

    imprimie();
    //imprimie();
    if(ui->checkBox_actualise_apres_limprission->isChecked())
        on_toolButton_refresh_clicked();
}

void Produit::imprimie()
{
    char n = 10 ;
    QString html ;
    html += "<html><font size='10'>" ;
    html +="<h1><font size='5'>Mon Dessert Creperie - Oran</font></h1>";
    html +="<h1><font size='3'>Date: "+ lastDATETIME + "</font></h1>";
    html += "<table style='border-collapse: collapse;' width=100%>" ;

    QSpinBox *quantite, *prix;
    for ( int i = 0 ; i < ui->tableWidget_commande->rowCount() ; i++ )
    {
        quantite = (QSpinBox*)ui->tableWidget_commande->cellWidget(i, 1);
        prix = (QSpinBox*)ui->tableWidget_commande->cellWidget(i, 3);

        html += "<tr>" ;
        html += "<td><font size='4'><center>   "+quantite->text()+"   </center></font></td>";

        html += "<td><font size='4'>"+ ui->tableWidget_commande->item(i, 0)->text()+"</font></td>" ;

        html += "<td><font size='4'><center>   "+QString::number(quantite->value() * prix->value())+" DA</center></font></td>";

        html += "</tr>" ;
    }
    html +="</table><p><font size='4'><center>Montant total: "+ui->label_prix_total_second->text()+" DA  </center></font></p>";
    html += "<p align='right'><font size='4'>"+gnom+" "+selectedTable+"         soyez les bienvenus.</font></p></font></html>" ;
    QPrinter printer(QPrinter::ScreenResolution);

    QPrintDialog *dialog = new QPrintDialog(&printer);
    dialog->setWindowTitle("Print Document");

    printer.setPaperSize(QSizeF(72, 297), QPrinter::Millimeter);

    printer.setFullPage(true);
    QTextDocument x;
    x.setPageSize(QSizeF(printer.pageRect().size()));
    x.setHtml(html);
    x.print(&printer);

}

void Produit::on_toolButton_valide_directement_clicked()
{
    on_toolButton_imprimie_clicked();
    if(ui->checkBox_actualise_apres_limprission->isChecked())
        on_toolButton_refresh_clicked();
    emit validedirectment();
}

void Produit::on_splitter_splitterMoved(int pos, int index)
{pos = pos ; index = index ;
    qDebug() << ui->splitter->sizes();
}

void Produit::on_lineEdit_search_editingFinished()
{
    if(!ui->lineEdit_search->text().isEmpty() )
    {
        myFilter = _mapColumns[enumProduit::tableName][enumProduit::_01Designation]+" LIKE '%"+ui->lineEdit_search->text()+"%'" ;
        ui->listWidget_categorie->setCurrentRow(-1);
        readTableNew();
        ui->label->setFocus();
    }
}

void Produit::on_listWidget_categorie_currentTextChanged(const QString &currentText)
{
//    if(ui->listWidget_categorie->currentRow() > -1 )
//    {
//        myFilter = _mapColumns[enumProduit::tableName][enumProduit::_02Categorie]+" LIKE '%"+currentText+"%'" ;
//        if(currentText == "ALL")
//        {
//            ui->lineEdit_search->setText("");
//            myFilter.clear();
//        }
//        readTable();
//    }
}

void Produit::on_listWidget_categorie_currentRowChanged(int currentRow)
{   if(disabledList)
    if(ui->listWidget_categorie->currentRow() > -1 )
    {
        QString currentText = ui->listWidget_categorie->item(currentRow)->text();
        myFilter = _mapColumns[enumProduit::tableName][enumProduit::_02Categorie]+" LIKE '%"+currentText+"%'" ;
        if(currentText == "01 ALL")
        {
            ui->lineEdit_search->setText("");
            myFilter.clear();
        }
//        readTable();
//        QTime myTimer;myTimer.start();
        readTableNew();
//        qDebug() << "time took to import data is : " << myTimer.elapsed();
    }
}

void Produit::remadeCommande(QStringList nameList, QStringList quantiteList)
{
    on_toolButton_refresh_clicked();
    //qDebug() << "nameList " << nameList ;
    for (int i = 0 ; i < nameList.length() ; i++ )
    {

        _myModel->setFilter(_mapColumns[enumProduit::tableName][enumProduit::_01Designation]+" == '"+
                nameList[i]+"'" );
        _myModel->select() ;
        if(_myModel->rowCount() < 1)
            continue ;
        int row = ui->tableWidget_commande->rowCount() ;
        ui->tableWidget_commande->setRowCount(row+1);

        QTableWidgetItem *x=new QTableWidgetItem (_myModel->record(0).value(enumProduit::_01Designation).toString());
        ui->tableWidget_commande->setItem(row, 0, x) ;

        QString temp = quantiteList[i];
        QSpinBox *quantiteQSpinBox = new QSpinBox(ui->tableWidget_commande);
        quantiteQSpinBox->setMinimum(0);
        quantiteQSpinBox->setMaximum(999999999);
        quantiteQSpinBox->setValue(temp.toInt());
        quantiteQSpinBox->setSingleStep(1);
        ui->tableWidget_commande->setCellWidget(row, 1, quantiteQSpinBox);


        QSpinBox *PUV= new QSpinBox(ui->tableWidget_commande);
        PUV->setMinimum(0);
        PUV->setMaximum(999999999);
        PUV->setValue(_myModel->record(0).value(enumProduit::_03Prix_Unit_Achate).toInt());
        PUV->setSingleStep(1);
        ui->tableWidget_commande->setCellWidget(row, 2, PUV);

        QSpinBox *PUA= new QSpinBox(ui->tableWidget_commande);
        PUA->setMinimum(0);
        PUA->setMaximum(999999999);
        PUA->setValue(_myModel->record(0).value(enumProduit::_04Prix_Unit_Vente).toInt());
        PUA->setSingleStep(1);
        ui->tableWidget_commande->setCellWidget(row, 3, PUA);

        if(!ui->frame1->isVisible())
        {
            ui->frame1->setVisible(true);
            QList<int> currentSizes = ui->splitter->sizes() ;
            currentSizes[0] = getSettings("splitterX").toInt() ;
            currentSizes[1] = getSettings("splitterY").toInt() ;
            ui->splitter->setSizes(currentSizes);
            ui->tableWidget_commande->resizeColumnsToContents();
        }

        calculeLePrixTotal();
        connect(quantiteQSpinBox, SIGNAL(valueChanged(int)), this, SLOT(calculeLePrixTotal()) );
    }
    ui->listWidget_categorie->setCurrentRow(0);
}

void Produit::on_toolButton_valide_directement_transcation_clicked()
{
//    on_toolButton_imprimie_clicked();
//    emit validedirectment();
}

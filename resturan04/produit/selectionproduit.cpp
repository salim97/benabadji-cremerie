#include "itemtable.h"
#include "selectionproduit.h"
#include "ui_selectionproduit.h"

SelectionProduit::SelectionProduit(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SelectionProduit)
{
    ui->setupUi(this);
}

SelectionProduit::~SelectionProduit()
{
    delete ui;
}

SelectionProduit::init()
{
    QString columnname=_mapColumns[enumProduit::tableName][enumProduit::_02Categorie] ;
    ui->comboBox_categorie->clear();
    ui->comboBox_categorie->addItem("ALL");
    ui->comboBox_categorie->addItems(getColumnFromTableWithoutDuplicates(enumProduit::tableName,columnname ));

    ui->listWidget->clear();
    _myModel->setTable(enumProduit::tableName);
    _myModel->select();

    QString itemS ;
    for ( int i = 0 ; i < _myModel->rowCount() ; i++ )
    {
        itemS = _myModel->record(i).value(enumProduit::_01Designation).toString();
        QListWidgetItem *item = new QListWidgetItem;
        item->setData( Qt::DisplayRole, itemS );
        item->setData( Qt::CheckStateRole, Qt::Unchecked );
        QFont fnt;
        fnt.setPointSize(25);
        fnt.setFamily("Arial");
        item->setFont(fnt);
        ui->listWidget->addItem( item );
    }
}

void SelectionProduit::on_toolButton_back_clicked()
{
    emit backButton();
}

void SelectionProduit::on_listWidget_clicked(const QModelIndex &index)
{
    if(ui->listWidget->item(index.row())->checkState() == Qt::Checked)
        ui->listWidget->item(index.row())->setCheckState(Qt::Unchecked);
    else
        ui->listWidget->item(index.row())->setCheckState(Qt::Checked);
}

void SelectionProduit::on_toolButton_valid_clicked()
{
    QStringList list ;
    for ( int i = 0 ;  i < ui->listWidget->count() ; i++ )
    {
        if( ui->listWidget->item(i)->checkState() == Qt::Checked )
            list << ui->listWidget->item(i)->text() ;
    }

    emit selectedList(list);
}

void SelectionProduit::on_lineEdit_search_textChanged(const QString &arg1)
{
    QString temp1, temp2 ;
    temp1 = arg1.toLower() ;

    for ( int i = 0 ; i < ui->listWidget->count() ; i++ )
    {
        if(!temp1.isEmpty())
        {
        temp2 = ui->listWidget->item(i)->text().toLower() ;
        if(temp2.contains(temp1))
            ui->listWidget->item(i)->setHidden(false);
        else
            ui->listWidget->item(i)->setHidden(true);

        }
        else
            ui->listWidget->item(i)->setHidden(false);
    }
}

void SelectionProduit::on_comboBox_type_currentIndexChanged(int index)
{
    if(index == 1 )
        ui->stackedWidget_search->setCurrentIndex(1);
    else
        ui->stackedWidget_search->setCurrentIndex(0);
}

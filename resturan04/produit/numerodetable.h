#ifndef NUMERODETABLE_H
#define NUMERODETABLE_H

#include <QDialog>

namespace Ui {
class NumeroDeTable;
}

class NumeroDeTable : public QDialog
{
    Q_OBJECT

public:
    explicit NumeroDeTable(QWidget *parent = 0);
    ~NumeroDeTable();
    QString selectedTable ;
    bool cancel = false ;
private slots:
    void on_pushButton_cancel_clicked();

    void on_pushButton_OK_clicked();

private:
    Ui::NumeroDeTable *ui;
};

#endif // NUMERODETABLE_H

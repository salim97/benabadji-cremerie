#ifndef NEWPRODUIT_H
#define NEWPRODUIT_H

#include <QWidget>
#include "myparentobject.h"

namespace Ui {
class NewProduit;
}

class NewProduit : public QWidget, public myParentObject
{
    Q_OBJECT

public:
    explicit NewProduit(QWidget *parent = 0);
    ~NewProduit();
    void setDataToView(QString _00id, QString _01Designation, QString _02Categorie, QString _03Prix_Unit_Achate,
                       QString _04Prix_Unit_Vente, QString _05Description, QByteArray _06Image, int itemRow);
    bool modifie = false ;
    QString _00id, _01Designation, _02Categorie,
    _03Prix_Unit_Achate, _04Prix_Unit_Vente,
    _05Description ;
    QByteArray _06Image ;
    int itemRow ;
    void setCurrentCategorie(QString name) ;
signals:
    void backButton();
private slots:
    void on_toolButton_back_clicked();

    void on_pushButton_browse_clicked();

    void on_toolButton_valide_clicked();

    void on_toolButton_reset_clicked();

    void on_spinBox_PUA_valueChanged(int arg1);

private:
    Ui::NewProduit *ui;
};

#endif // NEWPRODUIT_H

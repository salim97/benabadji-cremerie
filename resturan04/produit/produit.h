#ifndef PRODUIT_H
#define PRODUIT_H

#include <QWidget>
#include "myparentobject.h"
#include "itemtablewidget.h"
#include "newproduit.h"
#include "commande.h"
#include "selectionproduit.h"
#include "animationstackedwidget.h"
#include "itemtable.h"
#include "quickcommande.h"
#include "numerodetable.h"



namespace Ui {
class Produit;
}

class Produit : public QWidget, public myParentObject
{
    Q_OBJECT

public:
    explicit Produit(QWidget *parent = 0);
    ~Produit();
    void readTable();
    void cleanTable();
    void initAction();
    int viewItemsCount();

    QList<itemtable> getSelectedItems();
    bool enabled = false ;
    bool disabledList= true ;

    void imprimie();
    void remadeCommande(QStringList nameList, QStringList quantiteList);
    void refreshCategorie();
    void moveDatabaseToRAM();
    void readTableNew();
protected:
    void resizeEvent(QResizeEvent *event){event = event ; len = width() ;}
signals:
    void backButton();
    void validedirectment();
private slots:
     void on_toolButton_back_clicked();

     void on_toolButton_refresh_clicked();

     void on_toolButton_new_product_clicked();

     void on_tableWidget_clicked(const QModelIndex &index);

     void backToHomePage();
     void supprimeCurrentItem();
     void modifieCurrentItem();
     void selectedList(QStringList list) ;
     void calculeLePrixTotal();

     void on_toolButton_imprimie_clicked();

     void on_splitter_splitterMoved(int pos, int index);

     void supprimeCurrentItemSelected();
     void on_lineEdit_search_editingFinished();

     void on_toolButton_valide_directement_clicked();

     void on_listWidget_categorie_currentTextChanged(const QString &currentText);

     void on_listWidget_categorie_currentRowChanged(int currentRow);


     void myPrendre();
     void myRemplace();
     void myRename();
     void on_toolButton_valide_directement_transcation_clicked();

private:

    Ui::Produit *ui;
    int columnCount, len ;
    NewProduit *newproduit;
    Commande *commande ;
    SelectionProduit *selectionproduit ;
    QString myFilter ;
    bool filterON = false ;
    int oldTemp  = -1 ;
    QString gnom, selectedTable, lastDATETIME ;
    QAction  *ajoute, *supprime, *modifie, *supprimeSelected, *prendre, *replace;
    QString oldID, newID ;
    QList<itemtable> produitTable;
};

#endif // PRODUIT_H

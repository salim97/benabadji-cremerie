#include "produit.h"
#include "ui_produit.h"

#include <QSpinBox>
#include <QWidgetAction>

void Produit::readTable()
{
    readTableNew();
    return ;
    //int oldcurrentRowList = ui->listWidget_categorie->currentRow() ;
    QHeaderView *verticalHeader = ui->tableWidget->verticalHeader();
    verticalHeader->setSectionResizeMode(QHeaderView::Fixed);
    verticalHeader->setDefaultSectionSize(getSettings("itemSize").toInt());

    columnCount = getSettings("itembyCoulumn").toInt();
    _myModel->setTable(enumProduit::tableName);    
    _myModel->setFilter(myFilter);
    //qDebug() << "_myModel->filter() " << _myModel->filter() ;
    _myModel->select() ;
    myFilter.clear();

    // 01 compare between view and data
    // 02 if data rows == view rows then for each modifie values
    // 03 if data rows > view rows then modifie then add

 /*--------------------------------*/
    int rowT = _myModel->rowCount() ;
    int item_by_column = columnCount ;

    int column, row ;

    if( rowT > item_by_column )
        column = item_by_column ;
    else
        column = item_by_column ;

    row = rowT / item_by_column ;

    if( ( rowT % item_by_column ) > 0 )
        row++;
/*-----------------------------*/
    int viewCount = viewItemsCount() ;
    //if()
    ui->tableWidget->setColumnCount(column);
    ui->tableWidget->setRowCount(row);
    int currentRow = 0 ;
    QTime myTimer;
    myTimer.start();

    for ( int i = 0 ; i < row ; i++ )
    {
        for ( int j = 0 ; j < column ; j++)
        {
            itemTableWidget *mycell = nullptr ;
            if( currentRow >= rowT)
            {
                ui->tableWidget->setCellWidget(i,j, mycell);
            }
            else
            {

                if(currentRow < viewCount )
                {

                    mycell = (itemTableWidget*)ui->tableWidget->cellWidget(i, j);

                }
                else
                {

                    mycell = new itemTableWidget(this);
                    ui->tableWidget->setCellWidget(i,j, mycell);


                }

                if(mycell == nullptr)
                {
                    mycell = new itemTableWidget(this);
                    ui->tableWidget->setCellWidget(i,j, mycell);
                }
                if( currentRow < _myModel->rowCount())
                mycell->setDataToView(_myModel->record(currentRow).value(enumProduit::_00id).toString(),
                                      _myModel->record(currentRow).value(enumProduit::_01Designation).toString(),
                                      _myModel->record(currentRow).value(enumProduit::_02Categorie).toString(),
                                      _myModel->record(currentRow).value(enumProduit::_03Prix_Unit_Achate).toString(),
                                      _myModel->record(currentRow).value(enumProduit::_04Prix_Unit_Vente).toString(),
                                      _myModel->record(currentRow).value(enumProduit::_05Description).toString(),
                                      _myModel->record(currentRow).value(enumProduit::_06Image).toByteArray(),
                                      currentRow);

                mycell->setSelected(false);
                //connect(mycell, SIGNAL(clicked()), this, SLOT(on_tableWidget_clicked(QModelIndex)));

            }
            currentRow++;
        }
    }

qDebug() << "time took to import data is : " <<  myTimer.elapsed();



    //ui->tableWidget->resizeRowsToContents();
    ui->tableWidget->resizeColumnsToContents();
    //qDebug() << "viewItemsCount " << viewItemsCount() ;
    //ui->listWidget_categorie->setCurrentRow(oldcurrentRowList); ;

}
void Produit::moveDatabaseToRAM()
{
    _myModel->setTable(enumProduit::tableName);
    _myModel->setFilter("");
    _myModel->select();

    produitTable.clear();
    for ( int i = 0 ; i < _myModel->rowCount() ; i++ )
    {
        itemtable item ;
        item._00id = _myModel->record(i).value(enumProduit::_00id).toString() ;
        item._01Designation = _myModel->record(i).value(enumProduit::_01Designation).toString() ;
        item._02Categorie = _myModel->record(i).value(enumProduit::_02Categorie).toString() ;
        item._03Prix_Unit_Achate = _myModel->record(i).value(enumProduit::_03Prix_Unit_Achate).toString() ;
        item._04Prix_Unit_Vente = _myModel->record(i).value(enumProduit::_04Prix_Unit_Vente).toString() ;
        item._05Description = _myModel->record(i).value(enumProduit::_05Description).toString() ;
        //item._06Image = _myModel->record(i).value(enumProduit::_06Image).toByteArray() ;
        item._06ImagePixMap.loadFromData(
                    _myModel->record(i).value(enumProduit::_06Image).toByteArray() );
        produitTable << item ;
    }
}

void Produit::readTableNew()
{
    qDebug() << "readTableNew" ;
    //int oldcurrentRowList = ui->listWidget_categorie->currentRow() ;
    QHeaderView *verticalHeader = ui->tableWidget->verticalHeader();
    verticalHeader->setSectionResizeMode(QHeaderView::Fixed);
    verticalHeader->setDefaultSectionSize(getSettings("itemSize").toInt());
    qDebug() << "01" ;
    columnCount = getSettings("itembyCoulumn").toInt();
    QString categorie, designation ;
    if(ui->listWidget_categorie->currentRow() > 0 )
        categorie = ui->listWidget_categorie->currentItem()->text() ;
    designation = ui->lineEdit_search->text() ;
    QList<itemtable> produitTableTemp;
    qDebug() << "02" ;
    if( designation.isEmpty() && categorie.isEmpty() )
        produitTableTemp = produitTable;
    else if ( categorie.isEmpty())
    {
        for ( int i = 0 ; i < produitTable.length() ; i++ )
        {

            if(produitTable[i]._01Designation.toLower().contains(designation.toLower()))
            {
                produitTableTemp << produitTable[i] ;
            }
        }
    }
    else
    {
        for ( int i = 0 ; i < produitTable.length() ; i++ )
        {

            if(produitTable[i]._02Categorie == categorie)
            {
                produitTableTemp << produitTable[i] ;
            }
        }
    }
    myFilter.clear();
    qDebug() << "03" ;
    // 01 compare between view and data
    // 02 if data rows == view rows then for each modifie values
    // 03 if data rows > view rows then modifie then add

 /*--------------------------------*/
    int rowT = produitTableTemp.length() ;
    int item_by_column = columnCount ;

    int column, row ;

    if( rowT > item_by_column )
        column = item_by_column ;
    else
        column = item_by_column ;
    qDebug() << "item_by_column: " << item_by_column ;
    row = rowT / item_by_column ;

    if( ( rowT % item_by_column ) > 0 )
        row++;
/*-----------------------------*/
    int viewCount = viewItemsCount() ;
    //if()
    ui->tableWidget->setColumnCount(column);
    ui->tableWidget->setRowCount(row);
    int currentRow = 0 ;

qDebug() << "05" ;
    for ( int i = 0 ; i < row ; i++ )
    {
        for ( int j = 0 ; j < column ; j++)
        {
            itemTableWidget *mycell = nullptr ;
            if( currentRow >= rowT)
            {
                ui->tableWidget->setCellWidget(i,j, mycell);
            }
            else
            {

                if(currentRow < viewCount )
                {

                    mycell = (itemTableWidget*)ui->tableWidget->cellWidget(i, j);


                }
                else
                {
                    QTime myTimer;
                    myTimer.start();
                    mycell = new itemTableWidget(this);
                    ui->tableWidget->setCellWidget(i,j, mycell);


                }

                if(mycell == nullptr)
                {

                    mycell = new itemTableWidget(this);
                    ui->tableWidget->setCellWidget(i,j, mycell);

                }
                if( currentRow < produitTableTemp.length())
                mycell->setDataToViewNew(produitTableTemp[currentRow]._00id,
                                      produitTableTemp[currentRow]._01Designation,
                                      produitTableTemp[currentRow]._02Categorie,
                                      produitTableTemp[currentRow]._03Prix_Unit_Achate,
                                      produitTableTemp[currentRow]._04Prix_Unit_Vente,
                                      produitTableTemp[currentRow]._05Description,
                                      produitTableTemp[currentRow]._06ImagePixMap,
                                      currentRow);

                mycell->setSelected(false);
                //connect(mycell, SIGNAL(clicked()), this, SLOT(on_tableWidget_clicked(QModelIndex)));

            }
            currentRow++;
        }
    }




    qDebug() << "06" ;
    //ui->tableWidget->resizeRowsToContents();
    ui->tableWidget->resizeColumnsToContents();
    //qDebug() << "viewItemsCount " << viewItemsCount() ;
    //ui->listWidget_categorie->setCurrentRow(oldcurrentRowList); ;
}

void Produit::cleanTable()
{
    ui->tableWidget->setRowCount(0);
}

int Produit::viewItemsCount()
{
    int count = 0 ;
    qDebug() << "zbel 01" ;
    for (int i = 0 ; i < ui->tableWidget->rowCount() ; i++ )
    {
        for ( int j = 0 ; j < ui->tableWidget->columnCount() ; j++ )
        {
            if((itemTableWidget*)ui->tableWidget->cellWidget(i, j) == nullptr)
                break ;

            count++ ;
        }
    }
    qDebug() << "zbel 02" ;
    return count ;
}

QList<itemtable> Produit::getSelectedItems()
{
    QList<itemtable> selectedList ;
    int rowCount = ui->tableWidget->rowCount();
    int columnCount = ui->tableWidget->columnCount();
    itemTableWidget *temp = nullptr;

    for ( int i = 0 ; i < rowCount ; i++ )
    {
        for (int j = 0 ; j < columnCount ; j++ )
        {
            temp = (itemTableWidget*)ui->tableWidget->cellWidget(i, j);
            if( temp != nullptr )
            {
                if(temp->isSelected())
                {
                    itemtable item;
                    item._00id = temp->_00id ;
                    item._01Designation = temp->_01Designation ;
                    item._02Categorie = temp->_02Categorie ;
                    item._03Prix_Unit_Achate = temp->_03Prix_Unit_Achate ;
                    item._04Prix_Unit_Vente = temp->_04Prix_Unit_Vente ;
                    item._05Description = temp->_05Description ;
                    selectedList << item ;
                }
            }
        }
    }
    return selectedList ;
}

void Produit::calculeLePrixTotal()
{
    int total = 0 ;
    QSpinBox *quantite, *prix ;

    for ( int i = 0 ; i < ui->tableWidget_commande->rowCount() ; i++ )
    {
        quantite = (QSpinBox*)ui->tableWidget_commande->cellWidget(i, 1);
        prix = (QSpinBox*)ui->tableWidget_commande->cellWidget(i, 3);
        total += quantite->value() * prix->value() ;
    }

    ui->label_prix_total_second->setText(QString::number(total));
}

void Produit::initAction()
{
//    QString QSS= "QLabel::hover { border-style: outset; border-width: 2px; border-color: red;}"
//                 " QLabel { color: black; background-color: white ; }" ;
//    QSS = "" ;
//    QLabel *ajoutel = new QLabel(QString(ui->toolButton_new_product->text()), this);
//    ajoutel->setStyleSheet(QSS);

//    QLabel *supprimel = new QLabel(QString("Supprimié"), this);
//    supprimel->setStyleSheet(QSS);

//    QLabel *modifiel = new QLabel(QString("Modifié"), this);
//    modifiel->setStyleSheet(QSS);

//    QLabel *supprimeSelectedl = new QLabel(QString("Supprimié"), this);
//    supprimeSelectedl->setStyleSheet(QSS);

//    // init widget action

//    QWidgetAction *ajoute, *supprime, *modifie, *supprimeSelected;
//    ajoute = new QWidgetAction(this);
//    supprime = new QWidgetAction(this);
//    modifie = new QWidgetAction(this);
//    supprimeSelected = new QWidgetAction(this);

//    ajoute->setDefaultWidget(ajoutel);
//    supprime->setDefaultWidget(supprimel);
//    modifie->setDefaultWidget(modifiel);
//    supprimeSelected->setDefaultWidget(supprimeSelectedl);

//    QAction  *ajoute, *supprime, *modifie, *supprimeSelected, *prendre, *replace;
    ajoute = new QAction(ui->toolButton_new_product->text(), this);
    supprime = new QAction("Supprimié", this);
    modifie = new QAction("Modifié", this);
    supprimeSelected = new QAction("Supprimié", this);
    prendre = new QAction("Prendre ca", this);
    replace = new QAction("Remplacer avec celui la", this);
    // icon

    ajoute->setIcon(ui->toolButton_new_product->icon());
    supprime->setIcon(QIcon(":/win10/win10/Cancel-96.png"));
    modifie->setIcon(QIcon(":/icon/icon/Edit Column-96.png"));
    supprimeSelected->setIcon(QIcon(":/win10/win10/Cancel-96.png"));

    //Icon
    //ajoute->setIcon(ui->toolButton_Nouv_Travailleur->icon());
    //supprime->setIcon(ui->toolButton_suppr->icon());
    //modifie->setIcon(ui->toolButton_Attestation->icon());

    // on click
    connect(ajoute, SIGNAL(triggered(bool)), this, SLOT(on_toolButton_new_product_clicked()));
    connect(supprime, SIGNAL(triggered(bool)), this, SLOT(supprimeCurrentItem()));
    connect(modifie, SIGNAL(triggered(bool)), this, SLOT(modifieCurrentItem()));
    connect(supprimeSelected, SIGNAL(triggered(bool)), this, SLOT(supprimeCurrentItemSelected()));

    connect(prendre, SIGNAL(triggered(bool)), this, SLOT(myPrendre()));
    connect(replace, SIGNAL(triggered(bool)), this, SLOT(myRemplace()));

    // init table with action
    ui->tableWidget->setContextMenuPolicy(Qt::ActionsContextMenu);
    ui->tableWidget->addAction(ajoute);
    ui->tableWidget->addAction(supprime);
    ui->tableWidget->addAction(modifie);
    ui->tableWidget->addAction(prendre);
    ui->tableWidget->addAction(replace);
    ui->tableWidget_commande->setContextMenuPolicy(Qt::ActionsContextMenu);
    ui->tableWidget_commande->addAction(supprimeSelected);

    replace->setDisabled(true);
}

void Produit::myPrendre()
{
    itemTableWidget *temp = nullptr;
    temp = (itemTableWidget*)ui->tableWidget->cellWidget(ui->tableWidget->currentIndex().row(),
                                                         ui->tableWidget->currentIndex().column());
    if(temp == nullptr) return ;

    oldID = temp->_00id ;
    replace->setEnabled(true);
    prendre->setDisabled(true);
}

void Produit::myRemplace()
{
    itemTableWidget *temp = nullptr;
    temp = (itemTableWidget*)ui->tableWidget->cellWidget(ui->tableWidget->currentIndex().row(),
                                                         ui->tableWidget->currentIndex().column());
    if(temp == nullptr) return ;
    newID = temp->_00id ;

    _myModel->setFilter("id == "+newID);
    _myModel->select();
    _myModel->setData(_myModel->index(0, enumProduit::_00id), -1);
    if( !_myModel->submitAll() ) msgCritical("insertion erreur", _myModel->lastError().text());

    _myModel->setFilter("id == "+oldID);
    _myModel->select();
    _myModel->setData(_myModel->index(0, enumProduit::_00id), newID.toInt());
    if( !_myModel->submitAll() ) msgCritical("insertion erreur", _myModel->lastError().text());

    _myModel->setFilter("id == -1");
    _myModel->select();
    _myModel->setData(_myModel->index(0, enumProduit::_00id), oldID.toInt());
    if( !_myModel->submitAll() ) msgCritical("insertion erreur", _myModel->lastError().text());

    _myModel->setFilter("");
    //moveDatabaseToRAM();
    int possitionX, possitionY ;
    possitionX = -1 ;
    possitionY = -1 ;
    for ( int i = 0 ; i < produitTable.length() ; i++ )
    {
        if(possitionX != -1 && possitionY != -1)
            break ;

        if( produitTable[i]._00id == newID)
            possitionX = i;

        if( produitTable[i]._00id == oldID)
            possitionY = i;
    }
    itemtable x;
    x = produitTable[possitionX];
    produitTable[possitionX] = produitTable[possitionY];
    produitTable[possitionY] = x ;

    int currentRow = ui->listWidget_categorie->currentRow() ;
    //on_toolButton_refresh_clicked();
    ui->listWidget_categorie->setCurrentRow(-1);
    ui->listWidget_categorie->setCurrentRow(currentRow);

    replace->setDisabled(true);
    prendre->setEnabled(true);
}

void Produit::supprimeCurrentItemSelected()
{
    int currentRow = ui->tableWidget_commande->currentRow() ;
    QString designation = ui->tableWidget_commande->item(currentRow, 0)->text() ;

    int rowCount = ui->tableWidget->rowCount();
    int columnCount = ui->tableWidget->columnCount();
    itemTableWidget *temp = nullptr;
    for ( int i = 0 ; i < rowCount ; i++ )
    {
        for (int j = 0 ; j < columnCount ; j++ )
        {
            temp = (itemTableWidget*)ui->tableWidget->cellWidget(i, j);
            if( temp != nullptr )
            {
                if(temp->_01Designation == designation)
                {
                    temp->setSelected(false);
                    break ;
                }
            }
        }
    }
    ui->tableWidget_commande->removeRow(ui->tableWidget_commande->currentRow());
    calculeLePrixTotal();
}

void Produit::supprimeCurrentItem()
{if(!getSettings("admin").toBool()) return ;
    int row = ui->tableWidget->currentIndex().row() ;
    int column = ui->tableWidget->currentIndex().column() ;

    itemTableWidget *temp = nullptr ;
    temp = (itemTableWidget*)ui->tableWidget->cellWidget(row, column);
    if(temp == nullptr) return ;

    _myModel->setFilter("id == "+ temp->_00id);
    _myModel->select();
    _myModel->removeRow(0) ;
    moveDatabaseToRAM();
    readTableNew();
}

void Produit::modifieCurrentItem()
{if(!getSettings("admin").toBool()) return ;
    ui->stackedWidget->setCurrentIndex(1);
    int row = ui->tableWidget->currentIndex().row() ;
    int column = ui->tableWidget->currentIndex().column() ;

    itemTableWidget *temp = nullptr ;
    temp = (itemTableWidget*)ui->tableWidget->cellWidget(row, column);
    if(temp == nullptr) return ;

    int k = temp->itemRow ;
    /*
    newproduit->setDataToView(_myModel->record(k).value(enumProduit::_00id).toString(),
                              _myModel->record(k).value(enumProduit::_01Designation).toString(),
                              _myModel->record(k).value(enumProduit::_02Categorie).toString(),
                              _myModel->record(k).value(enumProduit::_03Prix_Unit_Achate).toString(),
                              _myModel->record(k).value(enumProduit::_04Prix_Unit_Vente).toString(),
                              _myModel->record(k).value(enumProduit::_05Description).toString(),
                              _myModel->record(k).value(enumProduit::_06Image).toByteArray(),
                              k);
                              */
    newproduit->setDataToView(temp->_00id,
                              temp->_01Designation,
                              temp->_02Categorie,
                              temp->_03Prix_Unit_Achate,
                              temp->_04Prix_Unit_Vente,
                              temp->_05Description,
                              temp->_06Image,
                              k);

}

void Produit::selectedList(QStringList list)
{//useless
    QList<itemtable> selectedList  ;
    int rowCount = ui->tableWidget->rowCount();
    int columnCount = ui->tableWidget->columnCount();
    itemTableWidget *temp = nullptr;
    for ( int i = 0 ; i < rowCount ; i++ )
    {
        for (int j = 0 ; j < columnCount ; j++ )
        {
            temp = (itemTableWidget*)ui->tableWidget->cellWidget(i, j);
            if( temp != nullptr )
            {
                for ( int u = 0 ; u < list.length() ; u++)
                if(temp->_01Designation == list[u])
                {
                    itemtable item;
                    item._00id = temp->_00id ;
                    item._01Designation = temp->_01Designation ;
                    item._02Categorie = temp->_02Categorie ;
                    item._03Prix_Unit_Achate = temp->_03Prix_Unit_Achate ;
                    item._04Prix_Unit_Vente = temp->_04Prix_Unit_Vente ;
                    item._05Description = temp->_05Description ;
                    selectedList << item ;
                    break ;
                }
            }
        }
    }
    //ui->stackedWidget->start(3);
    commande->init(selectedList) ;
    ui->stackedWidget->setLength(len, AnimationStackedWidget::RightToLeft);
}


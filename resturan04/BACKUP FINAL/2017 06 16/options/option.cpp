#include "option.h"
#include "ui_option.h"

Option::Option(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Option)
{
    ui->setupUi(this);

}

Option::~Option()
{
    delete ui;
}

void Option::on_toolButton_back_clicked()
{
    emit backButton();
}

void Option::init()
{

    int gTimeSwitch = getSettings("gTimeSwitch").toInt();
    int itembyCoulumn = getSettings("itembyCoulumn").toInt();
    int calc = getSettings("calc").toInt();
    int itemSize = getSettings("itemSize").toInt();
    ui->spinBox_animation_speed->setValue(gTimeSwitch);
    ui->spinBox_item_by_column->setValue(itembyCoulumn);
    ui->comboBox_calc->setCurrentIndex(calc);
    ui->spinBox_item_size->setValue(itemSize);
}

void Option::on_toolButton_Reset_clicked()
{
    ui->spinBox_animation_speed->setValue(5000);
    ui->spinBox_item_by_column->setValue(3);
    ui->spinBox_item_size->setValue(200);
    ui->comboBox_calc->setCurrentIndex(0);
    on_toolButton_Save_clicked();
}

void Option::on_toolButton_Save_clicked()
{
    setSettings("gTimeSwitch", ui->spinBox_animation_speed->text());
    setSettings("itembyCoulumn", ui->spinBox_item_by_column->text());
    setSettings("calc", ui->comboBox_calc->currentIndex());
    setSettings("itemSize", ui->spinBox_item_size->text());

    on_toolButton_back_clicked();
}

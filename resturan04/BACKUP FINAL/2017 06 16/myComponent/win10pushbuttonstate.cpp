#include "win10pushbuttonstate.h"
#include "ui_win10pushbuttonstate.h"

Win10PushButtonState::Win10PushButtonState(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Win10PushButtonState)
{
    ui->setupUi(this);
}

Win10PushButtonState::~Win10PushButtonState()
{
    delete ui;
}

void Win10PushButtonState::setImage(QString imagePath)
{
    QPixmap pixmap(imagePath);
    int w = ui->label_image->width();
    int h = ui->label_image->height();
    ui->label_image->setPixmap( pixmap.scaled(w, h, Qt::KeepAspectRatio) );
    ui->label_image->setScaledContents(true);
}

void Win10PushButtonState::setLabelText(QString text)
{
    ui->label_text->setText(text);
}

void Win10PushButtonState::setLabelState(QString text)
{
    ui->label_state->setText(text);
}

void Win10PushButtonState::setCSS(QString css)
{
    this->setStyleSheet(css);
}

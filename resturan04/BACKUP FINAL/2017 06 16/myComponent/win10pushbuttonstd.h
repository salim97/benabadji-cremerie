#ifndef WIN10PUSHBUTTONSTD_H
#define WIN10PUSHBUTTONSTD_H

#include <QWidget>
#include <QTimer>

#include "animationstackedwidget.h"

namespace Ui {
class Win10PushButtonStd;
}

class Win10PushButtonStd : public QWidget
{
    Q_OBJECT

public:
    explicit Win10PushButtonStd(QWidget *parent = 0);
    ~Win10PushButtonStd();
    void setImage(QString imagePath);
    void setLabelText(QString text);
    void setLabelTextstate(QString text);
    void setCSS(QString css);

    void startAnimation(int timerIntervalMS, int lenght, QString direction);
private slots:
    void timeout();
protected:
    void resizeEvent(QResizeEvent *event);
private:
    Ui::Win10PushButtonStd *ui;
    QTimer timer;
    bool timerEnabled ;
    QString direction;
    int lenght ;
    void switchAnimation();

};

#endif // WIN10PUSHBUTTONSTD_H

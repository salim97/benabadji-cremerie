#ifndef CALCULATOR_H
#define CALCULATOR_H

#include <QWidget>

namespace Ui {
class Calculator;
}

class Calculator : public QWidget
{
    Q_OBJECT

public:
    explicit Calculator(QWidget *parent = 0);
    ~Calculator();
    bool showed ;
private slots:
    void digi_pressed();
    void on_pushButton_decimal_released();

    void on_pushButton_clear_clicked();

    void unary_operation_pressed();
    void on_pushButton_equals_released();
    void binary_operation_released() ;
private:
    Ui::Calculator *ui;
    double firstNum ;
    bool userIsTypingSecondNumber;
};

#endif // CALCULATOR_H

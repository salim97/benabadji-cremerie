#include "calculator.h"
#include "ui_calculator.h"

Calculator::Calculator(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Calculator)
{
    ui->setupUi(this);
    ui->label->setText("0");
    QFont fnt;
    fnt.setPointSize(17);
    fnt.setFamily("Arial");
    ui->label->setFont(fnt);
    this->setWindowTitle("CALCULATRICE");

    connect(ui->pushButton_0, SIGNAL(released()), this, SLOT(digi_pressed())) ;
    connect(ui->pushButton_1, SIGNAL(released()), this, SLOT(digi_pressed())) ;
    connect(ui->pushButton_2, SIGNAL(released()), this, SLOT(digi_pressed())) ;
    connect(ui->pushButton_3, SIGNAL(released()), this, SLOT(digi_pressed())) ;
    connect(ui->pushButton_4, SIGNAL(released()), this, SLOT(digi_pressed())) ;
    connect(ui->pushButton_5, SIGNAL(released()), this, SLOT(digi_pressed())) ;
    connect(ui->pushButton_6, SIGNAL(released()), this, SLOT(digi_pressed())) ;
    connect(ui->pushButton_7, SIGNAL(released()), this, SLOT(digi_pressed())) ;
    connect(ui->pushButton_8, SIGNAL(released()), this, SLOT(digi_pressed())) ;
    connect(ui->pushButton_9, SIGNAL(released()), this, SLOT(digi_pressed())) ;

    connect(ui->pushButton_plusMinus, SIGNAL(released()), this, SLOT(unary_operation_pressed()) );
    connect(ui->pushButton_percent, SIGNAL(released()), this, SLOT(unary_operation_pressed()) );

    connect(ui->pushButton_add, SIGNAL(released()), this, SLOT(binary_operation_released()) );
    connect(ui->pushButton_substract, SIGNAL(released()), this, SLOT(binary_operation_released()) );
    connect(ui->pushButton_multiply, SIGNAL(released()), this, SLOT(binary_operation_released()) );
    connect(ui->pushButton_divide, SIGNAL(released()), this, SLOT(binary_operation_released()) );

    ui->pushButton_add->setCheckable(true);
    ui->pushButton_substract->setCheckable(true);
    ui->pushButton_multiply->setCheckable(true);
    ui->pushButton_divide->setCheckable(true);

    ui->pushButton_add->setChecked(false);
    ui->pushButton_substract->setChecked(false);
    ui->pushButton_multiply->setChecked(false);
    ui->pushButton_divide->setChecked(false);
}

Calculator::~Calculator()
{
    delete ui;
}

void Calculator::digi_pressed()
{
    QPushButton *button = (QPushButton*)sender() ;
    double labelNumbre ;
    QString newLabel ;
    if((ui->pushButton_add->isChecked() || ui->pushButton_substract->isChecked()
            || ui->pushButton_multiply->isChecked() || ui->pushButton_divide->isChecked()) && (!userIsTypingSecondNumber) )
    {
        labelNumbre = button->text().toDouble() ;
        userIsTypingSecondNumber = true ;
        newLabel = QString::number(labelNumbre,'g', 15) ;
    }
    else
    {
        if(ui->label->text().contains('.') && button->text() == "0" )
        {
            newLabel = ui->label->text() + button->text() ;
        }
        else
        {
            labelNumbre = (ui->label->text() + button->text()).toDouble() ;
            newLabel = QString::number(labelNumbre,'g', 15) ;
        }

    }


    ui->label->setText(newLabel);
}

void Calculator::on_pushButton_decimal_released()
{
    if(!ui->label->text().contains("."))
        ui->label->setText(ui->label->text()+".");
}

void Calculator::on_pushButton_clear_clicked()
{
    ui->label->setText("0");
    userIsTypingSecondNumber = false ;

    ui->pushButton_add->setChecked(false);
    ui->pushButton_substract->setChecked(false);
    ui->pushButton_multiply->setChecked(false);
    ui->pushButton_divide->setChecked(false);
}

void Calculator::unary_operation_pressed()
{
    QPushButton *button = (QPushButton*)sender() ;
    double labelNumbre ;
    QString newLabel ;
    if(button->text() == "+/-")
    {
        labelNumbre = ui->label->text().toDouble() ;
        labelNumbre = labelNumbre * -1 ;
        newLabel = QString::number(labelNumbre, 'g', 15) ;
        ui->label->setText(newLabel);
    }

    if(button->text() == "%")
    {
        labelNumbre = ui->label->text().toDouble() ;
        labelNumbre = labelNumbre * 0.01 ;
        newLabel = QString::number(labelNumbre, 'g', 15) ;
        ui->label->setText(newLabel);
    }
}

void Calculator::on_pushButton_equals_released()
{
    double labelNumbre, secondNum ;
    secondNum = ui->label->text().toDouble() ;
    if(ui->pushButton_add->isChecked())
    {
        labelNumbre = firstNum + secondNum ;
        ui->pushButton_add->setChecked(false);
    }
    else if(ui->pushButton_substract->isChecked())
    {
        labelNumbre = firstNum - secondNum ;
        ui->pushButton_substract->setChecked(false);
    }
    else if(ui->pushButton_multiply->isChecked())
    {
        labelNumbre = firstNum * secondNum ;
        ui->pushButton_multiply->setChecked(false);
    }
    else if(ui->pushButton_divide->isChecked())
    {
        if(secondNum != 0)
            labelNumbre = firstNum / secondNum ;
        else
            labelNumbre = 0 ;
        ui->pushButton_divide->setChecked(false);
    }
    QString newLabel = QString::number(labelNumbre, 'g', 15) ;
    ui->label->setText(newLabel);
    userIsTypingSecondNumber = false ;
}

void Calculator::binary_operation_released()
{
    QPushButton *button = (QPushButton*)sender() ;
    firstNum = ui->label->text().toDouble() ;
/*    if(ui->pushButton_add->isChecked() || ui->pushButton_divide->isChecked()
            || ui->pushButton_multiply->isChecked() || ui->pushButton_substract->isChecked())
    {
        on_pushButton_equals_released();
        QString temp = ui->label->text() ;
        on_pushButton_clear_clicked();
        ui->label->setText(temp);
    }
*/

    button->setChecked(true);
}

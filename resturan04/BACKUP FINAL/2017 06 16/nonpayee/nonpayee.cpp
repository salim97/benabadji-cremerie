#include "nonpayee.h"
#include "ui_nonpayee.h"

#include <QWidgetAction>

NonPayee::NonPayee(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::NonPayee)
{
    ui->setupUi(this);


    _myModel->setTable(enumNoPayee::tableName);
    _myModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
    _myModel->select() ;
    ui->tableView->setModel(_myModel);
    //ui->comboBox_type->addItems(_mapColumns[enumNoPayee]);
    //ui->comboBox_type->removeItem(0);

    ui->toolButton_new_product->setDisabled(true);
    ui->toolButton_remove->setDisabled(true);
    initAction();
}

NonPayee::~NonPayee()
{
    delete ui;
}

void NonPayee::initAction()
{

    // init widget action

    QAction  *supprime, *valide;
    supprime = new QAction("Supprime", this);
    valide = new QAction("Valide", this);

    //Icon

    supprime->setIcon(ui->toolButton_remove->icon());
    valide->setIcon(ui->toolButton_new_product->icon());

    // on click
    connect(supprime, SIGNAL(triggered(bool)), this, SLOT(on_toolButton_remove_clicked()));
    connect(valide, SIGNAL(triggered(bool)), this, SLOT(on_toolButton_new_product_clicked()));

    // init table with action
    ui->tableView->setContextMenuPolicy(Qt::ActionsContextMenu);
    ui->tableView->addAction(valide);
    ui->tableView->addAction(supprime);


}
void NonPayee::on_toolButton_back_clicked()
{
    if(_myModel->tableName() == enumCommande::tableName)
        init();
    else
        emit backButton();
}

void NonPayee::init()
{
    _myModel->setTable(enumNoPayee::tableName);
    _myModel->setFilter("");
    _myModel->select() ;
    ui->tableView->hideColumn(enumNoPayee::_00id);
    ui->tableView->hideColumn(enumNoPayee::_04Prix_ntkamlek);
    ui->tableView->resizeColumnsToContents();
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->toolButton_new_product->setEnabled(true);
}

void NonPayee::on_tableView_clicked(const QModelIndex &index)
{
    ui->toolButton_new_product->setEnabled(true);
    ui->toolButton_remove->setEnabled(true);
}

void NonPayee::on_toolButton_remove_clicked()
{
    if(_myModel->tableName() == enumNoPayee::tableName)
    {
    int currentRow = ui->tableView->currentIndex().row() ;
    QString id = _myModel->record(currentRow).value(enumNoPayee::_00id).toString() ;
    _myModel->removeRow(currentRow);
    if( !_myModel->submitAll() ) msgCritical("insertion erreur", _myModel->lastError().text());

    QSqlTableModel  tempModel;
    tempModel.setTable(enumCommande::tableName);
    tempModel.setEditStrategy(QSqlTableModel::OnManualSubmit);
    tempModel.setFilter("id_facture == '"+id+"'");
    tempModel.select() ;
    for ( int i = 0 ; i < tempModel.rowCount() ; i++ )
        tempModel.removeRow(i);
    if( !tempModel.submitAll() ) msgCritical("insertion erreur", tempModel.lastError().text());
    }
    else
    {
        int currentRow = ui->tableView->currentIndex().row() ;
        _myModel->removeRow(currentRow);
        if( !_myModel->submitAll() ) msgCritical("insertion erreur", _myModel->lastError().text());
    }
}

void NonPayee::on_toolButton_new_product_clicked()
{
    QSqlTableModel temp;

    temp.setTable(enumTransaction::tableName);
    temp.setEditStrategy(QSqlTableModel::OnManualSubmit);
    temp.select();
    int row = temp.rowCount() ;
    temp.insertRow(row) ;

    temp.setData(temp.index(row, enumTransaction::_01Date), getCurrentDateTime());
    temp.setData(temp.index(row, enumTransaction::_02Prix),
                      _myModel->record(ui->tableView->currentIndex().row()).value(enumNoPayee::_03Montant_Total).toString());
    temp.setData(temp.index(row, enumTransaction::_03Prix_ntkamlek),
                      _myModel->record(ui->tableView->currentIndex().row()).value(enumNoPayee::_04Prix_ntkamlek).toString());

    if( !temp.submitAll() ) msgCritical("insertion erreur Transaction", temp.lastError().text());
    //TODO : gla3 hadi
    int currentRow = ui->tableView->currentIndex().row() ;
    QString id = _myModel->record(currentRow).value(enumNoPayee::_00id).toString() ;
    _myModel->removeRow(currentRow);
    if( !_myModel->submitAll() ) msgCritical("insertion erreur", _myModel->lastError().text());
    QSqlTableModel  tempModel;
    tempModel.setTable(enumCommande::tableName);
    tempModel.setEditStrategy(QSqlTableModel::OnManualSubmit);
    tempModel.setFilter("id_facture == '"+id+"'");
    tempModel.select() ;
    id = temp.record(row).value(enumTransaction::_00id).toString() ;
    for ( int i = 0 ; i < tempModel.rowCount() ; i++ )
        tempModel.setData(tempModel.index(i, enumCommande::_01id_facture), id);
    if( !tempModel.submitAll() ) msgCritical("insertion erreur", tempModel.lastError().text());

}

QString NonPayee::montantTotal()
{
    int total =0 ;
    for (int i = 0 ; i < _myModel->rowCount() ; i++ )
    {
        total += _myModel->record(i).value(enumNoPayee::_03Montant_Total).toInt() ;
    }
    return QString::number(total) ;
}


void NonPayee::on_tableView_doubleClicked(const QModelIndex &index)
{
    QString id = _myModel->record(index.row()).value(enumNoPayee::_00id).toString() ;
    _myModel->setTable(enumCommande::tableName);
    _myModel->setFilter("id_facture == '"+id+"'");
    qDebug() << "_myModel->filter() " << _myModel->filter() << id;

    _myModel->select() ;
    ui->tableView->hideColumn(0);
    ui->tableView->hideColumn(1);
    ui->toolButton_new_product->setDisabled(true);
}

#include "commande.h"
#include "ui_commande.h"

#include <QPainter>
#include <QPrintDialog>
#include <QPrinter>
#include <QSpinBox>
#include <QTextDocument>
#include <QTextEdit>

Commande::Commande(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Commande)
{
    ui->setupUi(this);
    ui->tableWidget->horizontalHeader()->setVisible(true);
}

Commande::~Commande()
{
    delete ui;
}

void Commande::init(QList<itemtable> selectedList)
{
    //ui->tableWidget->setColumnCount(4);
    int rowCount = selectedList.count() ;
    ui->tableWidget->setRowCount(0);
    ui->tableWidget->setRowCount(rowCount);
    int x = 0 ;
    int y = 0 ;
    for (int i = 0 ; i < rowCount ; i++ )
    {
        {
            QTableWidgetItem *temp=new QTableWidgetItem (selectedList[i]._01Designation);
            ui->tableWidget->setItem(i, 0, temp);
        }

        {
            QSpinBox *temp= new QSpinBox(ui->tableWidget);
            temp->setMinimum(0);
            temp->setMaximum(999999999);
            temp->setValue(selectedList[i]._05Description.toInt());
            temp->setSingleStep(1);
            ui->tableWidget->setCellWidget(i, 1, temp);
            connect(temp, SIGNAL(valueChanged(int)), this, SLOT(valueChanged()) );
        }

        {

            QSpinBox *temp= new QSpinBox(ui->tableWidget);
            temp->setMinimum(0);
            temp->setMaximum(999999999);
            temp->setValue(selectedList[i]._04Prix_Unit_Vente.toInt());
            temp->setSingleStep(10);
            ui->tableWidget->setCellWidget(i, 2, temp);
            connect(temp, SIGNAL(valueChanged(int)), this, SLOT(valueChanged()) );
        }

        {
            QSpinBox *temp= new QSpinBox(ui->tableWidget);
            temp->setMinimum(0);
            temp->setMaximum(999999999);
            temp->setValue(selectedList[i]._04Prix_Unit_Vente.toInt() * selectedList[i]._05Description.toInt() );
            temp->setSingleStep(10);
            ui->tableWidget->setCellWidget(i, 3, temp);
            x += selectedList[i]._04Prix_Unit_Vente.toInt() * selectedList[i]._05Description.toInt() ;
        }

        {
            QSpinBox *temp= new QSpinBox(ui->tableWidget);
            temp->setMinimum(0);
            temp->setMaximum(999999999);
            temp->setValue(selectedList[i]._03Prix_Unit_Achate.toInt());
            temp->setSingleStep(10);
            ui->tableWidget->setCellWidget(i, 4, temp);
            connect(temp, SIGNAL(valueChanged(int)), this, SLOT(valueChanged()) );
        }

        {
            QSpinBox *temp= new QSpinBox(ui->tableWidget);
            temp->setMinimum(0);
            temp->setMaximum(999999999);
            temp->setValue(selectedList[i]._03Prix_Unit_Achate.toInt() * selectedList[i]._05Description.toInt());
            temp->setSingleStep(10);
            ui->tableWidget->setCellWidget(i, 5, temp);
            y += selectedList[i]._03Prix_Unit_Achate.toInt() * selectedList[i]._05Description.toInt() ;
        }

    }
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableWidget->resizeColumnsToContents();

    ui->label_prix_total->setText(QString::number(x));
    ui->label_2->setText(QString::number(y));
}

void Commande::initQuantite(QStringList idList, QStringList quantiteList)
{
    for (int i = 0 ; i < ui->tableWidget->rowCount() ; i++ )
    {
        for (int j = 0 ; j < idList.length() ; j++ )
        {
            if(ui->tableWidget->item(i, 0)->text() == idList[j])
            {
                QSpinBox *quantite = (QSpinBox*)ui->tableWidget->cellWidget(i, 1);
                QString temp = quantiteList[j] ;
                quantite->setValue(temp.toInt());
                break ;
            }

        }
    }
}

int Commande::tableCount()
{
    return ui->tableWidget->rowCount() ;
}

void Commande::on_toolButton_back_clicked()
{
    ui->tableWidget->setRowCount(0);
    emit backButton();
}

void Commande::valueChanged()
{
    int somme = 0 , sommeNK = 0;
    for ( int i = 0 ; i < ui->tableWidget->rowCount() ; i++ )
    {

        QSpinBox *quantite = (QSpinBox*)ui->tableWidget->cellWidget(i, 1);
        QSpinBox *prix = (QSpinBox*)ui->tableWidget->cellWidget(i, 2);
        QSpinBox *total = (QSpinBox*)ui->tableWidget->cellWidget(i, 3);
        QSpinBox *PUA = (QSpinBox*)ui->tableWidget->cellWidget(i, 4);
        QSpinBox *puaTotal = (QSpinBox*)ui->tableWidget->cellWidget(i, 5);
        total->setValue(quantite->value() * prix->value() );
        puaTotal->setValue(quantite->value() * PUA->value());
        somme += total->value() ;
        sommeNK += puaTotal->value() ;
    }
    ui->label_prix_total->setText(QString::number(somme));
    ui->label_2->setText(QString::number(sommeNK));
}

void Commande::on_toolButton_save_clicked()
{
    _myModel->setTable(enumNoPayee::tableName);
    _myModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
    _myModel->select() ;
    QString nom = getlastclient() ;
    gnom = nom ;
    int row = _myModel->rowCount() ;
    _myModel->insertRow(row);

    _myModel->setData(_myModel->index(row, enumNoPayee::_01Date), getCurrentDateTime());
    _myModel->setData(_myModel->index(row, enumNoPayee::_02Nom), nom);
    _myModel->setData(_myModel->index(row, enumNoPayee::_03Montant_Total), ui->label_prix_total->text());
    _myModel->setData(_myModel->index(row, enumNoPayee::_04Prix_ntkamlek), ui->label_2->text() );
    _myModel->setData(_myModel->index(row, enumNoPayee::_05NumeroTable), this->selectedTable );

    if( !_myModel->submitAll() ) msgCritical("insertion erreur", _myModel->lastError().text());
    /*--------*/
    QString lastId = _myModel->record(row).value(enumNoPayee::_00id).toString() ;
    _myModel->setTable(enumCommande::tableName);
    _myModel->select();

    row = _myModel->rowCount() ;

    QSpinBox *quantite, *total;
    qDebug() << "ui->tableWidget->rowCount() " << ui->tableWidget->rowCount() ;
    for ( int i = 0 ; i < ui->tableWidget->rowCount() ; i++ )
    {
        quantite = (QSpinBox*)ui->tableWidget->cellWidget(i, 1);
        total = (QSpinBox*)ui->tableWidget->cellWidget(i, 3);

        _myModel->insertRow(row);

       _myModel->setData(_myModel->index(row, enumCommande::_01id_facture), lastId);

       _myModel->setData(_myModel->index(row, enumCommande::_02Designation),
                         ui->tableWidget->item(i, 0)->text());

       _myModel->setData(_myModel->index(row, enumCommande::_03Quantite),
                         quantite->text());
       _myModel->setData(_myModel->index(row, enumCommande::_04Montant_Total),
                         total->text());

       row++ ;
    }

    if( !_myModel->submitAll() ) msgCritical("insertion erreur", _myModel->lastError().text());
    //on_toolButton_back_clicked();
}
QString Commande::getlastclient()
{
    //_myModel->setFilter("Verse == 'NO' ");
    //_myModel->select() ;
    QStringList list ;
    for (int i = 0 ; i < _myModel->rowCount() ; i++ )
    {
        list << _myModel->record(i).value(enumNoPayee::_02Nom).toString().replace("client", "") ;
    }
    if(list.isEmpty())
        return "client1" ;
    int max = 1 ;
    QString item ;
    for (int i = 0 ; i < list.length() ; i++ )
    {
        item = list[i] ;
        if( item.toInt() > max )
            max = item.toInt() ;
    }
    max++ ;
    QString temp = "client"+QString::number(max) ;
    return temp ;
}

void Commande::on_toolButton_saveToTransaction()
{
    /*
    _myModel->setTable(enumNoPayee::tableName);
    _myModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
    _myModel->select() ;
    QString nom = getlastclient() ;
    gnom = nom ;
    int row = _myModel->rowCount() ;
    _myModel->insertRow(row);

    _myModel->setData(_myModel->index(row, enumNoPayee::_01Date), getCurrentDateTime());
    _myModel->setData(_myModel->index(row, enumNoPayee::_02Nom), nom);
    _myModel->setData(_myModel->index(row, enumNoPayee::_03Montant_Total), ui->label_prix_total->text());
    _myModel->setData(_myModel->index(row, enumNoPayee::_04Prix_ntkamlek), ui->label_2->text() );
    _myModel->setData(_myModel->index(row, enumNoPayee::_05NumeroTable), this->selectedTable );

    if( !_myModel->submitAll() ) msgCritical("insertion erreur", _myModel->lastError().text());

    QString lastId = _myModel->record(row).value(enumNoPayee::_00id).toString() ;
    _myModel->setTable(enumCommande::tableName);
    _myModel->select();

    row = _myModel->rowCount() ;

    QSpinBox *quantite, *total;
    qDebug() << "ui->tableWidget->rowCount() " << ui->tableWidget->rowCount() ;
    for ( int i = 0 ; i < ui->tableWidget->rowCount() ; i++ )
    {
        quantite = (QSpinBox*)ui->tableWidget->cellWidget(i, 1);
        total = (QSpinBox*)ui->tableWidget->cellWidget(i, 3);

        _myModel->insertRow(row);

       _myModel->setData(_myModel->index(row, enumCommande::_01id_facture), lastId);

       _myModel->setData(_myModel->index(row, enumCommande::_02Designation),
                         ui->tableWidget->item(i, 0)->text());

       _myModel->setData(_myModel->index(row, enumCommande::_03Quantite),
                         quantite->text());
       _myModel->setData(_myModel->index(row, enumCommande::_04Montant_Total),
                         total->text());

       row++ ;
    }

    if( !_myModel->submitAll() ) msgCritical("insertion erreur", _myModel->lastError().text());
    //on_toolButton_back_clicked();
    */
}

void Commande::on_toolButton_imprimie_clicked()
{


    char n = 10 ;
    QString html ;
    html += "<html><font size='10'>" ;
    html +="<h1><font size='5'>Mon Dessert Creperie -Oran</font></h1>";
    html +="<h1><font size='3'>Date: "+ getCurrentDateTime()+ "</font></h1>";
    //html += "<table border='0' style='width:100%'>" ;
    html += "<table style='border-collapse: collapse;' width=100%>" ;

    /*
    html += "<tr>" ;
    html += "<td><font size='4'>Designation</font></td>" ;
    html += "<td><font size='4'>Quantite</font></td>" ;
    html += "<td><font size='4'>Prix</font></td>" ;
    html += "</tr>" ;
*/
    QSpinBox *quantite, *prix, *total;
    for ( int i = 0 ; i < ui->tableWidget->rowCount() ; i++ )
    {
        quantite = (QSpinBox*)ui->tableWidget->cellWidget(i, 1);
        prix = (QSpinBox*)ui->tableWidget->cellWidget(i, 2);
        total = (QSpinBox*)ui->tableWidget->cellWidget(i, 3);
        html += "<tr>" ;
        html += "<td><font size='4'><center>   "+quantite->text()+"   </center></font></td>";

        html += "<td><font size='4'>"+ ui->tableWidget->item(i, 0)->text()+"</font></td>" ;

        html += "<td><font size='4'><center>   "+prix->text()+" DA</center></font></td>";

        html += "</tr>" ;
    }
    html +="</table><p><font size='4'><center>Montant total: "+ui->label_prix_total->text()+" DA  </center></font></p>";
    html += "<p align='right'><font size='4'>"+gnom+" "+selectedTable+"         sois le bienvenu.</font></p></font></html>" ;
    QPrinter printer(QPrinter::ScreenResolution);
//    printer.setPageSize(QPrinter::A5);
    //printer.setPaperSize(QSizeF(210, 279), QPrinter::Millimeter);
    //printer.setOrientation(QPrinter::Portrait);

    //printer.setFullPage(true);
//    printer.setPageMargins (1,1,0.25,25,QPrinter::Millimeter);


    QPrintDialog *dialog = new QPrintDialog(&printer);
    dialog->setWindowTitle("Print Document");

    //if (dialog->exec() != QDialog::Accepted)
      //  return ;
    printer.setPaperSize(QSizeF(72, 297), QPrinter::Millimeter);

    //QRectF rect = printer.pageRect();
    //double margin = rect.width() * 0.05 ;
    //QRectF body = QRectF(margin, margin, rect.width()-margin*2, rect.height()-margin*2) ;

    //printer.setPageMargins (margin,margin,rect.width()-margin*2,rect.height()-margin*2 ,QPrinter::Millimeter);
    printer.setFullPage(true);
    QTextDocument x;
    x.setPageSize(QSizeF(printer.pageRect().size()));
    x.setHtml(html);
    //x.setDefaultStyleSheet("* {font-size: 130}");
    //QFont fnt;
    //fnt.setPointSize(14);
    //fnt.setFamily("Arial");
    //x.setFont(fnt);
    //x.adjustSize();
    //QPainter p ;
    //p.begin(&printer);
    //p.setFont(QFont("Arial", 10 ));

    //x.drawContents(&p);

    x.print(&printer);
/*
    QPainter painter;
    painter.begin(&printer);
    qDebug() << "01 ";
    //painter.drawText(100, 100, 500, 500, Qt::AlignCenter|Qt::AlignTop, text);
    painter.setFont(QFont("Arial",14));
    //painter.drawText();

    QTextDocument doc ;
    doc.setHtml(html);
    doc.drawContents(&painter);
    doc.adjustSize();
    doc.setPageSize(printer.paperSize(QPrinter::Point));
    //painter.drawText(200,200,html);
    qDebug() << "01 ";
    painter.end();
    */


}

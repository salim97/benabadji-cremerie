#ifndef SELECTIONPRODUIT_H
#define SELECTIONPRODUIT_H

#include <QWidget>
#include "itemtable.h"
#include "myparentobject.h"

namespace Ui {
class SelectionProduit;
}

class SelectionProduit : public QWidget, public myParentObject
{
    Q_OBJECT

public:
    explicit SelectionProduit(QWidget *parent = 0);
    ~SelectionProduit();
    init();
signals:
    void backButton();
    void selectedList(QStringList list) ;
private slots:
    void on_toolButton_back_clicked();

    void on_listWidget_clicked(const QModelIndex &index);

    void on_toolButton_valid_clicked();

    void on_lineEdit_search_textChanged(const QString &arg1);

    void on_comboBox_type_currentIndexChanged(int index);

private:
    Ui::SelectionProduit *ui;
};

#endif // SELECTIONPRODUIT_H

#ifndef QUICKCOMMANDE_H
#define QUICKCOMMANDE_H

#include <QDialog>
#include "itemtable.h"
#include "myparentobject.h"

namespace Ui {
class QuickCommande;
}

class QuickCommande : public QDialog, public myParentObject
{
    Q_OBJECT

public:
    explicit QuickCommande(QWidget *parent = 0);
    ~QuickCommande();
        QStringList list, listPUA, listPUV;
        void init(QString filter);

        void getSelectedItems();
        bool topping = false ;
        QList<itemtable> getSelectedItems2();
private slots:
    void on_pushButton_cancel_clicked();

    void on_pushButton_ok_clicked();

    void on_tableWidget_clicked(const QModelIndex &index);

private:
    Ui::QuickCommande *ui;

};

#endif // QUICKCOMMANDE_H

#include "itemtablewidget.h"
#include "quickcommande.h"
#include "ui_quickcommande.h"

#include <QInputDialog>
#include <QApplication>
#include <QDesktopWidget>

QuickCommande::QuickCommande(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::QuickCommande)
{
    ui->setupUi(this);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectItems);
    ui->tableWidget->setSelectionMode( QAbstractItemView::NoSelection );
    ui->tableWidget->setFocusPolicy(Qt::NoFocus);
    QHeaderView *verticalHeader = ui->tableWidget->verticalHeader();
    verticalHeader->setSectionResizeMode(QHeaderView::Fixed);
    verticalHeader->setDefaultSectionSize(200);

}

QuickCommande::~QuickCommande()
{
    delete ui;
}

void QuickCommande::on_pushButton_cancel_clicked()
{
    this->close();
}

void QuickCommande::on_pushButton_ok_clicked()
{
    getSelectedItems();
    this->close() ;
}

void QuickCommande::init(QString filter)
{

    int columnCount = 3; //getSettings("itembyCoulumn").toInt();
    _myModel->setTable(enumProduit::tableName);
    _myModel->setFilter(_mapColumns[enumProduit::tableName][enumProduit::_02Categorie]+" LIKE '%"+filter+"%'");
    _myModel->select() ;

    int rowT = _myModel->rowCount() ;
    int item_by_column = columnCount ;

    int column, row ;

    if( rowT > item_by_column )
        column = item_by_column ;
    else
        column = rowT ;

    row = rowT / item_by_column ;

    if( ( rowT % item_by_column ) > 0 )
        row++;

    ui->tableWidget->setColumnCount(column);
    ui->tableWidget->setRowCount(row);

    int currentRow = 0 ;

    for ( int i = 0 ; i < row ; i++ )
    {
        for ( int j = 0 ; j < column ; j++)
        {
            itemTableWidget *mycell = nullptr ;
            mycell = new itemTableWidget(this);
            mycell->setDataToView(_myModel->record(currentRow).value(enumProduit::_00id).toString(),
                                  _myModel->record(currentRow).value(enumProduit::_01Designation).toString(),
                                  _myModel->record(currentRow).value(enumProduit::_02Categorie).toString(),
                                  _myModel->record(currentRow).value(enumProduit::_03Prix_Unit_Achate).toString(),
                                  _myModel->record(currentRow).value(enumProduit::_04Prix_Unit_Vente).toString(),
                                  _myModel->record(currentRow).value(enumProduit::_05Description).toString(),
                                  _myModel->record(currentRow).value(enumProduit::_06Image).toByteArray(),
                                  currentRow);

            mycell->setSelected(false);
            ui->tableWidget->setCellWidget(i,j, mycell);
            currentRow++;
        }
    }
    ui->tableWidget->resizeColumnsToContents();
}

void QuickCommande::getSelectedItems()
{
    QStringList selectedList ;
    int rowCount = ui->tableWidget->rowCount();
    int columnCount = ui->tableWidget->columnCount();
    itemTableWidget *temp = nullptr;

    for ( int i = 0 ; i < rowCount ; i++ )
    {
        for (int j = 0 ; j < columnCount ; j++ )
        {
            temp = (itemTableWidget*)ui->tableWidget->cellWidget(i, j);
            if( temp != nullptr )
            {
                if(temp->isSelected())
                {

                    list <<  temp->_01Designation ;
                    listPUA <<  temp->_03Prix_Unit_Achate ;
                    listPUV <<  temp->_04Prix_Unit_Vente ;
                }
            }
        }
    }

}

void QuickCommande::on_tableWidget_clicked(const QModelIndex &index)
{
    if(topping)
    {
    bool ok ;
    int x = QInputDialog::getInt(this, "dialog", "Quantite:", 1, 1, 999999999,1,&ok);
    if(ok)
    {
        itemTableWidget *temp = nullptr;
        temp = (itemTableWidget*)ui->tableWidget->cellWidget(index.row(), index.column());
        if(temp != nullptr)
            temp->setSelected(!temp->isSelected())  ;
        temp->quantite = x;
    }
    }
    else
    {
        itemTableWidget *temp = nullptr;
        temp = (itemTableWidget*)ui->tableWidget->cellWidget(index.row(), index.column());
        if(temp != nullptr)
            temp->setSelected(!temp->isSelected())  ;

    }

}

QList<itemtable> QuickCommande::getSelectedItems2()
{
    QList<itemtable> selectedList ;
    int rowCount = ui->tableWidget->rowCount();
    int columnCount = ui->tableWidget->columnCount();
    itemTableWidget *temp = nullptr;

    for ( int i = 0 ; i < rowCount ; i++ )
    {
        for (int j = 0 ; j < columnCount ; j++ )
        {
            temp = (itemTableWidget*)ui->tableWidget->cellWidget(i, j);
            if( temp != nullptr )
            {
                if(temp->isSelected())
                {
                    itemtable item;
                    item._00id = temp->_00id ;
                    item._01Designation = temp->_01Designation ;
                    item._02Categorie = temp->_02Categorie ;
                    item._03Prix_Unit_Achate = temp->_03Prix_Unit_Achate ;
                    item._04Prix_Unit_Vente = temp->_04Prix_Unit_Vente ;
                    item._05Description = temp->_05Description ;
                    item.quantite = temp->quantite ;
                    selectedList << item ;
                }
            }
        }
    }
    return selectedList ;
}

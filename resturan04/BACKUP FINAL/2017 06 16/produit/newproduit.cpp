#include "newproduit.h"
#include "ui_newproduit.h"

#include <QBuffer>
#include <QKeyEvent>

NewProduit::NewProduit(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::NewProduit)
{
    ui->setupUi(this);
    _myModel->setTable(enumProduit::tableName);
    _myModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
    _myModel->select();
    QString columnname=_mapColumns[enumProduit::tableName][enumProduit::_02Categorie] ;
    ui->comboBox_Categorie->addItems(getColumnFromTableWithoutDuplicates(enumProduit::tableName,columnname ));

}

NewProduit::~NewProduit()
{
    delete ui;
}

void NewProduit::setDataToView(QString _00id, QString _01Designation,
                               QString _02Categorie, QString _03Prix_Unit_Achate,
                               QString _04Prix_Unit_Vente, QString _05Description,
                               QByteArray _06Image, int itemRow)
{
    this->_00id = _00id;
    this->_01Designation = _01Designation ;
    this->_02Categorie = _02Categorie ;
    this->_03Prix_Unit_Achate = _03Prix_Unit_Achate ;
    this->_04Prix_Unit_Vente = _04Prix_Unit_Vente ;
    this->_05Description = _05Description ;
    this->_06Image = _06Image ;
    this->itemRow = itemRow ;


    modifie = true ;
    ui->lineEdit_Designation->setText(_01Designation);
    for ( int i = 0 ; i < ui->comboBox_Categorie->count(); i++ )
    {
        if( _02Categorie == ui->comboBox_Categorie->itemText(i) )
        {
            ui->comboBox_Categorie->setCurrentIndex(i);
            break ;
        }
    }
    ui->spinBox_PUA->setValue(_03Prix_Unit_Achate.toInt());
    ui->spinBox_PUV->setValue(_04Prix_Unit_Vente.toInt());
    ui->textEdit_description->setPlainText(_05Description);
    //ui->label_image->setText("STORED ON DATABASE");
}



void NewProduit::on_toolButton_back_clicked()
{
    on_toolButton_reset_clicked();
    emit backButton();
}

void NewProduit::on_pushButton_browse_clicked()
{
    QString filter = "All files (*.*) " ;
    QString DefaultFolderPath = "c:/Users/unix/Documents/resturan01/pizza/"; // TODO : change each time for the last folder
    QString filename = QFileDialog::getOpenFileName(this,"open a file", DefaultFolderPath, filter );
    if ( filename.isEmpty() ) return ;
    ui->label_image->setText(filename);
}

void NewProduit::on_toolButton_valide_clicked()
{
    QString imagePath =  ui->label_image->text();


    QPixmap pixmap;
    QByteArray bArray;

    if( !imagePath.isEmpty() )
    {
        pixmap.load(imagePath);

        QBuffer buffer(&bArray);
        buffer.open(QIODevice::WriteOnly);
        if(imagePath.contains(".png"))
            pixmap.save(&buffer, "PNG");
        if(imagePath.contains(".PNG"))
            pixmap.save(&buffer, "PNG");

        if(imagePath.contains(".jpg"))
            pixmap.save(&buffer, "jpg");
        if(imagePath.contains(".JPG"))
            pixmap.save(&buffer, "JPG");
    }

    if(modifie)
    {
        _myModel->setData(_myModel->index(itemRow, enumProduit::_01Designation), ui->lineEdit_Designation->text());
        _myModel->setData(_myModel->index(itemRow, enumProduit::_02Categorie), ui->comboBox_Categorie->currentText());
        _myModel->setData(_myModel->index(itemRow, enumProduit::_03Prix_Unit_Achate), ui->spinBox_PUA->text());
        _myModel->setData(_myModel->index(itemRow, enumProduit::_04Prix_Unit_Vente), ui->spinBox_PUV->text());
        _myModel->setData(_myModel->index(itemRow, enumProduit::_05Description), ui->textEdit_description->toPlainText());
        if(!imagePath.isEmpty())
            _myModel->setData(_myModel->index(itemRow, enumProduit::_06Image), bArray);


    }
    else
    {
        int row = _myModel->rowCount() ;
        _myModel->insertRow(row);

        _myModel->setData(_myModel->index(row, enumProduit::_01Designation), ui->lineEdit_Designation->text());
        _myModel->setData(_myModel->index(row, enumProduit::_02Categorie), ui->comboBox_Categorie->currentText());
        _myModel->setData(_myModel->index(row, enumProduit::_03Prix_Unit_Achate), ui->spinBox_PUA->text());
        _myModel->setData(_myModel->index(row, enumProduit::_04Prix_Unit_Vente), ui->spinBox_PUV->text());
        _myModel->setData(_myModel->index(row, enumProduit::_05Description), ui->textEdit_description->toPlainText());
        _myModel->setData(_myModel->index(row, enumProduit::_06Image), bArray);
    }


    if( !_myModel->submitAll() ) msgCritical("insertion erreur", _myModel->lastError().text());
    modifie = false ;
    on_toolButton_back_clicked();
}


void NewProduit::on_toolButton_reset_clicked()
{
    ui->lineEdit_Designation->setText("");
    ui->spinBox_PUA->setValue(0);
    ui->spinBox_PUV->setValue(0);
    ui->label_image->setText("");
    ui->textEdit_description->setPlainText("");
    ui->textEdit_description->setPlaceholderText("Description.....");
}

void NewProduit::on_spinBox_PUA_valueChanged(int arg1)
{
    //ui->spinBox_PUV->setValue(arg1);
    //ui->spinBox_PUV->setMinimum(arg1);
}

FORMS += \
    $$PWD/produit.ui\
    $$PWD/itemtablewidget.ui\
    $$PWD/newproduit.ui \
    $$PWD/selectionproduit.ui \
    $$PWD/commande.ui \
    $$PWD/quickcommande.ui \
    $$PWD/numerodetable.ui

HEADERS += \
    $$PWD/produit.h\
    $$PWD/itemtablewidget.h \
    $$PWD/newproduit.h \
    $$PWD/selectionproduit.h \
    $$PWD/commande.h \
    $$PWD/itemtable.h \
    $$PWD/quickcommande.h \
    $$PWD/numerodetable.h

SOURCES += \
    $$PWD/produit.cpp \
    $$PWD/produit_methods.cpp\
    $$PWD/itemtablewidget.cpp \
    $$PWD/newproduit.cpp \
    $$PWD/selectionproduit.cpp \
    $$PWD/commande.cpp \
    $$PWD/quickcommande.cpp \
    $$PWD/numerodetable.cpp

#ifndef ITEMTABLEWIDGET_H
#define ITEMTABLEWIDGET_H

#include <QWidget>

namespace Ui {
class itemTableWidget;
}

class itemTableWidget : public QWidget
{
    Q_OBJECT

public:
    explicit itemTableWidget(QWidget *parent = 0);
    ~itemTableWidget();
    bool isSelected() ;
    QString productName();
    void setSelected(bool enable);
    void setDataToView(QString _00id, QString _01Designation, QString _02Categorie, QString _03Prix_Unit_Achate,
                       QString _04Prix_Unit_Vente, QString _05Description, QByteArray _06Image, int itemRow);
    QString _00id, _01Designation, _02Categorie,
    _03Prix_Unit_Achate, _04Prix_Unit_Vente,
    _05Description  ;
    QByteArray _06Image ;
    int quantite ;
    int itemRow ;
signals:
    void clicked();
private slots:
    void on_toolButton_add_clicked();

    void on_toolButton_rmv_clicked();

private:
    Ui::itemTableWidget *ui;
    bool selected ;

};

#endif // ITEMTABLEWIDGET_H

#include "produit.h"
#include "ui_produit.h"
#include "mainwindow.h"
#include "QThread"
#include <QAction>
#include <QInputDialog>
#include <QSpinBox>
#include <QWidgetAction>
#include <QApplication>
#include <QDesktopWidget>
Produit::Produit(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Produit)
{
    ui->setupUi(this);

    //readTable();
    newproduit = new NewProduit(this);
    connect(newproduit, SIGNAL(backButton()), this, SLOT(backToHomePage()));
    ui->stackedWidget->addWidget(newproduit);

    selectionproduit = new SelectionProduit(this);
    connect(selectionproduit, SIGNAL(backButton()), this, SLOT(backToHomePage()));
    ui->stackedWidget->addWidget(selectionproduit);

    commande = new Commande(this);
    connect(commande, SIGNAL(backButton()), this, SLOT(backToHomePage()));

    ui->stackedWidget->addWidget(commande);
    ui->stackedWidget->setDuration(500);//TODO: GVAR
    columnCount = 3 ; //TODO: GVAR
    //ui->stackedWidget->setLength(len, AnimationStackedWidget::RightToLeft);

    ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectItems);
    ui->tableWidget->setSelectionMode( QAbstractItemView::NoSelection );
    ui->tableWidget->setFocusPolicy(Qt::NoFocus);
    qDebug() << _myModel->database().tables() ;
    initAction();



    ui->comboBox_type->addItems(_mapColumns[enumProduit::tableName]);
    ui->comboBox_type->setCurrentIndex(1);
    connect(selectionproduit, SIGNAL(selectedList(QStringList)), this, SLOT(selectedList(QStringList)) );
    ui->frame1->setVisible(false);
    ui->tableWidget_commande->setColumnCount(4);
    ui->tableWidget_commande->hideColumn(2);
    ui->tableWidget_commande->hideColumn(3);
    ui->tableWidget_commande->resizeColumnsToContents();
    ui->tableWidget_commande->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableWidget_commande->setSelectionMode(QAbstractItemView::NoSelection);

    ui->toolButton_commande->setVisible(false);
    ui->toolButton_valide_directement->setVisible(false);

}

void Produit::initAction()
{
    QString QSS= "QLabel::hover { border-style: outset; border-width: 2px; border-color: red;}"
                 " QLabel { color: black; background-color: white ; }" ;
    QSS = "" ;
    QLabel *ajoutel = new QLabel(QString(ui->toolButton_new_product->text()), this);
    ajoutel->setStyleSheet(QSS);

    QLabel *supprimel = new QLabel(QString("Supprimié"), this);
    supprimel->setStyleSheet(QSS);

    QLabel *modifiel = new QLabel(QString("Modifié"), this);
    modifiel->setStyleSheet(QSS);

    QLabel *supprimeSelectedl = new QLabel(QString("Supprimié"), this);
    supprimeSelectedl->setStyleSheet(QSS);

    // init widget action

    QWidgetAction *ajoute, *supprime, *modifie, *supprimeSelected;
    ajoute = new QWidgetAction(this);
    supprime = new QWidgetAction(this);
    modifie = new QWidgetAction(this);
    supprimeSelected = new QWidgetAction(this);

    ajoute->setDefaultWidget(ajoutel);
    supprime->setDefaultWidget(supprimel);
    modifie->setDefaultWidget(modifiel);
    supprimeSelected->setDefaultWidget(supprimeSelectedl);

    //Icon
    //ajoute->setIcon(ui->toolButton_Nouv_Travailleur->icon());
    //supprime->setIcon(ui->toolButton_suppr->icon());
    //modifie->setIcon(ui->toolButton_Attestation->icon());

    // on click
    connect(ajoute, SIGNAL(triggered(bool)), this, SLOT(on_toolButton_new_product_clicked()));
    connect(supprime, SIGNAL(triggered(bool)), this, SLOT(supprimeCurrentItem()));
    connect(modifie, SIGNAL(triggered(bool)), this, SLOT(modifieCurrentItem()));
    connect(supprimeSelected, SIGNAL(triggered(bool)), this, SLOT(supprimeCurrentItemSelected()));

    // init table with action
    ui->tableWidget->setContextMenuPolicy(Qt::ActionsContextMenu);
    ui->tableWidget->addAction(ajoute);
    ui->tableWidget->addAction(supprime);
    ui->tableWidget->addAction(modifie);

    ui->tableWidget_commande->setContextMenuPolicy(Qt::ActionsContextMenu);
    ui->tableWidget_commande->addAction(supprimeSelected);
}

void Produit::supprimeCurrentItemSelected()
{
    int currentRow = ui->tableWidget_commande->currentRow() ;
    QString designation = ui->tableWidget_commande->item(currentRow, 0)->text() ;

    int rowCount = ui->tableWidget->rowCount();
    int columnCount = ui->tableWidget->columnCount();
    itemTableWidget *temp = nullptr;
    for ( int i = 0 ; i < rowCount ; i++ )
    {
        for (int j = 0 ; j < columnCount ; j++ )
        {
            temp = (itemTableWidget*)ui->tableWidget->cellWidget(i, j);
            if( temp != nullptr )
            {
                if(temp->_01Designation == designation)
                {
                    temp->setSelected(false);
                    break ;
                }
            }
        }
    }
    ui->tableWidget_commande->removeRow(ui->tableWidget_commande->currentRow());
}

void Produit::supprimeCurrentItem()
{
    int row = ui->tableWidget->currentIndex().row() ;
    int column = ui->tableWidget->currentIndex().column() ;
    itemTableWidget *temp = (itemTableWidget*)ui->tableWidget->cellWidget(row, column);
    _myModel->removeRow(temp->itemRow) ;
    readTable();
}

void Produit::modifieCurrentItem()
{
    ui->stackedWidget->setCurrentIndex(1);
    int row = ui->tableWidget->currentIndex().row() ;
    int column = ui->tableWidget->currentIndex().column() ;
    itemTableWidget *temp = (itemTableWidget*)ui->tableWidget->cellWidget(row, column);
    int k = temp->itemRow ;
    newproduit->setDataToView(_myModel->record(k).value(enumProduit::_00id).toString(),
                              _myModel->record(k).value(enumProduit::_01Designation).toString(),
                              _myModel->record(k).value(enumProduit::_02Categorie).toString(),
                              _myModel->record(k).value(enumProduit::_03Prix_Unit_Achate).toString(),
                              _myModel->record(k).value(enumProduit::_04Prix_Unit_Vente).toString(),
                              _myModel->record(k).value(enumProduit::_05Description).toString(),
                              _myModel->record(k).value(enumProduit::_05Description).toByteArray(),
                              k);


}

void Produit::selectedList(QStringList list)
{//useless
    QList<itemtable> selectedList  ;
    int rowCount = ui->tableWidget->rowCount();
    int columnCount = ui->tableWidget->columnCount();
    itemTableWidget *temp = nullptr;
    for ( int i = 0 ; i < rowCount ; i++ )
    {
        for (int j = 0 ; j < columnCount ; j++ )
        {
            temp = (itemTableWidget*)ui->tableWidget->cellWidget(i, j);
            if( temp != nullptr )
            {
                for ( int u = 0 ; u < list.length() ; u++)
                if(temp->_01Designation == list[u])
                {
                    itemtable item;
                    item._00id = temp->_00id ;
                    item._01Designation = temp->_01Designation ;
                    item._02Categorie = temp->_02Categorie ;
                    item._03Prix_Unit_Achate = temp->_03Prix_Unit_Achate ;
                    item._04Prix_Unit_Vente = temp->_04Prix_Unit_Vente ;
                    item._05Description = temp->_05Description ;
                    selectedList << item ;
                    break ;
                }
            }
        }
    }
    //ui->stackedWidget->start(3);
    commande->init(selectedList) ;
    ui->stackedWidget->setLength(len, AnimationStackedWidget::RightToLeft);
}

void Produit::backToHomePage()
{
    ui->stackedWidget->start(0);
    ui->stackedWidget->setLength(len, AnimationStackedWidget::LeftToRight);
    readTable();
    ui->tableWidget_commande->setRowCount(0);
}
Produit::~Produit()
{
    ui->frame1->setVisible(true);
    QList<int> currentSizes = ui->splitter->sizes() ;
    setSettings("splitterX", currentSizes[0]);
    setSettings("splitterY", currentSizes[1]);
    delete ui;
}


void Produit::on_toolButton_back_clicked()
{
    emit backButton();
}


void Produit::on_radioButton_select_items_clicked()
{

    ui->radioButton_read_only->setChecked(false);
    ui->tableWidget->setSelectionMode(QAbstractItemView::ExtendedSelection);

}

void Produit::on_radioButton_read_only_clicked()
{
    ui->radioButton_select_items->setChecked(false);
    ui->tableWidget->setSelectionMode(QAbstractItemView::NoSelection);
}

void Produit::on_toolButton_refresh_clicked()
{
    ui->lineEdit_search->setText("");
    readTable();
    ui->label_prix_total_second->setText("0");
    ui->tableWidget_commande->setRowCount(0);
}

void Produit::on_toolButton_new_product_clicked()
{
    newproduit->modifie = false ; // on cas ou tkon true machi bel3ani
    ui->stackedWidget->start(1);
    ui->stackedWidget->setLength(len, AnimationStackedWidget::RightToLeft);
}

void Produit::on_toolButton_commande_clicked()
{   
    QList<itemtable> temp = getSelectedItems();
    if(temp.isEmpty())
    {
        ui->stackedWidget->start(2);
        selectionproduit->init() ;
    }
    else
    {
        ui->stackedWidget->start(3);
        commande->init(temp) ;
    }
    ui->stackedWidget->setLength(len, AnimationStackedWidget::RightToLeft);
}

void Produit::on_tableWidget_clicked(const QModelIndex &index)
{
    //if(ui->tableWidget->item(index.row, index.column()))
    if(ui->radioButton_select_items->isChecked())
    {
        itemTableWidget *temp = nullptr;
        temp = (itemTableWidget*)ui->tableWidget->cellWidget(index.row(), index.column());
        if(temp != nullptr)
        {
            if(temp->isSelected())
            {
                return ;
            }

        }




        bool ok ;
        int quantite = QInputDialog::getInt(this, "dialog", "Quantite:", 1, 1, 999999999,1,&ok);
        if(!ok)
            return ;
        temp->setSelected(true)  ;
        int row = ui->tableWidget_commande->rowCount() ;
        ui->tableWidget_commande->setRowCount(row+1);
        QTableWidgetItem *x=new QTableWidgetItem (temp->_01Designation);
        ui->tableWidget_commande->setItem(row, 0, x) ;

        QSpinBox *y= new QSpinBox(ui->tableWidget_commande);
        y->setMinimum(0);
        y->setMaximum(999999999);
        y->setValue(quantite);
        y->setSingleStep(1);
        ui->tableWidget_commande->setCellWidget(row, 1, y);

        QSpinBox *yy= new QSpinBox(ui->tableWidget_commande);
        yy->setMinimum(0);
        yy->setMaximum(999999999);
        yy->setValue(temp->_04Prix_Unit_Vente.toInt());
        yy->setSingleStep(1);
        ui->tableWidget_commande->setCellWidget(row, 2, yy);

        QSpinBox *yyy= new QSpinBox(ui->tableWidget_commande);
        yyy->setMinimum(0);
        yyy->setMaximum(999999999);
        yyy->setValue(temp->_03Prix_Unit_Achate.toInt());
        yyy->setSingleStep(1);
        ui->tableWidget_commande->setCellWidget(row, 3, yyy);

        if(!ui->frame1->isVisible())
        {
            ui->frame1->setVisible(true);
            QList<int> currentSizes = ui->splitter->sizes() ;
            currentSizes[0] = getSettings("splitterX").toInt() ;
            currentSizes[1] = getSettings("splitterY").toInt() ;
            ui->splitter->setSizes(currentSizes);
            ui->tableWidget_commande->resizeColumnsToContents();
        }
        calculeLePrixTotal();
        /*
        QuickCommande quickcommande ;
        QList<itemtable> myList ;
        quickcommande.init(myList);
        quickcommande.exec() ;
        */
    }
}

void Produit::on_lineEdit_search_textChanged(const QString &arg1)
{
//    int rowCount = ui->tableWidget->rowCount();
//    int columnCount = ui->tableWidget->columnCount();
//    itemTableWidget *temp = nullptr;

//    QString temp1, temp2 ;
//    for ( int i = 0 ; i < rowCount ; i++ )
//    {
//        for (int j = 0 ; j < columnCount ; j++ )
//        {
//            temp = (itemTableWidget*)ui->tableWidget->cellWidget(i, j);
//            if( temp != nullptr )
//            {
//                temp1 = arg1.toLower();

//                if(ui->comboBox_type->currentIndex() == 1)
//                    temp2 = temp->_01Designation.toLower() ;
//                else if(ui->comboBox_type->currentIndex() == 3)
//                    temp2 = temp->_03Prix_Unit_Achate.toLower() ;
//                else if(ui->comboBox_type->currentIndex() == 4)
//                    temp2 = temp->_04Prix_Unit_Vente.toLower() ;
//                else if(ui->comboBox_type->currentIndex() == 5)
//                    temp2 = temp->_05Description.toLower() ;


//                if( temp2.contains(temp1) )
//                {
//                    temp->setHidden(false);
//                }
//                else
//                    temp->setHidden(true);


//            }
//        }
//    }

}

void Produit::on_comboBox_type_currentIndexChanged(const QString &arg1)
{
    if(arg1 == _mapColumns[enumProduit::tableName][enumProduit::_02Categorie] )
        ui->stackedWidget_search->setCurrentIndex(1);
    else
    {
        ui->stackedWidget_search->setCurrentIndex(0);
        ui->lineEdit_search->setText("");
    }
}

void Produit::on_comboBox_categorie_currentIndexChanged(const QString &arg1)
{
    if(enabled)
    {
        if(oldTemp == -1 )
        {
            oldTemp = ui->comboBox_categorie->currentIndex() ;
            ui->lineEdit_search->setText(arg1);
            on_lineEdit_search_editingFinished();
            ui->comboBox_categorie->setCurrentIndex(oldTemp);
        }
        else
        {
            if(oldTemp != ui->comboBox_categorie->currentIndex())
            {
                oldTemp = ui->comboBox_categorie->currentIndex() ;
                ui->lineEdit_search->setText(arg1);
                on_lineEdit_search_editingFinished();
                ui->comboBox_categorie->setCurrentIndex(oldTemp);
            }
        }

    }
    /*
    int rowCount = ui->tableWidget->rowCount();
    int columnCount = ui->tableWidget->columnCount();
    itemTableWidget *temp = nullptr;

    for ( int i = 0 ; i < rowCount ; i++ )
    {
        for (int j = 0 ; j < columnCount ; j++ )
        {
            temp = (itemTableWidget*)ui->tableWidget->cellWidget(i, j);
            if( temp != nullptr )
            {
                if(arg1 == "ALL")
                    temp->setHidden(false);
                else
                if( temp->_02Categorie == arg1 )
                {
                    temp->setHidden(false);
                }
                else
                    temp->setHidden(true);


            }
        }
    }
    */
}

void Produit::on_toolButton_imprimie_clicked()
{   
    NumeroDeTable numerodetable ;
    numerodetable.exec() ;

    if(numerodetable.cancel)
        numerodetable.selectedTable = "" ;
QList<itemtable> selectedList;
    QSpinBox *y ;
    for ( int k = 0 ; k < ui->tableWidget_commande->rowCount() ; k++ )
    {
        itemtable item;
        item._01Designation = ui->tableWidget_commande->item(k, 0 )->text();

        y = (QSpinBox*)ui->tableWidget_commande->cellWidget(k, 1);
        item._05Description = y->text();

        y = (QSpinBox*)ui->tableWidget_commande->cellWidget(k, 2);
        item._04Prix_Unit_Vente = y->text();

        y = (QSpinBox*)ui->tableWidget_commande->cellWidget(k, 3);
        item._03Prix_Unit_Achate = y->text();

        selectedList << item;
    }

    commande->selectedTable = numerodetable.selectedTable ;

    commande->init(selectedList);

    commande->on_toolButton_save_clicked();
    commande->on_toolButton_imprimie_clicked();
    commande->on_toolButton_imprimie_clicked();

}

void Produit::on_toolButton_les_supp_clicked()
{
    //if(ui->tableWidget_commande->currentRow() > -1 )
    QString temp ;
        QuickCommande quickcommande ;
        quickcommande.init("Suplement");
        QRect rec = QApplication::desktop()->screenGeometry();

        quickcommande.resize( rec.width() -300 ,rec.height() -300 );
        quickcommande.exec();
        for ( int i = 0 ; i < quickcommande.list.length() ; i++ )
        {
            int row = ui->tableWidget_commande->rowCount() ;
            ui->tableWidget_commande->setRowCount(row+1);
            QTableWidgetItem *x=new QTableWidgetItem (quickcommande.list[i]);
            ui->tableWidget_commande->setItem(row, 0, x) ;

            QSpinBox *y= new QSpinBox(ui->tableWidget_commande);
            y->setMinimum(0);
            y->setMaximum(999999999);
            y->setValue(1);
            y->setSingleStep(1);
            ui->tableWidget_commande->setCellWidget(row, 1, y);


            QSpinBox *yy= new QSpinBox(ui->tableWidget_commande);
            yy->setMinimum(0);
            yy->setMaximum(999999999);
            temp = quickcommande.listPUV[i] ;
            yy->setValue(temp.toInt());
            yy->setSingleStep(1);
            ui->tableWidget_commande->setCellWidget(row, 2, yy);

            QSpinBox *yyy= new QSpinBox(ui->tableWidget_commande);
            yyy->setMinimum(0);
            yyy->setMaximum(999999999);
            temp = quickcommande.listPUA[i] ;
            yyy->setValue(temp.toInt());
            yyy->setSingleStep(1);
            ui->tableWidget_commande->setCellWidget(row, 3, yyy);
            //ui->tableWidget_commande->resizeColumnsToContents();
        }

    calculeLePrixTotal();
}

void Produit::on_splitter_splitterMoved(int pos, int index)
{
    //qDebug() << ui->splitter->sizes();
}

void Produit::on_toolButton_topping_clicked()
{
    QuickCommande quickcommande ;
    quickcommande.init("Topping");
    quickcommande.topping = true ;
    QRect rec = QApplication::desktop()->screenGeometry();
    quickcommande.resize( rec.width() -300 ,rec.height() -300 );
    quickcommande.exec();
    QList<itemtable> x = quickcommande.getSelectedItems2();
    itemtable za ;
    for ( int i = 0 ; i < x.length() ; i++ )
    {
        int row = ui->tableWidget_commande->rowCount() ;
        ui->tableWidget_commande->setRowCount(row+1);
        za = x[i] ;
        QTableWidgetItem *x=new QTableWidgetItem(za._01Designation);
        ui->tableWidget_commande->setItem(row, 0, x) ;

        QSpinBox *y= new QSpinBox(ui->tableWidget_commande);
        y->setMinimum(0);
        y->setMaximum(999999999);
        y->setValue(za.quantite);
        y->setSingleStep(1);
        ui->tableWidget_commande->setCellWidget(row, 1, y);

        QSpinBox *yy= new QSpinBox(ui->tableWidget_commande);
        yy->setMinimum(0);
        yy->setMaximum(999999999);
        yy->setValue(za._04Prix_Unit_Vente.toInt());
        yy->setSingleStep(1);
        ui->tableWidget_commande->setCellWidget(row, 2, yy);

        QSpinBox *yyy= new QSpinBox(ui->tableWidget_commande);
        yyy->setMinimum(0);
        yyy->setMaximum(999999999);
        yyy->setValue(za._03Prix_Unit_Achate.toInt());
        yyy->setSingleStep(1);
        ui->tableWidget_commande->setCellWidget(row, 3, yyy);
        //ui->tableWidget_commande->resizeColumnsToContents();
    }

    calculeLePrixTotal();
}

void Produit::on_tableWidget_commande_clicked(const QModelIndex &index)
{

}

void Produit::on_lineEdit_search_editingFinished()
{
    readTable();
    ui->comboBox_categorie->setFocus();
}

void Produit::on_toolButton_valide_directement_clicked()
{


    /*
    NumeroDeTable numerodetable ;
    numerodetable.exec() ;

    if(numerodetable.cancel)
        numerodetable.selectedTable = "" ;
    QList<itemtable> selectedList;
    QSpinBox *y ;
    for ( int k = 0 ; k < ui->tableWidget_commande->rowCount() ; k++ )
    {
        itemtable item;
        item._01Designation = ui->tableWidget_commande->item(k, 0 )->text();

        y = (QSpinBox*)ui->tableWidget_commande->cellWidget(k, 1);
        item._05Description = y->text();

        y = (QSpinBox*)ui->tableWidget_commande->cellWidget(k, 2);
        item._04Prix_Unit_Vente = y->text();

        y = (QSpinBox*)ui->tableWidget_commande->cellWidget(k, 3);
        item._03Prix_Unit_Achate = y->text();

        selectedList << item;
    }

    commande->selectedTable = numerodetable.selectedTable ;
    bool ok ;
    int quantite = QInputDialog::getInt(this, "dialog", "Quantite:", 1, 1, 999999999,1,&ok);
    if(!ok)
        return ;
    commande->ch3aldfa3 =
    commande->ch3altrodlah
    commande->init(selectedList);

    commande->on_toolButton_saveToTransaction();
    */
}

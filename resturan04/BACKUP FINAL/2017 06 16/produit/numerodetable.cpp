#include "numerodetable.h"
#include "ui_numerodetable.h"

NumeroDeTable::NumeroDeTable(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NumeroDeTable)
{
    ui->setupUi(this);
    for (int i = 0 ; i < 10 ; i++ )
    {
        ui->listWidget->addItem("Table"+QString::number(i+1));
    }
    ui->listWidget->addItem("Emporter");
}

NumeroDeTable::~NumeroDeTable()
{
    delete ui;
}

void NumeroDeTable::on_pushButton_cancel_clicked()
{
    this->close();
    cancel = true ;
}

void NumeroDeTable::on_pushButton_OK_clicked()
{
    if(ui->listWidget->currentRow() > -1 )
    selectedTable = ui->listWidget->currentItem()->text() ;
    this->close();
}

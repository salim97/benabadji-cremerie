#ifndef MYFILTER_H
#define MYFILTER_H

#include <QDialog>

namespace Ui {
class myFilter;
}

class myFilter : public QDialog
{
    Q_OBJECT

public:
    explicit myFilter(QWidget *parent = 0);
    ~myFilter();

private slots:
    void on_pushButton_Cancel_clicked();

    void on_pushButton_OK_clicked();

private:
    Ui::myFilter *ui;
};

#endif // MYFILTER_H

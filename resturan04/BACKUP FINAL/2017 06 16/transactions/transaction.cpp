#include "transaction.h"
#include "ui_transaction.h"

Transaction::Transaction(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Transaction)
{
    ui->setupUi(this);
    _myModel->setTable(enumTransaction::tableName);
    _myModel->select();
    ui->tableView->setModel(_myModel);
    initAction();
    ui->stackedWidget->setCurrentIndex(0);

}
void Transaction::initAction()
{
    // init widget action
    QAction *supp;
    supp = new QAction("Supprime", this);

    //Icon
    supp->setIcon(ui->toolButton_remove->icon());

    // on click
    connect(supp, SIGNAL(triggered(bool)), this, SLOT(on_toolButton_remove_clicked()));

    // init table with action
    ui->tableView->setContextMenuPolicy(Qt::ActionsContextMenu);
    ui->tableView->addAction(supp);
}
Transaction::~Transaction()
{
    delete ui;
}

void Transaction::on_toolButton_back_clicked()
{
    if(_myModel->tableName() == enumCommande::tableName)
        init();
    else
        emit backButton();
}

void Transaction::init()
{
    _myModel->setTable(enumTransaction::tableName);
    _myModel->setFilter("");
    _myModel->select();
    ui->tableView->hideColumn(enumTransaction::_00id);
    ui->tableView->hideColumn(enumTransaction::_03Prix_ntkamlek);
    ui->tableView->resizeColumnsToContents();
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

}

QString Transaction::chifferDaffaire()
{
    _myModel->setFilter(_mapColumns[enumTransaction::tableName][enumTransaction::_01Date]+" LIKE '%"+getCurrentDate()+"%'");
    _myModel->select();
    int total =0 ;
    for (int i = 0 ; i < _myModel->rowCount() ; i++ )
    {
        total += _myModel->record(i).value(enumTransaction::_02Prix).toInt() ;
    }
    return QString::number(total) ;
}

QString Transaction::recette()
{
    _myModel->setFilter(_mapColumns[enumTransaction::tableName][enumTransaction::_01Date]+" LIKE '%"+getCurrentDate()+"%'");
    _myModel->select();
    int total =0 ;
    for (int i = 0 ; i < _myModel->rowCount() ; i++ )
    {
        total += _myModel->record(i).value(enumTransaction::_03Prix_ntkamlek).toInt() ;
    }
    return QString::number(total) ;
}

void Transaction::on_toolButton_remove_clicked()
{
    _myModel->removeRow(ui->tableView->currentIndex().row());
    _myModel->select();
    ui->tableView->hideColumn(enumTransaction::_00id);
    ui->tableView->hideColumn(enumTransaction::_03Prix_ntkamlek);
}

void Transaction::on_tableView_doubleClicked(const QModelIndex &index)
{
    QString id = _myModel->record(index.row()).value(enumTransaction::_00id).toString() ;
    _myModel->setTable(enumCommande::tableName);
    _myModel->setFilter("id_facture == '"+id+"'");
    qDebug() << "_myModel->filter() " << _myModel->filter() << id;

    _myModel->select() ;
    ui->tableView->hideColumn(0);
    ui->tableView->hideColumn(1);
    //ui->toolButton_new_product->setDisabled(true);
}

void Transaction::on_toolButton_filter_clicked()
{

}

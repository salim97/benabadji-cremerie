
SOURCES +=  $$PWD/tableviewtools.cpp \
            $$PWD/filter.cpp \
            $$PWD/viewcolumns.cpp


HEADERS  += $$PWD/tableviewtools.h \
            $$PWD/filter.h \
            $$PWD/viewcolumns.h

FORMS    += $$PWD/tableviewtools.ui \
            $$PWD/filter.ui \
            $$PWD/viewcolumns.ui


RESOURCES += \
    $$PWD/Qt_Resource_File/tableViewToolsResource.qrc

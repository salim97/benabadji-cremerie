#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QProcess>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->tableWidget->setColumnCount(3);
    ui->tableWidget->setRowCount(3);

    initVariables();
    initView();
    /*--------*/

    initializeTables();
    createFolderIfNotExsist(dirRS);
    gTimeSwitch = getSettings("gTimeSwitch").toInt() ;
    if( gTimeSwitch == 0 )
        gTimeSwitch = 5000 ;
//    executeQuery(prepare_dropTable(enumCommande::tableName));

}
void MainWindow::initVariables()
{
    myProduit = new Produit(this) ; // 01
    connect(myProduit, SIGNAL(backButton()), this, SLOT(backToHomePage()));
    ui->stackedWidget->addWidget(myProduit);

    myTransaction = new Transaction(this); // 02
    connect(myTransaction, SIGNAL(backButton()), this, SLOT(backToHomePage()));
    ui->stackedWidget->addWidget(myTransaction);

    myStock = new Stock(this); // 03
    connect(myStock, SIGNAL(backButton()), this, SLOT(backToHomePage()));
    ui->stackedWidget->addWidget(myStock);

    myOption = new Option(this); // 04
    connect(myOption, SIGNAL(backButton()), this, SLOT(backToHomePage()));
    ui->stackedWidget->addWidget(myOption);

    myNonPayee = new NonPayee(this); // 05
    connect(myNonPayee, SIGNAL(backButton()), this, SLOT(backToHomePage()));
    ui->stackedWidget->addWidget(myNonPayee);


    myCalculator = new Calculator;
    myCalculator->showed=false;

    produit = new Win10PushButtonState(this) ;
    stock = new Win10PushButtonState(this);
    transaction = new Win10PushButtonState(this);

    option = new Win10PushButtonStd(this) ;
    calculatrice = new Win10PushButtonStd(this) ;
    nonPayee = new Win10PushButtonStd(this) ;
}

void MainWindow::initView()
{
    // set cell to position
    ui->tableWidget->setCellWidget(0, 0, produit);
    ui->tableWidget->setCellWidget(1, 0, transaction);
    ui->tableWidget->setCellWidget(2, 0, stock);
    ui->tableWidget->setCellWidget(0, 2, nonPayee);
    ui->tableWidget->setCellWidget(1, 2, calculatrice);
    ui->tableWidget->setCellWidget(2, 2, option);

    //fucion cell
    ui->tableWidget->setSpan(0, 0, 1, 2);
    ui->tableWidget->setSpan(1, 0, 1, 2);
    ui->tableWidget->setSpan(2, 0, 1, 2);

    // resize column and row to fit size of window
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableWidget->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableWidget->resizeRowsToContents();
    ui->tableWidget->resizeColumnsToContents();

    //set image
    produit->setImage(":/win10/win10/Product.png");
    transaction->setImage(":/win10/win10/transaction.png");
    stock->setImage(":/win10/win10/Hangar Filled-100.png");
    nonPayee->setImage(":/win10/win10/unpaid.png");
    calculatrice->setImage(":/win10/win10/Calculator-96.png");
    option->setImage(":/win10/win10/Settings Filled-100.png");

    //set label name
    produit->setLabelText("PRODUITS");
    transaction->setLabelText("TRANSACTIONS");
    stock->setLabelText("STOCK");
    nonPayee->setLabelText("NON PAYEE");
    calculatrice->setLabelText("CALCULATRICE");
    option->setLabelText("PARAMETRES");

    //set css stylesheet
    produit->setCSS("background-color: rgb(0, 120, 215);");
    transaction->setCSS(" background-color: rgb(153,53,255) ;");
    stock->setCSS("background-color: rgb(0, 120, 215);");
    nonPayee->setCSS("background-color: rgb(0, 120, 215);");
    calculatrice->setCSS("background-color: rgb(255, 85, 0);");
    option->setCSS("background-color: rgb(102,177,29) ;");

    //no selection and no focus and no frame or rectanlge of selction
    ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectItems);
    ui->tableWidget->setSelectionMode( QAbstractItemView::NoSelection );
    ui->tableWidget->setFocusPolicy(Qt::NoFocus);

    //animation
    nonPayee->startAnimation(gTimeSwitch,100, "bottom");


    dashboard();

}

void MainWindow::backToHomePage()
{
    ui->stackedWidget->start(0);
    dashboard();
    ui->stackedWidget->setLength(len, AnimationStackedWidget::LeftToRight);
}

MainWindow::~MainWindow()
{
    ui->tableWidget->setRowCount(0);
    delete ui;
}

void MainWindow::on_tableWidget_pressed(const QModelIndex &index)
{
    qDebug() << index.row() << index.column() ;
    if(index.row() == 0 && index.column() != 2)
    {
        ui->stackedWidget->start(1);
        myProduit->readTable();
    }
    if(index.row() == 1 && index.column() != 2)
    {
        if(!showTransaction)
        {
            passwordTransaction = passwordTransaction;
            QString password = getStringFromUser(this, "Login", "Mot de passe:");
            if( password == passwordTransaction )
                showTransaction = true;
            dashboard();
        }
        else
        {
            ui->stackedWidget->start(2);
            myTransaction->init();
        }

    }
    if(index.row() == 2 && index.column() != 2)
    {
        ui->stackedWidget->start(3);
        myStock->init();
    }
    /*----------*/
    if(index.row() == 0 && index.column() == 2)
    {
        ui->stackedWidget->start(5);
        myNonPayee->init();
    }


    if(index.row() == 1 && index.column() == 2)
    {
        if(getSettings("calc").toInt() == 0)
        {
            if(myCalculator->showed)
            {
                myCalculator->hide();
                myCalculator->showed=false;
            }
            else
            {
                myCalculator->show();
                myCalculator->showed=true;
            }
        }
        else
        {

            process.start("calc");
        }
    }

    if(index.row() == 2 && index.column() == 2)
    {
        ui->stackedWidget->start(4);
        myOption->init();
    }
    ui->stackedWidget->setLength(len, AnimationStackedWidget::RightToLeft);
}

void MainWindow::dashboard()
{

    myNonPayee->_myModel->select();
    int numbreNonPayee = myNonPayee->_myModel->rowCount() ;
    QString temp01 ;
    temp01 += "Nombre facture non payee: "+QString::number(numbreNonPayee);
    temp01 += "\nMontant total des facture non payee: "+myNonPayee->montantTotal() +" DA " ;
    nonPayee->setLabelTextstate(temp01);

    myProduit->_myModel->setTable(enumProduit::tableName);
    myProduit->_myModel->select();
    int numbreDesProduit = myProduit->_myModel->rowCount() ;
    produit->setLabelState("Nombre des produits : "+QString::number(numbreDesProduit));

    myNonPayee->_myModel->select();
    if(showTransaction)
    {
        transaction->setLabelState("CHIFFRE D'AFFAIRE : "+myTransaction->chifferDaffaire()+" DA");
    }
    myStock->_myModel->select();
    int numbreProduitDansStock = myStock->_myModel->rowCount() ;
    stock->setLabelState("Nombre des produits : "+QString::number(numbreProduitDansStock));

}

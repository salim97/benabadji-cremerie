#ifndef WIN10PUSHBUTTONSTATE_H
#define WIN10PUSHBUTTONSTATE_H

#include <QWidget>
#include "animationstackedwidget.h"

namespace Ui {
class Win10PushButtonState;
}

class Win10PushButtonState : public QWidget
{
    Q_OBJECT

public:
    explicit Win10PushButtonState(QWidget *parent = 0);
    ~Win10PushButtonState();
    void setImage(QString imagePath);
    void setLabelText(QString text);
    void setLabelState(QString text);
    void setCSS(QString css);
private:
    Ui::Win10PushButtonState *ui;
};

#endif // WIN10PUSHBUTTONSTATE_H

FORMS += \
    $$PWD/win10pushbuttonstd.ui \
    $$PWD/win10pushbuttonstate.ui

HEADERS += \
    $$PWD/win10pushbuttonstd.h \
    $$PWD/win10pushbuttonstate.h\
    $$PWD/animationstackedwidget.h

SOURCES += \
    $$PWD/win10pushbuttonstd.cpp \
    $$PWD/win10pushbuttonstate.cpp\
    $$PWD/animationstackedwidget.cpp

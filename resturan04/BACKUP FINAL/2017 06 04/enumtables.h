#ifndef ENUMTABLES_H
#define ENUMTABLES_H

#include <QString>
#include <QStringList>


class enumProduit
{
public:
    QString tableName ;
    QStringList columnsList;
    enumProduit()
    {
        tableName = "produit" ;
        columnsList << "id integer primary key autoincrement not null" ;
        columnsList << "Designation TEXT" ;
        columnsList << "Categorie TEXT" ;
        columnsList << "Prix_Unit_Achate TEXT" ;
        columnsList << "Prix_Unit_Vente TEXT" ;
        columnsList << "Description TEXT" ;
        columnsList << "Image BLOB" ;
    }

    enum TableName
    {
        _00id, _01Designation, _02Categorie,
        _03Prix_Unit_Achate, _04Prix_Unit_Vente,
        _05Description, _06Image
    };
};

class enumTransaction
{
public:
    QString tableName ;
    QStringList columnsList;
    enumTransaction()
    {
        tableName = "log" ;
        columnsList << "id integer primary key autoincrement not null" ;
        columnsList << "Date DATETIME" ;
        columnsList << "Prix TEXT" ;
    }

    enum TableName
    {
        _00id, _01Date, _02Prix
    };
};

class enumCommande
{
public:
    QString tableName ;
    QStringList columnsList;
    enumCommande()
    {
        tableName = "commande" ;
        columnsList << "id integer primary key autoincrement not null" ;
        columnsList << "id_facture TEXT" ;
        columnsList << "Designation TEXT" ;
        columnsList << "Quantite TEXT" ;
        columnsList << "Montant_Total TEXT" ;
    }

    enum TableName
    {
        _00id, _01id_facture, _02Designation,
        _03Quantite, _04Montant_Total
    };
};

class enumStock
{
public:
    QString tableName ;
    QStringList columnsList;
    enumStock()
    {
        tableName = "stock" ;
        columnsList << "id integer primary key autoincrement not null" ;
        columnsList << "Designation TEXT" ;
        columnsList << "Quantite TEXT" ;
        columnsList << "Prix_Unit TEXT" ;
    }

    enum TableName
    {
        _00id, _01Designation, _02Quantite, _03Prix_Unit
    };
};
class enumNoPayee
{
public:
    QString tableName ;
    QStringList columnsList;
    enumNoPayee()
    {
        tableName = "nopayee" ;
        columnsList << "id integer primary key autoincrement not null" ;
        columnsList << "Date DATETIME" ;
        columnsList << "Nom TEXT" ;
        columnsList << "Montant_Total TEXT" ;
    }

    enum TableName
    {
        _00id, _01Date, _02Nom, _03Montant_Total
    };
};

#endif // ENUMTABLES_H

#ifndef TRANSACTION_H
#define TRANSACTION_H

#include <QWidget>
#include "myparentobject.h"

namespace Ui {
class Transaction;
}

class Transaction : public QWidget, public myParentObject
{
    Q_OBJECT

public:
    explicit Transaction(QWidget *parent = 0);
    ~Transaction();
    void init();
    QString chifferDaffaire();
signals:
    void backButton();
private slots:
    void on_toolButton_back_clicked();

    void on_toolButton_remove_clicked();

private:
    Ui::Transaction *ui;
    void initAction();
};

#endif // TRANSACTION_H

#include "transaction.h"
#include "ui_transaction.h"

Transaction::Transaction(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Transaction)
{
    ui->setupUi(this);
    _myModel->setTable(enumTransaction::tableName);
    _myModel->select();
    ui->tableView->setModel(_myModel);
    initAction();
}
void Transaction::initAction()
{
    // init widget action
    QAction *supp;
    supp = new QAction("Supprime", this);

    //Icon
    supp->setIcon(ui->toolButton_remove->icon());

    // on click
    connect(supp, SIGNAL(triggered(bool)), this, SLOT(on_toolButton_remove_clicked()));

    // init table with action
    ui->tableView->setContextMenuPolicy(Qt::ActionsContextMenu);
    ui->tableView->addAction(supp);
}
Transaction::~Transaction()
{
    delete ui;
}

void Transaction::on_toolButton_back_clicked()
{
    emit backButton();
}

void Transaction::init()
{
    _myModel->select();
    ui->tableView->resizeColumnsToContents();
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
}

QString Transaction::chifferDaffaire()
{
    _myModel->select();
    int total =0 ;
    for (int i = 0 ; i < _myModel->rowCount() ; i++ )
    {
        total += _myModel->record(i).value(enumTransaction::_02Prix).toInt() ;
    }
    return QString::number(total) ;
}

void Transaction::on_toolButton_remove_clicked()
{
    _myModel->removeRow(ui->tableView->currentIndex().row());
    _myModel->select();
}

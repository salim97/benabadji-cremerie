#include "stock.h"
#include "ui_stock.h"

Stock::Stock(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Stock)
{
    ui->setupUi(this);
    _myModel->setTable(enumStock::tableName);
    _myModel->select();
    ui->tableView->setModel(_myModel);
    initAction();
}
void Stock::initAction()
{

    // init widget action

    QAction  *ajoute, *supp;
    ajoute = new QAction("Ajoute", this);
    supp = new QAction("Supprime", this);

    //Icon

    ajoute->setIcon(ui->toolButton_new_product->icon());
    supp->setIcon(ui->toolButton_remove->icon());

    // on click
    connect(ajoute, SIGNAL(triggered(bool)), this, SLOT(on_toolButton_new_product_clicked()));
    connect(supp, SIGNAL(triggered(bool)), this, SLOT(on_toolButton_remove_clicked()));

    // init table with action
    ui->tableView->setContextMenuPolicy(Qt::ActionsContextMenu);
    ui->tableView->addAction(ajoute);
    ui->tableView->addAction(supp);


}
Stock::~Stock()
{
    delete ui;
}

void Stock::on_toolButton_back_clicked()
{
    emit backButton();
}

void Stock::on_toolButton_new_product_clicked()
{
    addstock newStock ;
    newStock.exec() ;
    _myModel->select();
}

void Stock::init()
{
    _myModel->select();
    ui->tableView->resizeColumnsToContents();
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
}

void Stock::on_toolButton_remove_clicked()
{
    _myModel->removeRow(ui->tableView->currentIndex().row());
    _myModel->select();
}

#ifndef ADDSTOCK_H
#define ADDSTOCK_H

#include <QDialog>
#include "myparentobject.h"

namespace Ui {
class addstock;
}

class addstock : public QDialog, public myParentObject
{
    Q_OBJECT

public:
    explicit addstock(QWidget *parent = 0);
    ~addstock();

private slots:
    void on_pushButton_cancel_clicked();

    void on_pushButton_ok_clicked();

private:
    Ui::addstock *ui;
};

#endif // ADDSTOCK_H

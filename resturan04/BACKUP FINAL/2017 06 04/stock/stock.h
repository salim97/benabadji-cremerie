#ifndef STOCK_H
#define STOCK_H

#include <QWidget>
#include "myparentobject.h"
#include "addstock.h"

namespace Ui {
class Stock;
}

class Stock : public QWidget, public myParentObject
{
    Q_OBJECT

public:
    explicit Stock(QWidget *parent = 0);
    ~Stock();
    void init();
signals:
    void backButton();
private slots:
    void on_toolButton_back_clicked();

    void on_toolButton_new_product_clicked();

    void on_toolButton_remove_clicked();

private:
    Ui::Stock *ui;
    void initAction();
};

#endif // STOCK_H

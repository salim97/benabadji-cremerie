#ifndef NONPAYEE_H
#define NONPAYEE_H

#include <QWidget>
#include "myparentobject.h"

namespace Ui {
class NonPayee;
}

class NonPayee : public QWidget, public myParentObject
{
    Q_OBJECT

public:
    explicit NonPayee(QWidget *parent = 0);
    ~NonPayee();
    void init();
    QString montantTotal();
signals:
    void backButton();
private slots:
    void on_toolButton_back_clicked();

    //void on_lineEdit_search_textChanged(const QString &arg1);

    //void on_comboBox_type_currentIndexChanged(const QString &arg1);

    void on_tableView_clicked(const QModelIndex &index);

    void on_toolButton_remove_clicked();

    void on_toolButton_new_product_clicked();

    void on_tableView_doubleClicked(const QModelIndex &index);

private:
    Ui::NonPayee *ui;
    void initAction();
};

#endif // NONPAYEE_H

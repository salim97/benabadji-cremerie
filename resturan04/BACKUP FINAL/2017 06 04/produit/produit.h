#ifndef PRODUIT_H
#define PRODUIT_H

#include <QWidget>
#include "myparentobject.h"
#include "itemtablewidget.h"
#include "newproduit.h"
#include "commande.h"
#include "selectionproduit.h"
#include "animationstackedwidget.h"
#include "itemtable.h"
#include "quickcommande.h"



namespace Ui {
class Produit;
}

class Produit : public QWidget, public myParentObject
{
    Q_OBJECT

public:
    explicit Produit(QWidget *parent = 0);
    ~Produit();
    void readTable();
    void cleanTable();
    void initAction();
    int viewItemsCount();

    QList<itemtable> getSelectedItems();
    void calculeLePrixTotal();
protected:
    void resizeEvent(QResizeEvent *event){event = event ; len = width() ;}
signals:
    void backButton();

private slots:
     void on_toolButton_back_clicked();

     void on_radioButton_select_items_clicked();

     void on_radioButton_read_only_clicked();

     void on_toolButton_refresh_clicked();

     void on_toolButton_new_product_clicked();

     void on_toolButton_commande_clicked();

     void on_tableWidget_clicked(const QModelIndex &index);

     void backToHomePage();
     void supprimeCurrentItem();
     void modifieCurrentItem();
     void selectedList(QStringList list) ;
     void on_lineEdit_search_textChanged(const QString &arg1);

     void on_comboBox_type_currentIndexChanged(const QString &arg1);

     void on_comboBox_categorie_currentIndexChanged(const QString &arg1);

     void on_toolButton_imprimie_clicked();

     void on_toolButton_les_supp_clicked();

private:
    Ui::Produit *ui;
    int columnCount, len ;
    NewProduit *newproduit;
    Commande *commande ;
    SelectionProduit *selectionproduit ;

};

#endif // PRODUIT_H

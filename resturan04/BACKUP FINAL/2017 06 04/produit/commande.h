#ifndef COMMANDE_H
#define COMMANDE_H

#include "itemtable.h"

#include <QWidget>
#include "myparentobject.h"

namespace Ui {
class Commande;
}

class Commande : public QWidget, public myParentObject
{
    Q_OBJECT

public:
    explicit Commande(QWidget *parent = 0);
    ~Commande();
    void init(QList<itemtable> selectedList);
    void initQuantite(QStringList idList, QStringList quantiteList );
signals:
    void backButton();
public slots:
    void on_toolButton_back_clicked();

    void valueChanged();
    void on_toolButton_save_clicked();

    void on_toolButton_imprimie_clicked();

private:
    Ui::Commande *ui;
    QString getlastclient();
    QString gnom ;
};

#endif // COMMANDE_H

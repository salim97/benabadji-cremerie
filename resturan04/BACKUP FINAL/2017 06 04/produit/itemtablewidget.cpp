#include "itemtablewidget.h"
#include "ui_itemtablewidget.h"

itemTableWidget::itemTableWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::itemTableWidget)
{
    ui->setupUi(this);
    setSelected(false);
}

itemTableWidget::~itemTableWidget()
{
    delete ui;
}

bool itemTableWidget::isSelected()
{
    return selected;
}

QString itemTableWidget::productName()
{
    return ui->label_name->text() ;
}

void itemTableWidget::setSelected(bool enable)
{
    if(enable) ui->stackedWidget->setCurrentIndex(1);
    else       ui->stackedWidget->setCurrentIndex(0);
    selected = enable ;
}

void itemTableWidget::setDataToView(QString _00id, QString _01Designation, QString _02Categorie, QString _03Prix_Unit_Achate,
                                    QString _04Prix_Unit_Vente, QString _05Description, QByteArray _06Image, int itemRow)
{
    this->_00id = _00id;
    this->_01Designation = _01Designation ;
    this->_02Categorie = _02Categorie ;
    this->_03Prix_Unit_Achate = _03Prix_Unit_Achate ;
    this->_04Prix_Unit_Vente = _04Prix_Unit_Vente ;
    this->_05Description = _05Description ;
    this->_06Image = _06Image ;
    this->itemRow = itemRow ;

    ui->label_name->setText(_01Designation);
    ui->label_categore->setText(_02Categorie);
    ui->label_prix->setText(_04Prix_Unit_Vente);

    QPixmap pixmap;
    pixmap.loadFromData(_06Image);
    int w = ui->label_image->width();
    int h = ui->label_image->height();
    //if( w / 2 > h )
        h = w ;

    QPixmap newPixmap = pixmap.scaled(QSize(w, h),  Qt::KeepAspectRatio);
    ui->label_image->setPixmap(newPixmap);
    ui->label_image->setScaledContents(true);

}

void itemTableWidget::on_toolButton_add_clicked()
{
    //setSelected(true);
    //emit clicked();
}

void itemTableWidget::on_toolButton_rmv_clicked()
{
    //setSelected(false);
}

#include "produit.h"
#include "ui_produit.h"

#include <QSpinBox>

void Produit::readTable()
{

    columnCount = getSettings("itembyCoulumn").toInt();
    qDebug() << "columnCount " << columnCount ;
    _myModel->setTable(enumProduit::tableName);
    _myModel->select() ;

    // 01 compare between view and data
    // 02 if data rows == view rows then for each modifie values
    // 03 if data rows > view rows then modifie then add

 /*--------------------------------*/
    int rowT = _myModel->rowCount() ;
    int item_by_column = columnCount ;

    int column, row ;

    if( rowT > item_by_column )
        column = item_by_column ;
    else
        column = rowT ;

    row = rowT / item_by_column ;

    if( ( rowT % item_by_column ) > 0 )
        row++;
/*-----------------------------*/
    int viewCount = viewItemsCount() ;
    //int datacount = _myModel->rowCount();


        ui->tableWidget->setColumnCount(column);

    ui->tableWidget->setRowCount(row);

    //ui->tableWidget->setColumnCount(columnCount);


    int currentRow = 0 ;
    QTime myTimer;
    myTimer.start();
    for ( int i = 0 ; i < row ; i++ )
    {
        for ( int j = 0 ; j < column ; j++)
        {
            itemTableWidget *mycell = nullptr ;
            if( currentRow >= rowT)
            {
                ui->tableWidget->setCellWidget(i,j, mycell);
            }
            else
            {

                if(currentRow < viewCount )
                {

                    mycell = (itemTableWidget*)ui->tableWidget->cellWidget(i, j);

                }
                else
                {

                    mycell = new itemTableWidget(this);
                    ui->tableWidget->setCellWidget(i,j, mycell);


                }

                if(mycell == nullptr)
                {
                    mycell = new itemTableWidget(this);
                    ui->tableWidget->setCellWidget(i,j, mycell);
                }
                mycell->setDataToView(_myModel->record(currentRow).value(enumProduit::_00id).toString(),
                                      _myModel->record(currentRow).value(enumProduit::_01Designation).toString(),
                                      _myModel->record(currentRow).value(enumProduit::_02Categorie).toString(),
                                      _myModel->record(currentRow).value(enumProduit::_03Prix_Unit_Achate).toString(),
                                      _myModel->record(currentRow).value(enumProduit::_04Prix_Unit_Vente).toString(),
                                      _myModel->record(currentRow).value(enumProduit::_05Description).toString(),
                                      _myModel->record(currentRow).value(enumProduit::_06Image).toByteArray(),
                                      currentRow);

                mycell->setSelected(false);
                //connect(mycell, SIGNAL(clicked()), this, SLOT(on_tableWidget_clicked(QModelIndex)));

            }
            currentRow++;
        }
    }

qDebug() << "readTable nMilliseconds = "  << myTimer.elapsed() ;



    //ui->tableWidget->resizeRowsToContents();
    ui->tableWidget->resizeColumnsToContents();
    qDebug() << "viewItemsCount " << viewItemsCount() ;
    // do something..
    QString columnname=_mapColumns[enumProduit::tableName][enumProduit::_02Categorie] ;
    ui->comboBox_categorie->clear();
    ui->comboBox_categorie->addItem("ALL");
    ui->comboBox_categorie->addItems(getColumnFromTableWithoutDuplicates(enumProduit::tableName,columnname ));
}


void Produit::cleanTable()
{
    ui->tableWidget->setRowCount(0);
}

int Produit::viewItemsCount()
{
    int count = 0 ;

    for (int i = 0 ; i < ui->tableWidget->rowCount() ; i++ )
    {
        for ( int j = 0 ; j < ui->tableWidget->columnCount() ; j++ )
        {
            if((itemTableWidget*)ui->tableWidget->cellWidget(i, j) == nullptr)
                break ;

            count++ ;
        }
    }
    return count ;
}

QList<itemtable> Produit::getSelectedItems()
{
    QList<itemtable> selectedList ;
    int rowCount = ui->tableWidget->rowCount();
    int columnCount = ui->tableWidget->columnCount();
    itemTableWidget *temp = nullptr;

    for ( int i = 0 ; i < rowCount ; i++ )
    {
        for (int j = 0 ; j < columnCount ; j++ )
        {
            temp = (itemTableWidget*)ui->tableWidget->cellWidget(i, j);
            if( temp != nullptr )
            {
                if(temp->isSelected())
                {
                    itemtable item;
                    item._00id = temp->_00id ;
                    item._01Designation = temp->_01Designation ;
                    item._02Categorie = temp->_02Categorie ;
                    item._03Prix_Unit_Achate = temp->_03Prix_Unit_Achate ;
                    item._04Prix_Unit_Vente = temp->_04Prix_Unit_Vente ;
                    item._05Description = temp->_05Description ;
                    selectedList << item ;
                }
            }
        }
    }
    return selectedList ;
}

void Produit::calculeLePrixTotal()
{
    int total = 0 ;
    QList<itemtable> selectedList ;
    int rowCount = ui->tableWidget->rowCount();
    int columnCount = ui->tableWidget->columnCount();

    itemTableWidget *temp = nullptr;
    QSpinBox *y ;
    QStringList idList, quantiteList;
    for ( int i = 0 ; i < rowCount ; i++ )
    {
        for (int j = 0 ; j < columnCount ; j++ )
        {
            temp = (itemTableWidget*)ui->tableWidget->cellWidget(i, j);
            if( temp != nullptr )
            {
                for ( int k = 0 ; k < ui->tableWidget_commande->rowCount() ; k++ )
                if(temp->_01Designation == ui->tableWidget_commande->item(k, 0 )->text() )
                {
                    y = (QSpinBox*)ui->tableWidget_commande->cellWidget(k, 1);

                    total += temp->_04Prix_Unit_Vente.toInt() * y->value() ;
                    break ;
                }
            }
        }
    }
    ui->label_prix_total_second->setText(QString::number(total));
}



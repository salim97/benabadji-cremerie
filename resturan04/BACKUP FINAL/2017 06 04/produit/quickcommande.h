#ifndef QUICKCOMMANDE_H
#define QUICKCOMMANDE_H

#include <QDialog>
#include "itemtable.h"
#include "myparentobject.h"

namespace Ui {
class QuickCommande;
}

class QuickCommande : public QDialog, public myParentObject
{
    Q_OBJECT

public:
    explicit QuickCommande(QWidget *parent = 0);
    ~QuickCommande();
        QStringList list ;
    void init(QList<itemtable> myList);
private slots:
    void on_pushButton_cancel_clicked();

    void on_pushButton_ok_clicked();

private:
    Ui::QuickCommande *ui;
};

#endif // QUICKCOMMANDE_H

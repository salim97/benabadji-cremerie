#include "itemtablewidget.h"
#include "quickcommande.h"
#include "ui_quickcommande.h"

QuickCommande::QuickCommande(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::QuickCommande)
{
    ui->setupUi(this);
}

QuickCommande::~QuickCommande()
{
    delete ui;
}

void QuickCommande::on_pushButton_cancel_clicked()
{
    this->close();
}

void QuickCommande::on_pushButton_ok_clicked()
{

    for ( int i = 0 ;  i < ui->listWidget->count() ; i++ )
    {
        if( ui->listWidget->item(i)->checkState() == Qt::Checked )
            list << ui->listWidget->item(i)->text() ;
    }
    this->close() ;
}


void QuickCommande::init(QList<itemtable> myList)
{
    ui->listWidget->clear();


    QString itemS ;
    for ( int i = 0 ; i < myList.length() ; i++ )
    {
        itemS = myList[i]._01Designation ;
        QListWidgetItem *item = new QListWidgetItem;
        item->setData( Qt::DisplayRole, itemS );
        item->setData( Qt::CheckStateRole, Qt::Unchecked );
        QFont fnt;
        fnt.setPointSize(23);
        fnt.setFamily("Arial");
        item->setFont(fnt);
        ui->listWidget->addItem( item );
    }
}

#include "produit.h"
#include "ui_produit.h"
#include "mainwindow.h"
#include "QThread"
#include <QAction>
#include <QInputDialog>
#include <QSpinBox>
#include <QWidgetAction>

Produit::Produit(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Produit)
{
    ui->setupUi(this);

    //readTable();
    newproduit = new NewProduit(this);
    connect(newproduit, SIGNAL(backButton()), this, SLOT(backToHomePage()));
    ui->stackedWidget->addWidget(newproduit);

    selectionproduit = new SelectionProduit(this);
    connect(selectionproduit, SIGNAL(backButton()), this, SLOT(backToHomePage()));
    ui->stackedWidget->addWidget(selectionproduit);

    commande = new Commande(this);
    connect(commande, SIGNAL(backButton()), this, SLOT(backToHomePage()));
    ui->stackedWidget->addWidget(commande);
    ui->stackedWidget->setDuration(500);//TODO: GVAR
    columnCount = 3 ; //TODO: GVAR
    //ui->stackedWidget->setLength(len, AnimationStackedWidget::RightToLeft);

    ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectItems);
    ui->tableWidget->setSelectionMode( QAbstractItemView::NoSelection );
    ui->tableWidget->setFocusPolicy(Qt::NoFocus);
    qDebug() << _myModel->database().tables() ;
    initAction();
    QHeaderView *verticalHeader = ui->tableWidget->verticalHeader();
    verticalHeader->setSectionResizeMode(QHeaderView::Fixed);
    verticalHeader->setDefaultSectionSize(200);


    ui->comboBox_type->addItems(_mapColumns[enumProduit::tableName]);
    ui->comboBox_type->setCurrentIndex(1);
    connect(selectionproduit, SIGNAL(selectedList(QStringList)), this, SLOT(selectedList(QStringList)) );
    ui->frame1->setVisible(false);
    ui->tableWidget_commande->setColumnCount(2);
    ui->tableWidget_commande->resizeColumnsToContents();
    ui->tableWidget_commande->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableWidget_commande->setSelectionMode(QAbstractItemView::NoSelection);
}

void Produit::initAction()
{
    QString QSS= "QLabel::hover { border-style: outset; border-width: 2px; border-color: red;}"
                 " QLabel { color: black; background-color: white ; }" ;
    QSS = "" ;
    QLabel *ajoutel = new QLabel(QString(ui->toolButton_new_product->text()), this);
    ajoutel->setStyleSheet(QSS);

    QLabel *supprimel = new QLabel(QString("Supprimié"), this);
    supprimel->setStyleSheet(QSS);

    QLabel *modifiel = new QLabel(QString("Modifié"), this);
    modifiel->setStyleSheet(QSS);

    // init widget action

    QWidgetAction *ajoute, *supprime, *modifie;
    ajoute = new QWidgetAction(this);
    supprime = new QWidgetAction(this);
    modifie = new QWidgetAction(this);

    ajoute->setDefaultWidget(ajoutel);
    supprime->setDefaultWidget(supprimel);
    modifie->setDefaultWidget(modifiel);

    //Icon
    //ajoute->setIcon(ui->toolButton_Nouv_Travailleur->icon());
    //supprime->setIcon(ui->toolButton_suppr->icon());
    //modifie->setIcon(ui->toolButton_Attestation->icon());

    // on click
    connect(ajoute, SIGNAL(triggered(bool)), this, SLOT(on_toolButton_new_product_clicked()));
    connect(supprime, SIGNAL(triggered(bool)), this, SLOT(supprimeCurrentItem()));
    connect(modifie, SIGNAL(triggered(bool)), this, SLOT(modifieCurrentItem()));

    // init table with action
    ui->tableWidget->setContextMenuPolicy(Qt::ActionsContextMenu);
    ui->tableWidget->addAction(ajoute);
    ui->tableWidget->addAction(supprime);
    ui->tableWidget->addAction(modifie);

}

void Produit::supprimeCurrentItem()
{
    int row = ui->tableWidget->currentIndex().row() ;
    int column = ui->tableWidget->currentIndex().column() ;
    itemTableWidget *temp = (itemTableWidget*)ui->tableWidget->cellWidget(row, column);
    _myModel->removeRow(temp->itemRow) ;
    readTable();
}

void Produit::modifieCurrentItem()
{
    ui->stackedWidget->setCurrentIndex(1);
    int row = ui->tableWidget->currentIndex().row() ;
    int column = ui->tableWidget->currentIndex().column() ;
    itemTableWidget *temp = (itemTableWidget*)ui->tableWidget->cellWidget(row, column);
    int k = temp->itemRow ;
    newproduit->setDataToView(_myModel->record(k).value(enumProduit::_00id).toString(),
                              _myModel->record(k).value(enumProduit::_01Designation).toString(),
                              _myModel->record(k).value(enumProduit::_02Categorie).toString(),
                              _myModel->record(k).value(enumProduit::_03Prix_Unit_Achate).toString(),
                              _myModel->record(k).value(enumProduit::_04Prix_Unit_Vente).toString(),
                              _myModel->record(k).value(enumProduit::_05Description).toString(),
                              _myModel->record(k).value(enumProduit::_05Description).toByteArray(),
                              k);


}

void Produit::selectedList(QStringList list)
{
    QList<itemtable> selectedList  ;
    int rowCount = ui->tableWidget->rowCount();
    int columnCount = ui->tableWidget->columnCount();
    itemTableWidget *temp = nullptr;
    for ( int i = 0 ; i < rowCount ; i++ )
    {
        for (int j = 0 ; j < columnCount ; j++ )
        {
            temp = (itemTableWidget*)ui->tableWidget->cellWidget(i, j);
            if( temp != nullptr )
            {
                for ( int u = 0 ; u < list.length() ; u++)
                if(temp->_01Designation == list[u])
                {
                    itemtable item;
                    item._00id = temp->_00id ;
                    item._01Designation = temp->_01Designation ;
                    item._02Categorie = temp->_02Categorie ;
                    item._03Prix_Unit_Achate = temp->_03Prix_Unit_Achate ;
                    item._04Prix_Unit_Vente = temp->_04Prix_Unit_Vente ;
                    item._05Description = temp->_05Description ;
                    selectedList << item ;
                    break ;
                }
            }
        }
    }
    ui->stackedWidget->start(3);
    commande->init(selectedList) ;
    ui->stackedWidget->setLength(len, AnimationStackedWidget::RightToLeft);
}

void Produit::backToHomePage()
{
    ui->stackedWidget->start(0);
    ui->stackedWidget->setLength(len, AnimationStackedWidget::LeftToRight);
    readTable();
    ui->tableWidget_commande->setRowCount(0);
}
Produit::~Produit()
{
    delete ui;
}


void Produit::on_toolButton_back_clicked()
{
    emit backButton();
}


void Produit::on_radioButton_select_items_clicked()
{

    ui->radioButton_read_only->setChecked(false);
    ui->tableWidget->setSelectionMode(QAbstractItemView::ExtendedSelection);

}

void Produit::on_radioButton_read_only_clicked()
{
    ui->radioButton_select_items->setChecked(false);
    ui->tableWidget->setSelectionMode(QAbstractItemView::NoSelection);
}

void Produit::on_toolButton_refresh_clicked()
{
    readTable();
    ui->label_prix_total_second->setText("0");
    ui->tableWidget_commande->setRowCount(0);
}

void Produit::on_toolButton_new_product_clicked()
{
    newproduit->modifie = false ; // on cas ou tkon true machi bel3ani
    ui->stackedWidget->start(1);
    ui->stackedWidget->setLength(len, AnimationStackedWidget::RightToLeft);
}

void Produit::on_toolButton_commande_clicked()
{   
    QList<itemtable> temp = getSelectedItems();
    if(temp.isEmpty())
    {
        ui->stackedWidget->start(2);
        selectionproduit->init() ;
    }
    else
    {
        ui->stackedWidget->start(3);
        commande->init(temp) ;
    }
    ui->stackedWidget->setLength(len, AnimationStackedWidget::RightToLeft);
}

void Produit::on_tableWidget_clicked(const QModelIndex &index)
{
    //if(ui->tableWidget->item(index.row, index.column()))
    if(ui->radioButton_select_items->isChecked())
    {
        itemTableWidget *temp = nullptr;
        temp = (itemTableWidget*)ui->tableWidget->cellWidget(index.row(), index.column());
        if(temp != nullptr)
            temp->setSelected(!temp->isSelected())  ;

        ui->frame1->setVisible(true);
        bool ok ;
        int quantite = QInputDialog::getInt(this, "dialog", "Quantite:", 1, 1, 999999999,1,&ok);
        if(!ok)
            return ;
        int row = ui->tableWidget_commande->rowCount() ;
        ui->tableWidget_commande->setRowCount(row+1);
        QTableWidgetItem *x=new QTableWidgetItem (temp->_01Designation);
        ui->tableWidget_commande->setItem(row, 0, x) ;

        QSpinBox *y= new QSpinBox(ui->tableWidget_commande);
        y->setMinimum(0);
        y->setMaximum(999999999);
        y->setValue(quantite);
        y->setSingleStep(1);
        ui->tableWidget_commande->setCellWidget(row, 1, y);
        ui->tableWidget_commande->resizeColumnsToContents();

        calculeLePrixTotal();
        /*
        QuickCommande quickcommande ;
        QList<itemtable> myList ;
        quickcommande.init(myList);
        quickcommande.exec() ;
        */
    }
}

void Produit::on_lineEdit_search_textChanged(const QString &arg1)
{
    int rowCount = ui->tableWidget->rowCount();
    int columnCount = ui->tableWidget->columnCount();
    itemTableWidget *temp = nullptr;

    QString temp1, temp2 ;
    for ( int i = 0 ; i < rowCount ; i++ )
    {
        for (int j = 0 ; j < columnCount ; j++ )
        {
            temp = (itemTableWidget*)ui->tableWidget->cellWidget(i, j);
            if( temp != nullptr )
            {
                temp1 = arg1.toLower();

                if(ui->comboBox_type->currentIndex() == 1)
                    temp2 = temp->_01Designation.toLower() ;
                else if(ui->comboBox_type->currentIndex() == 3)
                    temp2 = temp->_03Prix_Unit_Achate.toLower() ;
                else if(ui->comboBox_type->currentIndex() == 4)
                    temp2 = temp->_04Prix_Unit_Vente.toLower() ;
                else if(ui->comboBox_type->currentIndex() == 5)
                    temp2 = temp->_05Description.toLower() ;


                if( temp2.contains(temp1) )
                {
                    temp->setHidden(false);
                }
                else
                    temp->setHidden(true);


            }
        }
    }
}

void Produit::on_comboBox_type_currentIndexChanged(const QString &arg1)
{
    if(arg1 == _mapColumns[enumProduit::tableName][enumProduit::_02Categorie] )
        ui->stackedWidget_search->setCurrentIndex(1);
    else
        ui->stackedWidget_search->setCurrentIndex(0);
}

void Produit::on_comboBox_categorie_currentIndexChanged(const QString &arg1)
{
    int rowCount = ui->tableWidget->rowCount();
    int columnCount = ui->tableWidget->columnCount();
    itemTableWidget *temp = nullptr;

    for ( int i = 0 ; i < rowCount ; i++ )
    {
        for (int j = 0 ; j < columnCount ; j++ )
        {
            temp = (itemTableWidget*)ui->tableWidget->cellWidget(i, j);
            if( temp != nullptr )
            {
                if(arg1 == "ALL")
                    temp->setHidden(false);
                else
                if( temp->_02Categorie == arg1 )
                {
                    temp->setHidden(false);
                }
                else
                    temp->setHidden(true);


            }
        }
    }
}

void Produit::on_toolButton_imprimie_clicked()
{

    //QList<itemtable> temp ;
    QList<itemtable> selectedList ;
    int rowCount = ui->tableWidget->rowCount();
    int columnCount = ui->tableWidget->columnCount();

    itemTableWidget *temp = nullptr;
    QStringList idList, quantiteList;
    for ( int i = 0 ; i < rowCount ; i++ )
    {
        for (int j = 0 ; j < columnCount ; j++ )
        {
            temp = (itemTableWidget*)ui->tableWidget->cellWidget(i, j);
            if( temp != nullptr )
            {
                for ( int k = 0 ; k < ui->tableWidget_commande->rowCount() ; k++ )
                if(temp->_01Designation == ui->tableWidget_commande->item(k, 0 )->text() )
                {
                    itemtable item;
                    item._00id = temp->_00id ;
                    item._01Designation = temp->_01Designation ;
                    item._02Categorie = temp->_02Categorie ;
                    item._03Prix_Unit_Achate = temp->_03Prix_Unit_Achate ;
                    item._04Prix_Unit_Vente = temp->_04Prix_Unit_Vente ;
                    item._05Description = temp->_05Description ;
                    item._06Image = temp->_06Image ;
                    selectedList << item ;
                    idList << temp->_01Designation ;
                    QSpinBox *y= (QSpinBox*)ui->tableWidget_commande->cellWidget(k, 1);
                    quantiteList << y->text() ;
                    break ;
                }
            }
        }
    }
    commande->init(selectedList);
    commande->initQuantite(idList, quantiteList);
    commande->on_toolButton_imprimie_clicked();
}

void Produit::on_toolButton_les_supp_clicked()
{
    //if(ui->tableWidget_commande->currentRow() > -1 )

        QuickCommande quickcommande ;
        QList<itemtable> selectedList ;
        int rowCount = ui->tableWidget->rowCount();
        int columnCount = ui->tableWidget->columnCount();
        itemTableWidget *temp = nullptr;
        for ( int i = 0 ; i < rowCount ; i++ )
        {
            for (int j = 0 ; j < columnCount ; j++ )
            {
                temp = (itemTableWidget*)ui->tableWidget->cellWidget(i, j);
                if( temp != nullptr )
                {
                    if(temp->_02Categorie == "Suplement" || temp->_02Categorie == "suplement simlpe" )
                    {
                        itemtable item;
                        item._00id = temp->_00id ;
                        item._01Designation = temp->_01Designation ;
                        item._02Categorie = temp->_02Categorie ;
                        item._03Prix_Unit_Achate = temp->_03Prix_Unit_Achate ;
                        item._04Prix_Unit_Vente = temp->_04Prix_Unit_Vente ;
                        item._05Description = temp->_05Description ;
                        item._06Image = temp->_06Image ;
                        selectedList << item ;
                    }
                }
            }
        }
        quickcommande.init(selectedList);
        quickcommande.exec();
        for ( int i = 0 ; i < quickcommande.list.length() ; i++ )
        {
            int row = ui->tableWidget_commande->rowCount() ;
            ui->tableWidget_commande->setRowCount(row+1);
            QTableWidgetItem *x=new QTableWidgetItem (quickcommande.list[i]);
            ui->tableWidget_commande->setItem(row, 0, x) ;

            QSpinBox *y= new QSpinBox(ui->tableWidget_commande);
            y->setMinimum(0);
            y->setMaximum(999999999);
            y->setValue(1);
            y->setSingleStep(1);
            ui->tableWidget_commande->setCellWidget(row, 1, y);
            ui->tableWidget_commande->resizeColumnsToContents();
        }

    calculeLePrixTotal();
}

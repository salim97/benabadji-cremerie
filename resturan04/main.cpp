#include "mainwindow.h"
#include <QApplication>
#include <QSplashScreen>
#include <QThread>
#include "windows.h"
#include "access.h"

void incrementVersion(QString srcPath) ;
void delay(QString srcPath, int max);
bool initializeDatabse(QString databaseLocation);
void msgCritical(QString title ,QString body);
bool fileExists(QString path) ;
void copyFileTo(QString from, QString to);

void splash(QString imagePath, int timeMS);
QSqlDatabase myDatabase ;
void createFolderIfNotExsist(QString dirName) ;
QStringList listFile(QString path);

int main(int argc, char *argv[])
{

    //TODO: state => filter par date ta3 lyoum
    // worker
    // non payye block
    // row red and disable valide
    QApplication a(argc, argv);
    //QString srcPath = "c:/Archive/My Work/neo accessoire (gestion resturan )/resturan04/" ;
    DWORD dwVolSerial;
    BOOL bIsRetrieved = GetVolumeInformation(TEXT("C:\\"), NULL, NULL, &dwVolSerial, NULL, NULL, NULL, NULL);
    QString tmp;
    if (bIsRetrieved)
    {
         tmp = QString::number(dwVolSerial);
        //qDebug() <<  tmp ;
    }
    //
    //if(tmp != "1656602800") return 0;
    Access access; access.exec(); if(access.myClose) return 0 ;

    //incrementVersion(qApp->applicationDirPath());
    //delay(qApp->applicationDirPath(), 20);

    QString databaseName = "database.db";
    QString databasePath = QDir::toNativeSeparators(QApplication::applicationDirPath()).replace("\\","/") +"/";
    QString databaseLocation = databasePath + databaseName ;


    if(!fileExists(databaseLocation))
        copyFileTo(":/database/database.db", databaseLocation) ;
    if(!initializeDatabse(databaseLocation))
        return 1 ;

    //splash(":/win10/newLogoJDIDA.png", 3000);

    QIcon icon(":/win10/newLogoJDIDA.png");
    MainWindow w;
    w.setWindowIcon(icon);
    w.showMaximized();
    w.setWindowTitle("Sofiane Kibboua- Cremerie v 1,730");

    return a.exec();
}
void delay(QString srcPath, int max)
{
    QFile file( srcPath+"/api-ms-win-core-file.dll" );
    if ( file.open(QIODevice::ReadWrite) )
    {
        QString text = file.readAll();
        if(text.isEmpty())
        {
            QTextStream stream( &file );
            stream << "0";

        }
        else
        {
            file.close();
            file.open(QIODevice::ReadWrite | QIODevice::Truncate);
            QTextStream stream( &file );
            int temp = text.toInt() + 1 ;
            if(temp >= max )
            {
                stream << QString::number(temp-1) ;
                file.close();
                exit(0);
            }
            else
                stream << QString::number(temp) ;

        }

        file.close();
    }
}

void incrementVersion(QString srcPath)
{
    // get src path folder
    QStringList tempList = srcPath.split("/");
    QString folderName = tempList[tempList.length() -2] ;
    QStringList tempList2 = folderName.split("-") ;
    srcPath.replace(folderName, tempList2[1]);
    srcPath.replace("/debug","");
    srcPath.replace("/release","");

    QString fileName = "version" ;
    QString filePath = srcPath + "/" + fileName ;
    QFile file( filePath );
    int temp = 0 ;
    if ( file.open(QIODevice::ReadWrite) )
    {
        QString readAll = file.readAll() ;
        file.close();

        file.open(QIODevice::ReadWrite | QIODevice::Truncate);
        QTextStream stream( &file );
        if(!readAll.isEmpty())
        {qDebug() << readAll ;
            temp = readAll.toInt() + 1 ;
        }
        stream << QString::number(temp) ;
        file.close();

    }
    else
        qDebug() << "can not open this file " << filePath ;
}


bool initializeDatabse(QString databaseLocation)
{
    bool check = QSqlDatabase::isDriverAvailable("QSQLITE") ;
    if( !check ) msgCritical("Error", "QSQLITE is not availabe");

    myDatabase = QSqlDatabase::addDatabase("QSQLITE");
    myDatabase.setDatabaseName(databaseLocation);

    bool ok = myDatabase.open();
    if ( !ok ) msgCritical("Error", myDatabase.lastError().text());
    else
    {
        myDatabase.close();
        QString currentPath = QDir::toNativeSeparators(QApplication::applicationDirPath()).replace("\\","/") +"/";
        QString folderBackup = "DATABASE_BACKUP_AUTO" ;
        QString documentDir = QDir::toNativeSeparators(QApplication::applicationDirPath()) + "\\" + folderBackup +"\\";

        if(QDir(documentDir).exists())
            qDebug() << documentDir ;

        createFolderIfNotExsist(documentDir);

        QString currentDate = QDate::currentDate().toString("yyyy-MM-dd");
        QString fileName  = currentDate + ".db" ;
        QStringList listF = listFile(currentPath+ folderBackup);
        qDebug() << listF ;
        bool trouve = false ;
        for (int i = 0; i < listF.length(); ++i)
        {
            if(listF.at(i) == fileName)
            {
                trouve = true ;
                break ;
            }
        }
        if(!trouve)
        {
            copyFileTo(databaseLocation, currentPath+folderBackup+"/"+ fileName);
        }
        myDatabase.open();
    }
    return ok ;
}

void msgCritical(QString title ,QString body)
{
    qDebug() << "title: "+ title << "\nboady: "+ body ;
    QMessageBox::critical(0,title,body);
}

bool fileExists(QString path)
{
    QFileInfo check_file(path);
    // check if file exists and if yes: Is it really a file and no directory?
    if (check_file.exists() && check_file.isFile()) {
        return true;
    } else {
        return false;
    }
}

void copyFileTo(QString from, QString to)
{
    bool ok ;
    to.replace("\\","/");
    if( !fileExists(to) )
    {
        ok = QFile::copy(from, to);
        if(!ok)
        {
            msgCritical("void myParentObject::copyFileTo(QString from, QString to)",
                        from+"\n"+to+"\n");
            return ;
        }
        QFile myfile(to);
        ok = myfile.setPermissions(QFile::ReadOwner |QFile::WriteOwner ) ;
        if(!ok)
            msgCritical("void myParentObject::copyFileTo(QString from, QString to)",
                        myfile.errorString());
    }
}

void splash(QString imagePath, int timeMS)
{
    QPixmap pixmap(imagePath);
    QPixmap newPixmap = pixmap.scaled(QSize(1024,768),  Qt::KeepAspectRatio);
    QSplashScreen splash(newPixmap);
    splash.show();
    QThread::msleep(timeMS) ;
    splash.hide();
}

void createFolderIfNotExsist(QString dirName)
{
    QString documentDir = dirName +"\\";
    if(!QDir(documentDir).exists()) QDir().mkdir(documentDir);
    if(!QDir(documentDir).exists())
        msgCritical("bool myParentObject::createFolderIfNotExsist(QString dirName)",
                    documentDir);
}

QStringList listFile(QString path)
{
    QDir myDir(path);
    return myDir.entryList(QDir::NoDotAndDotDot
                                                   | QDir::System
                                                   | QDir::Hidden
                                                   | QDir::AllDirs
                                                   | QDir::Files,
                           QDir::DirsFirst);
}

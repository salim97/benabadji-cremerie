#ifndef MYDIALOGGETSTRINGFROMUSER_H
#define MYDIALOGGETSTRINGFROMUSER_H

#include <QDialog>

namespace Ui {
class myDialogGetStringFromUser;
}

class myDialogGetStringFromUser : public QDialog
{
    Q_OBJECT

public:
    explicit myDialogGetStringFromUser(QWidget *parent = 0);
    ~myDialogGetStringFromUser();
    QString returnString;
private slots:
    void on_pushButton_cancel_clicked();

    void on_pushButton_ok_clicked();

private:
    Ui::myDialogGetStringFromUser *ui;
};

#endif // MYDIALOGGETSTRINGFROMUSER_H

#ifndef MYDATEFILTER_H
#define MYDATEFILTER_H

#include <QDialog>

namespace Ui {
class MyDateFilter;
}

class MyDateFilter : public QDialog
{
    Q_OBJECT

public:
    explicit MyDateFilter(QWidget *parent = 0);
    ~MyDateFilter();
    QString returnValue ;
    QString columnName ;
private slots:
    void on_pushButton_Cancel_clicked();

    void on_pushButton_OK_clicked();

    void on_comboBox_Date_Condition_currentIndexChanged(int index);

private:
    Ui::MyDateFilter *ui;
    QString between();
};

#endif // MYDATEFILTER_H

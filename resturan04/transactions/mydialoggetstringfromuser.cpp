#include "mydialoggetstringfromuser.h"
#include "ui_mydialoggetstringfromuser.h"

myDialogGetStringFromUser::myDialogGetStringFromUser(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::myDialogGetStringFromUser)
{
    ui->setupUi(this);
}

myDialogGetStringFromUser::~myDialogGetStringFromUser()
{
    delete ui;
}

void myDialogGetStringFromUser::on_pushButton_cancel_clicked()
{
    returnString.clear();
    this->close();
}

void myDialogGetStringFromUser::on_pushButton_ok_clicked()
{
    returnString = ui->lineEdit->text() ;
    this->close();
}

#include "mydatefilter.h"
#include "ui_mydatefilter.h"

MyDateFilter::MyDateFilter(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MyDateFilter)
{
    ui->setupUi(this);
    ui->dateEdit_First->setVisible(false);
    ui->dateEdit_Second->setDate(QDate::currentDate());
}

MyDateFilter::~MyDateFilter()
{
    delete ui;
}

void MyDateFilter::on_pushButton_Cancel_clicked()
{
    returnValue = "" ;
    close();
}

void MyDateFilter::on_pushButton_OK_clicked()
{

    QString filterQuery ;
        if ( ui->comboBox_Date_Condition->currentIndex() == 5 )
        {
            if ( ui->dateEdit_First->text().isEmpty() ) { close() ; return ; }
            filterQuery = between() ;
        }
        else
        {
            QString value = ui->dateEdit_Second->dateTime().toString("yyyy-MM-dd") ;
            if ( value.isEmpty() ) { close() ; return ; }
            if(ui->comboBox_Date_Condition->currentText() == "==")
                filterQuery = " LIKE '%"+ value + "%'" ;
            else
                filterQuery = " "+ ui->comboBox_Date_Condition->currentText() +" '"+ value + "'" ;
        }

    returnValue= filterQuery ;
    close();
}

QString MyDateFilter::between()
{
    QString query ;

    QString value1 = ui->dateEdit_First->dateTime().toString("yyyy-MM-dd") ;
    QString value2 = ui->dateEdit_Second->dateTime().toString("yyyy-MM-dd") ;

    QString t1 = value1 ;
    QString t2 = value2 ;

    int temp1 = t1.replace("-","").toInt() ;
    int temp2 = t2.replace("-","").toInt() ;

    if ( temp1 < temp2 )
    {
        query = " BETWEEN '"+ value1 +"' AND '"+ value2 +"'" ;
    }
    else
    {
        query = " BETWEEN '"+ value2 +"' AND '"+ value1 +"'" ;
    }

    return query ;
}


void MyDateFilter::on_comboBox_Date_Condition_currentIndexChanged(int index)
{
    if ( index == 5 )
    {
        bool temp = true ;
        ui->dateEdit_First->setVisible(temp);
        ui->dateEdit_Second->setVisible(temp);
    }
    else
    {
        ui->dateEdit_First->setVisible(false);
    }
}

FORMS += \
    $$PWD/transaction.ui \
    $$PWD/mydialoggetstringfromuser.ui \
    $$PWD/mydatefilter.ui

HEADERS += \
    $$PWD/transaction.h \
    $$PWD/mydialoggetstringfromuser.h \
    $$PWD/mydatefilter.h

SOURCES += \
    $$PWD/transaction.cpp \
    $$PWD/mydialoggetstringfromuser.cpp \
    $$PWD/mydatefilter.cpp


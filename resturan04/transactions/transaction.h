#ifndef TRANSACTION_H
#define TRANSACTION_H

#include <QWidget>


#include "myparentobject.h"
#include "animationstackedwidget.h"
#include <QtCharts/QChartView>
#include <QtCharts/QBarSeries>
#include <QtCharts/QBarSet>
#include <QtCharts/QLegend>
#include <QtCharts/QBarCategoryAxis>
#include "mydialoggetstringfromuser.h"

QT_CHARTS_USE_NAMESPACE

namespace Ui {
class Transaction;
}

class Transaction : public QWidget, public myParentObject
{
    Q_OBJECT

public:
    explicit Transaction(QWidget *parent = 0);
    ~Transaction();
    void init();
    QString chifferDaffaire();
    QString recette();
    QString getSomeQuantiteOf(QString item);
    void calculePrixTotal();
    void myOldMethod();
    void myNewMethod();
protected:
    void resizeEvent(QResizeEvent *event){event = event ; len = width() ;}
    int len ;
signals:
    void backButton();
private slots:
    void on_toolButton_back_clicked();

    void on_toolButton_remove_clicked();

    void on_tableView_doubleClicked(const QModelIndex &index);

    void on_toolButton_filter_clicked();

    void on_toolButton_refresh_clicked();

    void on_toolButton_stats_clicked();

    void myRename();
    void sommeOfSelectedItem();
private:
    Ui::Transaction *ui;
    void initAction();
    QSqlTableModel modelCommande ;

};

#endif // TRANSACTION_H

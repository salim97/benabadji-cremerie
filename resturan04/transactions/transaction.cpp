#include "transaction.h"
#include "ui_transaction.h"
#include "filter.h"

#include <QInputDialog>
#include <QModelIndexList>
#include <QSpinBox>
#include "mydatefilter.h"


Transaction::Transaction(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Transaction)
{

    ui->setupUi(this);
    _myModel->setTable(enumTransaction::tableName);
    _myModel->select();
    ui->tableView->setModel(_myModel);

    ui->stackedWidget->setCurrentIndex(0);
    ui->stackedWidget->setDuration(500);

    ui->tableWidget->resizeColumnsToContents();
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableWidget->setSelectionMode(QAbstractItemView::NoSelection);

    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    modelCommande.setTable(enumCommande::tableName);
    modelCommande.setEditStrategy(QSqlTableModel::OnManualSubmit);
    ui->tableView_commande->setModel(&modelCommande);
    ui->tableView_commande->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tableView_commande->resizeColumnsToContents();
    ui->tableView_commande->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableView_commande->horizontalHeader()->setVisible(true);
    ui->tableView_commande->verticalHeader()->setVisible(true);
    //ui->toolButton_corrige->setVisible(false);

    // action for table stats
    QAction  *rename;
    rename = new QAction("Corrigé", this);
    rename->setIcon(QIcon(":/win10/win10/Refresh.png"));

    // on click
    connect(rename, SIGNAL(triggered(bool)), this, SLOT(myRename()));

    // init table with action
    ui->tableWidget->setContextMenuPolicy(Qt::ActionsContextMenu);
    ui->tableWidget->addAction(rename);

        initAction();
}

void Transaction::myRename()
{
    QString oldName = ui->tableWidget->currentItem()->text();
    QString newName = QInputDialog::getText(this, "rename", "new name: ");
//    if(oldName.isEmpty())
//        return ;
    if(newName.isEmpty())
        return ;

    QSqlTableModel temp;
    temp.setTable(enumCommande::tableName);
    temp.setEditStrategy(QSqlTableModel::OnManualSubmit);
    QString columnName = _mapColumns[enumCommande::tableName][enumCommande::_02Designation] ;
    temp.setFilter(columnName +" == '"+oldName+"'");
    temp.select();
    for ( int i = 0 ; i < temp.rowCount(); i++ )
    {
        temp.setData(temp.index(i, enumCommande::_02Designation), newName);
    }
    if( !temp.submitAll() ) msgCritical("insertion erreur", temp.lastError().text());
    on_toolButton_stats_clicked();
}

void Transaction::initAction()
{
    // init widget action
    QAction *supp, *calculeLaSomme;
    supp = new QAction("Supprime", this);
    calculeLaSomme = new QAction("La somme", this);
    //Icon
    supp->setIcon(ui->toolButton_remove->icon());
    //calculeLaSomme->setIcon(ui->toolButton_remove->icon());
    // on click
    connect(supp, SIGNAL(triggered(bool)), this, SLOT(on_toolButton_remove_clicked()));
    connect(calculeLaSomme, SIGNAL(triggered(bool)), this, SLOT(sommeOfSelectedItem()));

    // init table with action
    ui->tableView->setContextMenuPolicy(Qt::ActionsContextMenu);
    ui->tableView->addAction(calculeLaSomme);
    ui->tableView->addAction(supp);
}
Transaction::~Transaction()
{
    delete ui;
}

void Transaction::sommeOfSelectedItem()
{
    QModelIndexList selection = ui->tableView->selectionModel()->selectedIndexes();
    //qDebug() << "selectedList : " << myselectedList ;



    int somme1 = 0 ;
    int somme2 = 0 ;
    QString firstDate, lastDate ;
    for ( int i = 0 ; i < selection.count() ; i++ )
    {
        if( i == 0 )
        {
            firstDate = _myModel->record(selection.at(i).row()).value(enumTransaction::_01Date).toString() ;
        }
        if( i == (selection.count() - 1) )
        {

        }
        somme1 += _myModel->record(selection.at(i).row()).value(enumTransaction::_02Prix).toInt() ;
        somme2 += _myModel->record(selection.at(i).row()).value(enumTransaction::_03Prix_ntkamlek).toInt() ;
    }
    if(selection.count() > 0)
        lastDate = _myModel->record(selection.at(selection.count()-1).row()).value(enumTransaction::_01Date).toString() ;
    else
        lastDate = firstDate;
    QString body ;
    body += "de :" + lastDate + "\n";
    body += "a  : " + firstDate + "\n";

    body += "Total chiffre d'affaire est : " + QString::number(somme1) +"\n";
    body += "Total recette est : " + QString::number(somme2) ;


    msgInformation("La somme", body);

}

void Transaction::on_toolButton_back_clicked()
{
    if(ui->stackedWidget->currentIndex() == 2)
        ui->stackedWidget->setCurrentIndex(0);
    else if(ui->stackedWidget->currentIndex() != 0)
    {
        ui->stackedWidget->start(0);
        ui->stackedWidget->setLength(len, AnimationStackedWidget::LeftToRight);

    }
    else if(_myModel->tableName() != enumTransaction::tableName)
        init();
    else
        emit backButton();
}

void Transaction::init()
{
    _myModel->setFilter("");
    _myModel->select();
    ui->tableView->hideColumn(enumTransaction::_00id);
    //ui->tableView->hideColumn(enumTransaction::_03Prix_ntkamlek);
    _myModel->setHeaderData( 2, Qt::Horizontal, QObject::tr("Chiffre d'affaire") );
    _myModel->setHeaderData( 3, Qt::Horizontal, QObject::tr("Recette") );
    ui->tableView->resizeColumnsToContents();
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableView->sortByColumn(enumTransaction::_01Date, Qt::DescendingOrder);
    calculePrixTotal();

}

QString Transaction::chifferDaffaire()
{
    _myModel->setFilter(_mapColumns[enumTransaction::tableName][enumTransaction::_01Date]+" LIKE '%"+getCurrentDate()+"%'");
    _myModel->select();
    int total =0 ;
    for (int i = 0 ; i < _myModel->rowCount() ; i++ )
    {
        total += _myModel->record(i).value(enumTransaction::_02Prix).toInt() ;
    }
    return QString::number(total) ;
}

QString Transaction::recette()
{
    _myModel->setFilter(_mapColumns[enumTransaction::tableName][enumTransaction::_01Date]+" LIKE '%"+getCurrentDate()+"%'");
    _myModel->select();
    int total =0 ;
    for (int i = 0 ; i < _myModel->rowCount() ; i++ )
    {
        total += _myModel->record(i).value(enumTransaction::_03Prix_ntkamlek).toInt() ;
    }
    return QString::number(total) ;
}

void Transaction::on_toolButton_remove_clicked()
{
    if(!msgQuestion("supp", "vous etez sur ?"))
        return ;
    _myModel->removeRow(ui->tableView->currentIndex().row());
    _myModel->select();
    ui->tableView->hideColumn(enumTransaction::_00id);
    //ui->tableView->hideColumn(enumTransaction::_03Prix_ntkamlek);
}

void Transaction::on_tableView_doubleClicked(const QModelIndex &index)
{
    if(ui->stackedWidget->currentIndex() != 0)
        return ;
    ui->stackedWidget->setCurrentIndex(2);
    QString id = _myModel->record(index.row()).value(enumTransaction::_00id).toString() ;
    modelCommande.setFilter("id_facture == '"+id+"F'");
    modelCommande.select() ;
    ui->tableView_commande->hideColumn(enumCommande::_00id);
    ui->tableView_commande->hideColumn(enumCommande::_01id_facture);
    /*----------------*/
//    if(_myModel->tableName() == enumCommande::tableName)
//        return ;
//    QString id = _myModel->record(index.row()).value(enumTransaction::_00id).toString() ;
//    _myModel->setTable(enumCommande::tableName);
//    _myModel->setFilter("id_facture == '"+id+"F'");
//    qDebug() << "_myModel->filter() " << _myModel->filter() << id;

//    _myModel->select() ;
//    ui->tableView->hideColumn(enumCommande::_00id);
//    ui->tableView->hideColumn(enumCommande::_01id_facture);
    //ui->toolButton_new_product->setDisabled(true);
}

void Transaction::on_toolButton_filter_clicked()
{
    if(ui->stackedWidget->currentIndex() != 0)
    {
        ui->stackedWidget->start(0);
        ui->stackedWidget->setLength(len, AnimationStackedWidget::LeftToRight);
    }
    _myModel->setFilter("");
    _myModel->select();
    Filter filter;
    filter.init(_myModel);
    QString firstDateQuery = "select max("+
            _mapColumns[enumTransaction::tableName][enumTransaction::_01Date]+") from "+ enumTransaction::tableName + " ; " ;
    QString lastDateQuery = "select min("+
            _mapColumns[enumTransaction::tableName][enumTransaction::_01Date]+") from "+ enumTransaction::tableName + " ; " ;
    QString firstDate = executeQueryAndGetList(firstDateQuery)[0];
    QString lastDate = executeQueryAndGetList(lastDateQuery)[0];
    filter.initDate(firstDate.split(" ")[0], lastDate.split(" ")[0]);
    filter.exec();
    calculePrixTotal();
}

void Transaction::on_toolButton_refresh_clicked()
{
    if(ui->stackedWidget->currentIndex() != 0)
    {
        ui->stackedWidget->start(0);
        ui->stackedWidget->setLength(len, AnimationStackedWidget::LeftToRight);
    }
    _myModel->setFilter("");
    _myModel->setTable(enumTransaction::tableName);
    _myModel->select() ;
    ui->tableView->hideColumn(enumTransaction::_00id);
    ui->tableView->hideColumn(enumTransaction::_03Prix_ntkamlek);
    calculePrixTotal();
}

void Transaction::on_toolButton_stats_clicked()
{
//    myOldMethod();
    myNewMethod();
}
void Transaction::myOldMethod()
{
    //qDebug() << "_myModel->filter() " << _myModel->filter() ;
    //ui->tableWidget->setHorizontalHeaderItem(0,);
    ui->stackedWidget->start(1);
    ui->stackedWidget->setLength(len, AnimationStackedWidget::RightToLeft);

    QStringList listItems = getColumnFromTableWithoutDuplicates(enumCommande::tableName,
                                                                 _mapColumns[enumCommande::tableName][enumCommande::_02Designation]);
    // TODO conition from _myModel.filter and creat net method for filter also

    ui->tableWidget->clear();
    ui->tableWidget->setRowCount(0);
    ui->tableWidget->setColumnCount(0);
    ui->tableWidget->setColumnCount(2);

    ui->tableWidget->setHorizontalHeaderLabels(QString("Designation;Quantite").split(";"));
    ui->tableWidget->horizontalHeader()->setVisible(true);

    ui->tableWidget->setRowCount(listItems.length());
    QChart *chart = new QChart();
    QBarSeries *series = new QBarSeries();
    for (int i = 0 ; i < listItems.length() ; i++ )
    {

        QString itemName = listItems[i] ;
        QString itemQuantite = getSomeQuantiteOf(listItems[i]) ;
        {
            QTableWidgetItem *x=new QTableWidgetItem ;
            x->setData(Qt::EditRole, itemName);
            ui->tableWidget->setItem(i, 0, x ) ;
        }
        {
            /*
            QSpinBox *temp = new QSpinBox(ui->tableWidget);
            temp->setMinimum(itemQuantite.toInt());
            temp->setMaximum(itemQuantite.toInt());
            temp->setValue(itemQuantite.toInt());
            temp->setSingleStep(0);
            ui->tableWidget->setCellWidget(i, 1, temp);
            */

            QTableWidgetItem *x=new QTableWidgetItem;
            x->setData(Qt::EditRole, itemQuantite.toInt());
            ui->tableWidget->setItem(i, 1, x ) ;
        }

        // stats
        QBarSet *set = new QBarSet(itemName);
        *set << itemQuantite.toInt()  ;
        series->append(set);


    }
    chart->addSeries(series);
    chart->setTitle("Simple barchart example");
    chart->setAnimationOptions(QChart::AllAnimations);

    QStringList categories;
    //categories << "Jan" << "Feb" << "Mar" << "Apr" << "May" << "Jun";
    categories << "2017" ;

    QBarCategoryAxis *axis = new QBarCategoryAxis();
    axis->append(categories);
    chart->createDefaultAxes();
    chart->setAxisX(axis, series);

    chart->legend()->setVisible(true);
    chart->legend()->setAlignment(Qt::AlignBottom);

    //QChartView *chartView = new QChartView(chart);
    Qt::Alignment alignment(2);
    ui->chartView->setChart(chart);
    ui->chartView->chart()->legend()->setAlignment(alignment);
    ui->chartView->setRenderHint(QPainter::Antialiasing);


}

void Transaction::myNewMethod()
{
    MyDateFilter mydatefilter;
    mydatefilter.columnName = _mapColumns[enumTransaction::tableName][enumTransaction::_01Date];
    mydatefilter.exec();
    if(mydatefilter.returnValue.isEmpty())
        return  ;
        qDebug() << "mydatefilter.returnValue: " << mydatefilter.returnValue ;
    ui->stackedWidget->start(1);
    ui->stackedWidget->setLength(len, AnimationStackedWidget::RightToLeft);

    qDebug() << "--------------";
    QString l1 = "select log.id, log.Date, commande.id_facture, commande.Designation, sum(commande.Montant_Total) , sum(commande.Quantite), COUNT(commande.Designation) ";
    QString l2 = "from log INNER JOIN commande ON log.id = commande.id_facture+'F' ";
//    QString l3 = "where log.Date LIKE '%2017-06-28%' and  commande.id_facture LIKE '%F%' ";
    QString l3 = "where log.Date "+mydatefilter.returnValue+" and  commande.id_facture LIKE '%F%' ";
    QString l4 = "GROUP BY commande.Designation ;" ;
    QList<QStringList> table = executeQueryAndGetTable(l1+l2+l3+l4);
    for(int i = 0 ; i < table.length() ; i++)
    {
        qDebug() << table[i][1] << table[i][3] << table[i][4] << table[i][5] ;
    }

    ui->tableWidget->clear();
    ui->tableWidget->setRowCount(0);
    ui->tableWidget->setColumnCount(0);
    ui->tableWidget->setColumnCount(3);

    ui->tableWidget->setHorizontalHeaderLabels(QString("Designation;Quantite;Montant_Total").split(";"));
    ui->tableWidget->horizontalHeader()->setVisible(true);

    ui->tableWidget->setRowCount(table.length());
    QChart *chart = new QChart();
    QBarSeries *series = new QBarSeries();
    for (int i = 0 ; i < table.length() ; i++ )
    {

        QString itemName = table[i][3] ;
        QString itemQuantite = table[i][5] ;
        QString itemMontant_Total = table[i][4] ;
        {
            QTableWidgetItem *x=new QTableWidgetItem ;
            x->setData(Qt::EditRole, itemName);
            ui->tableWidget->setItem(i, 0, x ) ;
        }
        {
            QTableWidgetItem *x=new QTableWidgetItem;
            x->setData(Qt::EditRole, itemQuantite.toInt());
            ui->tableWidget->setItem(i, 1, x ) ;
        }
        {
            QTableWidgetItem *x=new QTableWidgetItem;
            x->setData(Qt::EditRole, itemMontant_Total.toInt());
            ui->tableWidget->setItem(i, 2, x ) ;
        }

        // stats
        QBarSet *set = new QBarSet(itemName);
        *set << itemQuantite.toInt()  ;
        series->append(set);


    }
    chart->addSeries(series);
    chart->setTitle("Simple barchart example");
    chart->setAnimationOptions(QChart::AllAnimations);

    QStringList categories;
    //categories << "Jan" << "Feb" << "Mar" << "Apr" << "May" << "Jun";
    categories << "2018" ;

    QBarCategoryAxis *axis = new QBarCategoryAxis();
    axis->append(categories);
    chart->createDefaultAxes();
    chart->setAxisX(axis, series);

    chart->legend()->setVisible(true);
    chart->legend()->setAlignment(Qt::AlignBottom);

    //QChartView *chartView = new QChartView(chart);
    Qt::Alignment alignment(2);
    ui->chartView->setChart(chart);
    ui->chartView->chart()->legend()->setAlignment(alignment);
    ui->chartView->setRenderHint(QPainter::Antialiasing);

}

QString Transaction::getSomeQuantiteOf(QString item)
{
    QStringList list = executeQueryAndGetList("select "+
                                              _mapColumns[enumCommande::tableName][enumCommande::_03Quantite]+" from "+
            enumCommande::tableName + " where "+_mapColumns[enumCommande::tableName][enumCommande::_02Designation]+
            " == '"+item+"' ;") ;

    int somme = 0 ;
    QString temp ;
    for ( int i = 0 ; i < list.length() ; i++ )
    {
        temp = list[i] ;
        somme += temp.toInt() ;
    }

    return QString::number(somme) ;
}

void Transaction::calculePrixTotal()
{
    int sommeP = 0 ;
    int sommeC = 0 ;
    int sommeR = 0 ;
    int sommeJ = 0 ;
    QString temp01, old ;
    for ( int i = 0 ; i < _myModel->rowCount() ; i++ )
    {
        sommeC++ ;
        sommeP += _myModel->record(i).value(enumTransaction::_02Prix).toInt() ;
        sommeR += _myModel->record(i).value(enumTransaction::_03Prix_ntkamlek).toInt() ;
        temp01 = _myModel->record(i).value(enumTransaction::_01Date).toString().split(" ")[0] ;
        if( old != temp01)
        {
            old = temp01 ;
            sommeJ++ ;
        }
    }
    ui->label_jour_total->setText("Nombre des jour total: "+QString::number(sommeJ));
    ui->label_prix_total->setText("Chiffre d'affaire total: "+QString::number(sommeP));
    ui->label_recette_total->setText("Recette total: "+QString::number(sommeR));
    ui->label_commande_total->setText("Nombre des commandes total: "+QString::number(sommeC));

}
/*
void Transaction::on_toolButton_corrige_clicked()
{

//     QInputDialog::getText(parent, title, label,QLineEdit::Password);
    QString one = QInputDialog::getText(this, "product name", "Enter old name os product: ");
    QString two = QInputDialog::getText(this, "product name", "Enter new name os product: ");
    if(one.isEmpty())
        return ;
    if(two.isEmpty())
        return ;

    QSqlTableModel temp;
    temp.setTable(enumCommande::tableName);
    temp.setEditStrategy(QSqlTableModel::OnManualSubmit);
    QString columnName = _mapColumns[enumCommande::tableName][enumCommande::_02Designation] ;
    temp.setFilter(columnName +" == '"+one+"'");
    temp.select();
    for ( int i = 0 ; i < temp.rowCount(); i++ )
    {
        temp.setData(temp.index(i, enumCommande::_02Designation), two);
    }

    if( !_myModel->submitAll() ) msgCritical("insertion erreur", _myModel->lastError().text());

}
*/

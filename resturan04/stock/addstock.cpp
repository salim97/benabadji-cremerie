#include "addstock.h"
#include "ui_addstock.h"

addstock::addstock(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addstock)
{
    ui->setupUi(this);
    _myModel->setTable(enumStock::tableName);
    _myModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
    _myModel->select();
}

addstock::~addstock()
{
    delete ui;
}

void addstock::on_pushButton_cancel_clicked()
{
    this->close();
}

void addstock::on_pushButton_ok_clicked()
{
    int row = _myModel->rowCount() ;
    _myModel->insertRow(row);

    _myModel->setData(_myModel->index(row, enumStock::_01Designation), ui->lineEdit_Designation->text());
    _myModel->setData(_myModel->index(row, enumStock::_02Quantite), ui->spinBox_Quantite->text());
    _myModel->setData(_myModel->index(row, enumStock::_03Prix_Unit), ui->spinBox_PU->text());


    if( !_myModel->submitAll() ) msgCritical("insertion erreur", _myModel->lastError().text());
    this->close();
}

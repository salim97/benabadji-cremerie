#-------------------------------------------------
#
# Project created by QtCreator 2017-05-25T15:36:03
#
#-------------------------------------------------

QT       += core gui sql printsupport charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Cremerie
win32: RC_ICONS = newLogoICON.ico
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
INCLUDEPATH += $$PWD/produit
include(produit/produit.pri)

INCLUDEPATH += $$PWD/stock
include(stock/stock.pri)

INCLUDEPATH += $$PWD/transactions
include(transactions/transactions.pri)

INCLUDEPATH += $$PWD/options
include(options/options.pri)

INCLUDEPATH += $$PWD/myComponent
include(myComponent/myComponent.pri)

INCLUDEPATH += $$PWD/calculator
include(calculator/calculator.pri)

INCLUDEPATH += $$PWD/nonpayee
include(nonpayee/nonpayee.pri)

INCLUDEPATH += $$PWD/tableview_tools
include(tableview_tools/tableview_tools.pri)

INCLUDEPATH += $$PWD/xlsx
include(xlsx/qtxlsx.pri)


SOURCES += main.cpp\
        mainwindow.cpp\
        myparentobject.cpp \
    access.cpp

HEADERS  += mainwindow.h\
        myparentobject.h\
        enumtables.h \
    access.h

FORMS    += mainwindow.ui \
    access.ui

RESOURCES += \
    Qt_Resource_File/myresource.qrc


# set exe to bin directory and others file to tmp
OBJECTS_DIR = tmp
MOC_DIR = tmp
DESTDIR = bin

# my auto deploy on release mode
CONFIG(release, debug|release ) {
    # on release mode
        win32{
            TARGET_EXT = .exe
            DEPLOY_COMMAND = windeployqt
            DEPLOY_TARGET = $$shell_quote($$shell_path($${OUT_PWD}/$${DESTDIR}/$${TARGET}$${TARGET_EXT}))

        #warning($${DEPLOY_COMMAND} $${DEPLOY_TARGET})
                QMAKE_POST_LINK = $${DEPLOY_COMMAND} $${DEPLOY_TARGET}
        }
}



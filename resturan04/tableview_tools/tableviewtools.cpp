#include "tableviewtools.h"
#include "ui_tableviewtools.h"
#include "filter.h"
#include "viewcolumns.h"

#include <QSqlField>
#include <QSqlRecord>

tableViewTools::tableViewTools(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::tableViewTools)
{
    ui->setupUi(this);
    ui->toolBox->setCurrentIndex(0);

}

tableViewTools::~tableViewTools()
{
    delete ui;
}

void tableViewTools::setModel(QSqlRelationalTableModel *&model)
{
    myModel = model ;
}

void tableViewTools::setTableView(QTableView *&tableView)
{
    myTableView = tableView;
}

void tableViewTools::on_toolButton_Filter_clicked()
{
    Filter _Filter ;
    _Filter.init(myModel);
    _Filter.exec();
}

void tableViewTools::on_toolButton_suppr_clicked()
{
    int currentRow = myTableView->currentIndex().row() ;
    if( currentRow == -1 ) return ;
    myModel->removeRow(currentRow);
    myModel->select();
}

void tableViewTools::on_toolButton_view_clicked()
{
    //TODO open dialog and show him list of check box for view columns true false then you know what to do
    // costume table view
    ViewColumns viewcolumns;
    viewcolumns.init(myModel);
    viewcolumns.exec();
    for ( int i = 0 ; i < myModel->columnCount() ; i++)
        myTableView->showColumn(i);
    for ( int i = 0 ; i < viewcolumns.columnsToHide.length() ; i++)
    {
        myTableView->hideColumn(viewcolumns.columnsToHide[i]);
    }
}

void tableViewTools::on_toolButton_Export_clicked()
{

}

void tableViewTools::on_toolButton_Import_clicked()
{

}

void tableViewTools::on_toolButton_resizeColumnsToContents_clicked()
{
    myTableView->resizeColumnsToContents();
}

void tableViewTools::on_toolButton_clean_table_clicked()
{
    myModel->query().exec("TRUNCATE "+myModel->tableName()) ;
    myModel->select();
}

#ifndef TABLEVIEWTOOLS_H
#define TABLEVIEWTOOLS_H

#include <QWidget>
#include <QtSql/QSqlTableModel>
#include <QtSql/QSqlRelationalTableModel>
#include <QDebug>
#include <QGridLayout>
#include <QVariant>
#include <qtableview.h>
#include "filter.h"
#include "viewcolumns.h"

namespace Ui {
class tableViewTools;
}

class tableViewTools : public QWidget
{
    Q_OBJECT

public:
    explicit tableViewTools(QWidget *parent = 0);
    ~tableViewTools();
    void setModel(QSqlRelationalTableModel *&model);
    void setTableView(QTableView *&tableView);

private slots:
    void on_toolButton_Filter_clicked();

    void on_toolButton_suppr_clicked();

    void on_toolButton_view_clicked();

    void on_toolButton_Export_clicked();

    void on_toolButton_Import_clicked();

    void on_toolButton_resizeColumnsToContents_clicked();

    void on_toolButton_clean_table_clicked();

private:
    Ui::tableViewTools *ui;
    QSqlRelationalTableModel *myModel ;
    QTableView *myTableView;
};

#endif // TABLEVIEWTOOLS_H

#include "win10pushbuttonstd.h"
#include "ui_win10pushbuttonstd.h"

Win10PushButtonStd::Win10PushButtonStd(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Win10PushButtonStd)
{
    ui->setupUi(this);
    timerEnabled = false ;
    ui->stackedWidget->setCurrentIndex(0);
}

Win10PushButtonStd::~Win10PushButtonStd()
{
    if(timerEnabled)
        timer.stop();
    delete ui;
}

void Win10PushButtonStd::setImage(QString imagePath)
{
    QPixmap pixmap(imagePath);
    int w = ui->label_image->width();
    int h = ui->label_image->height();
    ui->label_image->setPixmap( pixmap.scaled(w, h, Qt::KeepAspectRatio) );
    ui->label_image->setScaledContents(true);
}

void Win10PushButtonStd::setLabelText(QString text)
{
    ui->label_text->setText(text);

}

void Win10PushButtonStd::setLabelTextstate(QString text)
{
    ui->label_state->setText(text);
}

void Win10PushButtonStd::setCSS(QString css)
{
    this->setStyleSheet(css);
}

void Win10PushButtonStd::timeout()
{
    int currentIndex = ui->stackedWidget->currentIndex();

    if(currentIndex == 0 )
    {
        ui->stackedWidget->start(1);
        ui->stackedWidget->setLength(lenght, AnimationStackedWidget::TopToBottom);

    }
    else
    {
        //switchAnimation();
        ui->stackedWidget->start(0);
        ui->stackedWidget->setLength(lenght, AnimationStackedWidget::BottomToTop);
    }

}

void Win10PushButtonStd::startAnimation(int timerIntervalMS, int lenght, QString direction)
{
    this->direction = direction ;
    this->lenght = lenght ;
    timerEnabled = true ;
    timer.setInterval(timerIntervalMS);
    timer.start();
    connect(&timer, SIGNAL(timeout()), this, SLOT(timeout())) ;

    if(direction == "right")
        ui->stackedWidget->setLength(lenght, AnimationStackedWidget::LeftToRight);
    if(direction == "left")
        ui->stackedWidget->setLength(lenght, AnimationStackedWidget::RightToLeft);
    if(direction == "top")
        ui->stackedWidget->setLength(lenght, AnimationStackedWidget::BottomToTop);
    if(direction == "bottom")
        ui->stackedWidget->setLength(lenght, AnimationStackedWidget::TopToBottom);
}

void Win10PushButtonStd::resizeEvent(QResizeEvent *event)
{event = event ;
    lenght = height() ;
}

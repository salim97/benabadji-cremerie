#include "option.h"
#include "ui_option.h"

#include <QInputDialog>

Option::Option(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Option)
{
    ui->setupUi(this);

}

Option::~Option()
{
    delete ui;
}

void Option::on_toolButton_back_clicked()
{
    emit backButton();
}

void Option::init()
{

    int gTimeSwitch = getSettings("gTimeSwitch").toInt();
    int itembyCoulumn = getSettings("itembyCoulumn").toInt();
    int calc = getSettings("calc").toInt();
    int itemSize = getSettings("itemSize").toInt();
    ui->spinBox_animation_speed->setValue(gTimeSwitch);
    ui->spinBox_item_by_column->setValue(itembyCoulumn);
    ui->comboBox_calc->setCurrentIndex(calc);
    ui->spinBox_item_size->setValue(itemSize);
}

void Option::on_toolButton_Reset_clicked()
{
    ui->spinBox_animation_speed->setValue(5000);
    ui->spinBox_item_by_column->setValue(3);
    ui->spinBox_item_size->setValue(200);
    ui->comboBox_calc->setCurrentIndex(0);
    on_toolButton_Save_clicked();
}

void Option::on_toolButton_Save_clicked()
{
    setSettings("gTimeSwitch", ui->spinBox_animation_speed->text());
    setSettings("itembyCoulumn", ui->spinBox_item_by_column->text());
    setSettings("calc", ui->comboBox_calc->currentIndex());
    setSettings("itemSize", ui->spinBox_item_size->text());

    on_toolButton_back_clicked();
}

void Option::on_pushButton_export_clicked()
{
    QStringList tables ;
    tables << "TRANSACTIONS" << "COMMANDE" << "PRODUITS" << "STOCK" ;
    QString table = QInputDialog::getItem(NULL,"DB => xlsx", "sélectionner un tableau", tables);
    int cl = 0 ;
    if (table == "TRANSACTIONS")
    {
        table = enumTransaction::tableName ;
        cl = enumTransaction::columnsList.length();
    }
    else
    if (table == "COMMANDE")
    {
        table = enumCommande::tableName ;
        cl = enumCommande::columnsList.length();
    }
    else
    if (table == "PRODUITS")
    {
        table = enumProduit::tableName ;
        cl = enumProduit::columnsList.length();
    }
    else
    if (table == "STOCK")
    {
        table = enumStock::tableName ;
        cl = enumStock::columnsList.length();
    }
    else
        return ;
    QSqlTableModel temp ;
    temp.setTable(table);
    temp.select() ;
    qDebug() << "cl = " << cl << temp.tableName() << temp.rowCount();
    QXlsx::Document xlsx(":/excel/empty.xlsx");
    int x = 0;
    for ( int i = 0 ; i < temp.rowCount(); i++)
    {
             //if(cl >1) xlsx.write("A"+QString::number(i+1) , temp.record(i).value(0) )  ; // exception
        x++; if(cl >= x) xlsx.write("B"+QString::number(i+1) , temp.record(i).value(x - 1) )  ;
        x++; if(cl >= x) xlsx.write("C"+QString::number(i+1) , temp.record(i).value(x - 1 ))  ;
        x++; if(cl >= x) xlsx.write("D"+QString::number(i+1) , temp.record(i).value(x - 1) )  ;
        x++; if(cl >= x) xlsx.write("E"+QString::number(i+1) , temp.record(i).value(x - 1) )  ;
        x++; if(cl >= x) xlsx.write("F"+QString::number(i+1) , temp.record(i).value(x - 1) )  ;
        x++; if(cl >= x) xlsx.write("G"+QString::number(i+1) , temp.record(i).value(x - 1) )  ;
        x++; if(cl >= x) xlsx.write("H"+QString::number(i+1) , temp.record(i).value(x - 1) )  ;
        x++; if(cl >= x) xlsx.write("I"+QString::number(i+1) , temp.record(i).value(x - 1) )  ;
        x++; if(cl >= x) xlsx.write("J"+QString::number(i+1) , temp.record(i).value(x - 1) )  ;
        x = 0;
    }
    QString extenstion = ".xlsx" ;
    QString filename = QFileDialog::getSaveFileName( this, "Save file", "c://", extenstion );
    if ( filename.isEmpty() ) return ;
    if ( !filename.contains(extenstion) ) filename += extenstion ; // zidlah .Sql
    qDebug() << "filename " << filename ;

    xlsx.saveAs(filename);

    QDesktopServices::openUrl(QUrl(filename, QUrl::TolerantMode));

    //QDesktopServices::openUrl(QUrl::fromLocalFile(opendir ) );

}

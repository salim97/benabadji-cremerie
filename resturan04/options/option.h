#ifndef OPTION_H
#define OPTION_H

#include <QWidget>
#include "myparentobject.h"
#include "xlsxdocument.h"

namespace Ui {
class Option;
}

class Option : public QWidget, public myParentObject
{
    Q_OBJECT

public:
    explicit Option(QWidget *parent = 0);
    ~Option();
    void init();
signals:
    void backButton();
public slots:

    void on_toolButton_back_clicked();

private slots:
    void on_toolButton_Reset_clicked();

    void on_toolButton_Save_clicked();

    void on_pushButton_export_clicked();

private:
    Ui::Option *ui;
};

#endif // OPTION_H

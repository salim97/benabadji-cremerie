#ifndef ACCESS_H
#define ACCESS_H

#include <QDialog>
#include "myparentobject.h"

namespace Ui {
class Access;
}

class Access : public QDialog, public myParentObject
{
    Q_OBJECT

public:
    explicit Access(QWidget *parent = 0);
    ~Access();
    bool myClose ;
private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_comboBox_currentIndexChanged(int index);

private:
    Ui::Access *ui;
};

#endif // ACCESS_H

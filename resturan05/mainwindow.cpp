#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    initVariables();
    myProduit->readTable();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::initVariables()
{
    myProduit = new Produit(this) ; // 01
    //connect(myProduit, SIGNAL(backButton()), this, SLOT(backToHomePage()));
    //connect(myProduit, SIGNAL(validedirectment()), this, SLOT(validedirectment()));
    ui->tabWidget->addTab(myProduit, "PRODUITS") ;

}

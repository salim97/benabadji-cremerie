#include "mainwindow.h"
#include <QApplication>
#include <QSplashScreen>
#include <QThread>
#include "windows.h"
#include "access.h"

bool initializeDatabse(QString databaseLocation);
void msgCritical(QString title ,QString body);

bool fileExists(QString path) ;
void copyFileTo(QString from, QString to);
void createFolderIfNotExsist(QString dirName) ;
QStringList listFile(QString path);

QSqlDatabase myDatabase ;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    DWORD dwVolSerial;
    BOOL bIsRetrieved = GetVolumeInformation(TEXT("C:\\"), NULL, NULL, &dwVolSerial, NULL, NULL, NULL, NULL);
    QString tmp;
    if (bIsRetrieved)
    {
         tmp = QString::number(dwVolSerial);
        //qDebug() <<  tmp ;
    }
    qDebug() << tmp ;

    if ( tmp != "1314303674")
    {
        if ( tmp != "1656602800") return 0;
        Access access;
        access.exec();
        if(access.myClose) return 0 ;
    }

    QString databaseName = "database.db";
    QString databasePath = QDir::toNativeSeparators(QApplication::applicationDirPath()).replace("\\","/") +"/";
    QString databaseLocation = databasePath + databaseName ;

    if(!fileExists(databaseLocation))
        copyFileTo(":/database.db", databaseLocation) ;
    if(!initializeDatabse(databaseLocation))
        return 1 ;

    QIcon icon(":/newLogoJDIDA.png");

    MainWindow w;
    w.setWindowIcon(icon);
    w.showMaximized();
    w.setWindowTitle("Sofiane Kibboua- Cremerie v 2,00");

    return a.exec();
}

/*--------------------*/
bool initializeDatabse(QString databaseLocation)
{
    bool check = QSqlDatabase::isDriverAvailable("QSQLITE") ;
    if( !check ) msgCritical("Error", "QSQLITE is not availabe");

    myDatabase = QSqlDatabase::addDatabase("QSQLITE");
    myDatabase.setDatabaseName(databaseLocation);

    bool ok = myDatabase.open();
    if ( !ok ) msgCritical("Error", myDatabase.lastError().text());
    else
    {
        myDatabase.close();
        QString currentPath = QDir::toNativeSeparators(QApplication::applicationDirPath()).replace("\\","/") +"/";
        QString folderBackup = "DATABASE_BACKUP_AUTO" ;
        QString documentDir = QDir::toNativeSeparators(QApplication::applicationDirPath()) + "\\" + folderBackup +"\\";

        if(QDir(documentDir).exists())
            qDebug() << documentDir ;

        createFolderIfNotExsist(documentDir);

        QString currentDate = QDate::currentDate().toString("yyyy-MM-dd");
        QString fileName  = currentDate + ".db" ;
        QStringList listF = listFile(currentPath+ folderBackup);
        qDebug() << "listFile: " << listF ;
        bool trouve = false ;
        for (int i = 0; i < listF.length(); ++i)
        {
            if(listF.at(i) == fileName)
            {
                trouve = true ;
                break ;
            }
        }
        if(!trouve)
        {
            copyFileTo(databaseLocation, currentPath+folderBackup+"/"+ fileName);
        }
        myDatabase.open();
        qDebug() << "myDatabase.tables(): " << myDatabase.tables() ;

    }
    return ok ;
}

void msgCritical(QString title ,QString body)
{
    qDebug() << "title: "+ title << "\nboady: "+ body ;
    QMessageBox::critical(0,title,body);
}
/*-------------------*/
bool fileExists(QString path)
{
    QFileInfo check_file(path);
    // check if file exists and if yes: Is it really a file and no directory?
    if (check_file.exists() && check_file.isFile()) {
        return true;
    } else {
        return false;
    }
}

void copyFileTo(QString from, QString to)
{
    bool ok ;
    to.replace("\\","/");
    if( !fileExists(to) )
    {
        ok = QFile::copy(from, to);
        if(!ok)
        {
            msgCritical("void myParentObject::copyFileTo(QString from, QString to)",
                        from+"\n"+to+"\n");
            return ;
        }
        QFile myfile(to);
        ok = myfile.setPermissions(QFile::ReadOwner |QFile::WriteOwner ) ;
        if(!ok)
            msgCritical("void myParentObject::copyFileTo(QString from, QString to)",
                        myfile.errorString());
    }
}

void createFolderIfNotExsist(QString dirName)
{
    QString documentDir = dirName +"\\";
    if(!QDir(documentDir).exists()) QDir().mkdir(documentDir);
    if(!QDir(documentDir).exists())
        msgCritical("bool myParentObject::createFolderIfNotExsist(QString dirName)",
                    documentDir);
}

QStringList listFile(QString path)
{
    QDir myDir(path);
    return myDir.entryList(QDir::NoDotAndDotDot
                                                   | QDir::System
                                                   | QDir::Hidden
                                                   | QDir::AllDirs
                                                   | QDir::Files,
                           QDir::DirsFirst);
}

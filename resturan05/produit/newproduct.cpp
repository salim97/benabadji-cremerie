#include "newproduct.h"
#include "ui_newproduct.h"

#include <QBuffer>

NewProduct::NewProduct(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewProduct)
{
    ui->setupUi(this);
    _myModel->setTable(enumProduit::tableName);
    _myModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
    _myModel->select();
    QString columnname=_mapColumns[enumProduit::tableName][enumProduit::_02Categorie] ;
    ui->comboBox_Categorie->addItems(getColumnFromTableWithoutDuplicates(enumProduit::tableName,columnname ));

}

NewProduct::~NewProduct()
{
    delete ui;
}

void NewProduct::setDataToView(itemtable item)
{
    ui->comboBox_Categorie->clear();
    QString columnname=_mapColumns[enumProduit::tableName][enumProduit::_02Categorie] ;
    ui->comboBox_Categorie->addItems(getColumnFromTableWithoutDuplicates(enumProduit::tableName,columnname ));

    //this->itemRow = itemRow ;

    currentItem = item ;
    modifie = true ;
    ui->lineEdit_Designation->setText(item._01Designation);
    for ( int i = 0 ; i < ui->comboBox_Categorie->count(); i++ )
    {
        if( item._02Categorie == ui->comboBox_Categorie->itemText(i) )
        {
            ui->comboBox_Categorie->setCurrentIndex(i);
            break ;
        }
    }
    ui->spinBox_PUA->setValue(item._03Prix_Unit_Achate.toInt());
    ui->spinBox_PUV->setValue(item._04Prix_Unit_Vente.toInt());
    ui->lineEdit_pseudo->setText(item._05Description);
}

void NewProduct::reset()
{
    ui->lineEdit_Designation->setText("");
    ui->spinBox_PUA->setValue(0);
    ui->spinBox_PUV->setValue(0);
    ui->label_image->setText("");
    modifie = false;
    ui->lineEdit_pseudo->setText("");
    //ui->textEdit_description->setPlaceholderText("Description.....");
}

void NewProduct::on_pushButton_cancel_clicked()
{
    this->close();
}

void NewProduct::on_pushButton_save_clicked()
{
    if(ui->comboBox_Categorie->currentText().isEmpty())
    {
        msgCritical("erreur", "no categorie selected !");
        return ;
    }

    QString imagePath =  ui->label_image->text();
    QPixmap pixmap;
    QByteArray bArray;

    if( !imagePath.isEmpty() )
    {
        pixmap.load(imagePath);

        QBuffer buffer(&bArray);
        buffer.open(QIODevice::WriteOnly);
        if(imagePath.contains(".png"))
            pixmap.save(&buffer, "PNG");
        if(imagePath.contains(".PNG"))
            pixmap.save(&buffer, "PNG");

        if(imagePath.contains(".jpg"))
            pixmap.save(&buffer, "jpg");
        if(imagePath.contains(".JPG"))
            pixmap.save(&buffer, "JPG");
    }
    if(modifie)
    {// ici
       int itemRow = 0 ;
        _myModel->setFilter("id == "+ currentItem._00id);
        _myModel->select();
        _myModel->setData(_myModel->index(itemRow, enumProduit::_01Designation), ui->lineEdit_Designation->text());
        _myModel->setData(_myModel->index(itemRow, enumProduit::_02Categorie), ui->comboBox_Categorie->currentText());
        _myModel->setData(_myModel->index(itemRow, enumProduit::_03Prix_Unit_Achate), ui->spinBox_PUA->text());
        _myModel->setData(_myModel->index(itemRow, enumProduit::_04Prix_Unit_Vente), ui->spinBox_PUV->text());
        _myModel->setData(_myModel->index(itemRow, enumProduit::_05Description), ui->lineEdit_pseudo->text());
        if(!imagePath.isEmpty())
            _myModel->setData(_myModel->index(itemRow, enumProduit::_06Image), bArray);


    }
    else
    {
        int row = _myModel->rowCount() ;
        _myModel->insertRow(row);

        _myModel->setData(_myModel->index(row, enumProduit::_01Designation), ui->lineEdit_Designation->text());
        _myModel->setData(_myModel->index(row, enumProduit::_02Categorie), ui->comboBox_Categorie->currentText());
        _myModel->setData(_myModel->index(row, enumProduit::_03Prix_Unit_Achate), ui->spinBox_PUA->text());
        _myModel->setData(_myModel->index(row, enumProduit::_04Prix_Unit_Vente), ui->spinBox_PUV->text());
        _myModel->setData(_myModel->index(row, enumProduit::_05Description), ui->lineEdit_pseudo->text());
        _myModel->setData(_myModel->index(row, enumProduit::_06Image), bArray);
    }


    if( !_myModel->submitAll() ) msgCritical("insertion erreur", _myModel->lastError().text());
    modifie = false ;
    this->close();

}

void NewProduct::on_pushButton_browse_clicked()
{
    QString filter = "All files (*.*) " ;
    QString DefaultFolderPath = "c:/Users/unix/Documents/resturan01/pizza/"; // TODO : change each time for the last folder
    QString filename = QFileDialog::getOpenFileName(this,"open a file", DefaultFolderPath, filter );
    if ( filename.isEmpty() ) return ;
    ui->label_image->setText(filename);
}


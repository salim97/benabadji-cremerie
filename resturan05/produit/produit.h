#ifndef PRODUIT_H
#define PRODUIT_H

#include <QWidget>
#include "myparentobject.h"
#include "itemtablewidget.h"
#include "itemtable.h"
#include "newproduct.h"

namespace Ui {
class Produit;
}

class Produit : public QWidget, public myParentObject
{
    Q_OBJECT

public:
    explicit Produit(QWidget *parent = 0);
    ~Produit();

    void moveDatabaseToRAM();
    void readTable();
    int viewItemsCount();
    void refreshCategorie();
private slots:
    void on_toolButton_refresh_clicked();

    void on_lineEdit_search_editingFinished();

    void on_listWidget_categorie_currentRowChanged(int currentRow);

    void on_toolButton_new_product_clicked();

private:
    Ui::Produit *ui;
    QList<itemtable> produitTable;
    int columnCount;
    QString myFilter ;

};

#endif // PRODUIT_H

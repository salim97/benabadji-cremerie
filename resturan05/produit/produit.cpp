#include "produit.h"
#include "ui_produit.h"

Produit::Produit(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Produit)
{
    ui->setupUi(this);

    ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectItems);
    ui->tableWidget->setSelectionMode( QAbstractItemView::NoSelection );
    ui->tableWidget->setFocusPolicy(Qt::NoFocus);

    moveDatabaseToRAM();
    refreshCategorie();
}

Produit::~Produit()
{
    delete ui;
}

void Produit::on_toolButton_refresh_clicked()
{
    readTable();
    ui->lineEdit_search->setText("");
    ui->listWidget_categorie->setCurrentRow(0);
    refreshCategorie();
}

void Produit::refreshCategorie()
{
    QString columnname=_mapColumns[enumProduit::tableName][enumProduit::_02Categorie] ;
    ui->listWidget_categorie->setDisabled(true);
    ui->listWidget_categorie->clear();
    ui->listWidget_categorie->addItem("01 ALL");
    ui->listWidget_categorie->addItems(getColumnFromTableWithoutDuplicates(enumProduit::tableName,columnname ));
    ui->listWidget_categorie->setCurrentRow(0);
    ui->listWidget_categorie->setEnabled(true);
    ui->listWidget_categorie->sortItems(Qt::AscendingOrder); //DescendingOrder
}

void Produit::on_lineEdit_search_editingFinished()
{
    if(!ui->lineEdit_search->text().isEmpty() )
    {
        //myFilter = _mapColumns[enumProduit::tableName][enumProduit::_01Designation]+" LIKE '%"+ui->lineEdit_search->text()+"%'" ;
        ui->listWidget_categorie->setCurrentRow(-1);
        readTable();
        ui->label->setFocus();
    }
}



void Produit::on_listWidget_categorie_currentRowChanged(int currentRow)
{
    if(ui->listWidget_categorie->currentRow() > -1 )
    {
        QString currentText = ui->listWidget_categorie->item(currentRow)->text();
        if(currentText == "01 ALL")
            ui->lineEdit_search->setText("");
        readTable();
    }
}

void Produit::on_toolButton_new_product_clicked()
{
    NewProduct newproduct;
    newproduct.exec();
    moveDatabaseToRAM();
    readTable();
}

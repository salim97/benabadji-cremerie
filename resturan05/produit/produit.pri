FORMS += \
    $$PWD/produit.ui\
    $$PWD/itemtablewidget.ui \
    $$PWD/newproduct.ui

HEADERS += \
    $$PWD/produit.h\
    $$PWD/itemtablewidget.h\
    $$PWD/itemtable.h \
    $$PWD/newproduct.h

SOURCES += \
    $$PWD/produit.cpp \
    $$PWD/produit_methods.cpp\
    $$PWD/itemtablewidget.cpp \
    $$PWD/newproduct.cpp


#ifndef NEWPRODUCT_H
#define NEWPRODUCT_H

#include <QDialog>
#include "myparentobject.h"
#include "itemtable.h"

namespace Ui {
class NewProduct;
}

class NewProduct : public QDialog, public myParentObject
{
    Q_OBJECT

public:
    explicit NewProduct(QWidget *parent = 0);
    ~NewProduct();

    void setDataToView(itemtable item);
    void reset();
private slots:
    void on_pushButton_save_clicked();

    void on_pushButton_cancel_clicked();

    void on_pushButton_browse_clicked();

private:
    Ui::NewProduct *ui;
    itemtable currentItem ;
    bool modifie = false ;
};

#endif // NEWPRODUCT_H

#include "produit.h"
#include "ui_produit.h"

#include <QSpinBox>
#include <QWidgetAction>

void Produit::readTable()
{
    //int oldcurrentRowList = ui->listWidget_categorie->currentRow() ;
    QHeaderView *verticalHeader = ui->tableWidget->verticalHeader();
    verticalHeader->setSectionResizeMode(QHeaderView::Fixed);
    //verticalHeader->setDefaultSectionSize(getSettings("itemSize").toInt());
    verticalHeader->setDefaultSectionSize(200);

    columnCount = 6 ; //getSettings("itembyCoulumn").toInt();
    QString categorie, designation ;
    if(ui->listWidget_categorie->currentRow() > 0 )
        categorie = ui->listWidget_categorie->currentItem()->text() ;
    designation = ui->lineEdit_search->text() ;
    QList<itemtable> produitTableTemp;

    if( designation.isEmpty() && categorie.isEmpty() )
        produitTableTemp = produitTable;
    else if ( categorie.isEmpty())
    {
        for ( int i = 0 ; i < produitTable.length() ; i++ )
        {

            if(produitTable[i]._01Designation.toLower().contains(designation.toLower()))
            {
                produitTableTemp << produitTable[i] ;
            }
        }
    }
    else
    {
        for ( int i = 0 ; i < produitTable.length() ; i++ )
        {

            if(produitTable[i]._02Categorie == categorie)
            {
                produitTableTemp << produitTable[i] ;
            }
        }
    }
    myFilter.clear();

    // 01 compare between view and data
    // 02 if data rows == view rows then for each modifie values
    // 03 if data rows > view rows then modifie then add

 /*--------------------------------*/
    int rowT = produitTableTemp.length() ;
    int item_by_column = columnCount ;

    int column, row ;

    if( rowT > item_by_column )
        column = item_by_column ;
    else
        column = item_by_column ;

    row = rowT / item_by_column ;

    if( ( rowT % item_by_column ) > 0 )
        row++;
/*-----------------------------*/
    int viewCount = viewItemsCount() ;
    //if()
    ui->tableWidget->setColumnCount(column);
    ui->tableWidget->setRowCount(row);
    int currentRow = 0 ;


    for ( int i = 0 ; i < row ; i++ )
    {
        for ( int j = 0 ; j < column ; j++)
        {
            itemTableWidget *mycell = nullptr ;
            if( currentRow >= rowT)
            {
                ui->tableWidget->setCellWidget(i,j, mycell);
            }
            else
            {

                if(currentRow < viewCount )
                {

                    mycell = (itemTableWidget*)ui->tableWidget->cellWidget(i, j);


                }
                else
                {
                    QTime myTimer;
                    myTimer.start();
                    mycell = new itemTableWidget(this);
                    ui->tableWidget->setCellWidget(i,j, mycell);


                }

                if(mycell == nullptr)
                {

                    mycell = new itemTableWidget(this);
                    ui->tableWidget->setCellWidget(i,j, mycell);

                }
                if( currentRow < produitTableTemp.length())
                mycell->setDataToViewNew(produitTableTemp[currentRow]._00id,
                                      produitTableTemp[currentRow]._01Designation,
                                      produitTableTemp[currentRow]._02Categorie,
                                      produitTableTemp[currentRow]._03Prix_Unit_Achate,
                                      produitTableTemp[currentRow]._04Prix_Unit_Vente,
                                      produitTableTemp[currentRow]._05Description,
                                      produitTableTemp[currentRow]._06ImagePixMap,
                                      currentRow);

                mycell->setSelected(false);
                //connect(mycell, SIGNAL(clicked()), this, SLOT(on_tableWidget_clicked(QModelIndex)));

            }
            currentRow++;
        }
    }





    //ui->tableWidget->resizeRowsToContents();
    ui->tableWidget->resizeColumnsToContents();
    //qDebug() << "viewItemsCount " << viewItemsCount() ;
    //ui->listWidget_categorie->setCurrentRow(oldcurrentRowList); ;
}

int Produit::viewItemsCount()
{
    int count = 0 ;

    for (int i = 0 ; i < ui->tableWidget->rowCount() ; i++ )
    {
        for ( int j = 0 ; j < ui->tableWidget->columnCount() ; j++ )
        {
            if((itemTableWidget*)ui->tableWidget->cellWidget(i, j) == nullptr)
                break ;

            count++ ;
        }
    }
    return count ;
}
void Produit::moveDatabaseToRAM()
{
    _myModel->setTable(enumProduit::tableName);
    _myModel->setFilter("");
    _myModel->select();

    produitTable.clear();
    for ( int i = 0 ; i < _myModel->rowCount() ; i++ )
    {
        itemtable item ;
        item._00id = _myModel->record(i).value(enumProduit::_00id).toString() ;
        item._01Designation = _myModel->record(i).value(enumProduit::_01Designation).toString() ;
        item._02Categorie = _myModel->record(i).value(enumProduit::_02Categorie).toString() ;
        item._03Prix_Unit_Achate = _myModel->record(i).value(enumProduit::_03Prix_Unit_Achate).toString() ;
        item._04Prix_Unit_Vente = _myModel->record(i).value(enumProduit::_04Prix_Unit_Vente).toString() ;
        item._05Description = _myModel->record(i).value(enumProduit::_05Description).toString() ;
        //item._06Image = _myModel->record(i).value(enumProduit::_06Image).toByteArray() ;
        item._06ImagePixMap.loadFromData(
                    _myModel->record(i).value(enumProduit::_06Image).toByteArray() );
        produitTable << item ;
    }
}

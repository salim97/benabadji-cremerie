#include "access.h"
#include "ui_access.h"

Access::Access(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Access)
{
    ui->setupUi(this);
    ui->lineEdit->setVisible(false);
}

Access::~Access()
{
    delete ui;
}

void Access::on_pushButton_clicked()
{
    myClose = true ;
    this->close();
}

void Access::on_pushButton_2_clicked()
{
    if(ui->comboBox->currentIndex() == 0 )
    {
        setSettings("admin", false);

    }
    else
    {
        if(ui->lineEdit->text() == "18121994")
        {
            setSettings("admin", true);
        }
        else
        {
            msgCritical("access", "mauvais mot de passe");
            return ;
        }
    }
    myClose = false ;
    close();
}

void Access::on_comboBox_currentIndexChanged(int index)
{
    if(index == 0 )
        ui->lineEdit->setVisible(false);
    else
        ui->lineEdit->setVisible(true);
}
